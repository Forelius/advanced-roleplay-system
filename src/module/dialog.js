import * as effectManager from './effect/effects.js';
import * as actionManager from './apps/action.js';
import * as utilitiesManager from './utilities.js';
import * as debug from './debug.js';

/**
 * prompt dialog for a quantity of something
 * @param {*} mincount
 * @param {*} maxcount
 * @param {*} default_amount
 * @param {*} question
 * @param {*} title
 * @param {*} confirmLabel
 * @param {*} cancelLabel
 * @param {*} options
 * @returns
 */
export async function getQuantity(
    mincount = 0,
    maxcount = 1,
    default_amount = 1,
    question = 'Take how many?',
    title = 'Taking',
    confirmLabel = 'Aquire',
    cancelLabel = 'Leave',
    options = {}
) {
    console.log('utilities.js getQuantity', { mincount, maxcount, default_amount });
    // if default set to 0 we set it to maxcount (for loot mode)
    if (!default_amount) default_amount = maxcount;

    const content = await renderTemplate('systems/ars/templates/dialogs/dialog-quantity.hbs', {
        question,
        mincount,
        maxcount,
        default_amount,
    });

    return new Promise((resolve) => {
        new foundry.applications.api.DialogV2(
            {
                window: { title },
                content,
                buttons: [
                    {
                        action: 'accept',
                        label: 'Accept',
                        icon: 'fas fa-check',
                        default: true,
                        callback: (event, button, dialog) => {
                            let quantity = button.form.elements.quantity.value;
                            quantity = Math.max(mincount, Math.min(maxcount, quantity));
                            resolve(parseInt(quantity) || 0);
                        },
                    },
                    {
                        action: 'cancel',
                        label: game.i18n.localize('ARS.cancel'),
                        icon: 'fas fa-times',
                        callback: () => {
                            resolve(undefined);
                        },
                    },
                ],
            },
            options
        ).render(true);
    });
}

/**
 *
 * prompt a confirmation dialog
 *
 * @param {*} question
 * @param {*} title
 * @param {*} options
 * @returns
 */
export async function confirm(question = `<p>Are you sure?</p>`, title = 'Confirmation', options = {}) {
    // console.log("utilities.js dialogConfirm", { title, question });
    return foundry.applications.api.DialogV2.confirm({
        window: { title },
        content: question,
        modal: true,
        yes: () => {
            return true;
        },
        no: () => {
            return false;
        },
    });
}

/**
 *
 * Confirm long rest and if provisions (food/water) should be consumed.
 *
 * @param {*} title
 * @param {*} options
 * @returns
 */
export async function askLongRest(title = 'Long Rest Options', options = {}) {
    // Create the dialog content with checkboxes
    const content = `
    <form>
        <div>
            <label>
            <input type="checkbox" name="longRest" checked>
            Take a Long Rest
            </label>
        </div>
        <div>
            <label>
            <input type="checkbox" name="consumeProvisions" checked>
            Consume Provisions
            </label>
        </div>
        <hr />
    </form>
    `;

    return new Promise((resolve) => {
        const dialog = new foundry.applications.api.DialogV2({
            window: { title },
            modal: true,
            content,
            buttons: [
                {
                    action: 'confirm',
                    label: 'Confirm',
                    icon: 'fas fa-check',
                    callback: (event, button, dialog) => {
                        const longRest = button.form.elements.longRest.checked;
                        const consumeProvisions = button.form.elements.consumeProvisions.checked;
                        resolve({ longRest, consumeProvisions });
                    },
                },
                {
                    action: 'cancel',
                    icon: 'fas fa-times',
                    label: game.i18n.localize('ARS.cancel'),
                    default: true,
                    callback: () => {
                        resolve({ cancel: true });
                    },
                },
            ],
            ...options,
        });
        dialog.render(true);
    });
}

/**
 *
 * Get a situational modifer value
 *
 * @param {*} min
 * @param {*} max
 * @param {*} defmod Default modifier value
 * @param {*} question
 * @param {*} title
 * @param {*} flavor
 * @param {*} event
 * @param {*} options
 * @returns { mod, rollMode }
 */
export async function getSituational(
    min = 0,
    max = 1,
    defmod = 0,
    question = 'Situational Modifier:',
    title = 'Modifier',
    flavor = '',
    event = undefined,
    options = {}
) {
    // console.log("dialog.js dialogGetSituational", { min, max, defmod, question, title, flavor })

    const defaultRollMode = game.settings.get('core', 'rollMode');

    // we set a flag on this user that stores this situational value and reuse it if it exists
    const situationalDice = (await game.user.getFlag('world', 'situationalDice')) || [];
    // console.log("dialog.js dialogGetSituational", situationalDice)
    const safeKey = utilitiesManager.safeKey(title);
    if (situationalDice[safeKey]) {
        const mod = parseInt(situationalDice[safeKey].value) || 0;
        if (mod) defmod = mod;
    }
    const content = await renderTemplate('systems/ars/templates/dialogs/dialog-situational-modifier.hbs', {
        flavor,
        question,
        min,
        max,
        defmod,
        CONFIG,
        defaultRollMode,
    });
    return new Promise((resolve) => {
        let dialog = new foundry.applications.api.DialogV2(
            {
                window: { title },
                content,
                buttons: [
                    {
                        action: 'apply',
                        label: game.i18n.localize('ARS.apply'),
                        icon: 'fas fa-check',
                        callback: (event, button, dialog) => {
                            const rollMode = button.form.elements.rollMode.value;
                            let modifier = parseInt(button.form.elements.modifier.value) || 0;
                            modifier = Math.max(min, Math.min(max, modifier));

                            situationalDice[safeKey] = { value: modifier };
                            game.user.setFlag('world', 'situationalDice', situationalDice);

                            resolve({ mod: modifier, rollMode: rollMode });
                        },
                    },
                    {
                        action: 'cancel',
                        icon: 'fas fa-times',
                        label: game.i18n.localize('ARS.cancel'),
                        default: true,
                        callback: () => {
                            resolve({ cancel: true });
                        },
                    },
                ],
                // default: 'submit',
                // close: () => resolve({ cancel: true }),
            },
            options
        );
        if (event) {
            dialog.position.left = event.clientX;
            dialog.position.top = event.clientY;
        }
        dialog.render(true);
    });
}

/**
 *
 * Select a item in actor's inventory and return itemId of selected
 *
 * @param {*} actor
 * @param {*} question
 * @param {*} title
 * @param {*} options
 * @returns
 */
export async function getInventoryItem(actor, question = 'Select Item Resource', title = 'Resource Selection', options = {}) {
    const content = await renderTemplate('systems/ars/templates/dialogs/dialog-inventory-item.hbs', {
        actor,
        title,
        question,
        CONFIG,
        type: options?.type ?? '',
        inventoryList: options?.inventory ? options.inventory : actor.inventory,
    });
    return new Promise((resolve) => {
        new foundry.applications.api.DialogV2(
            {
                window: { title },
                content,
                buttons: [
                    {
                        action: 'submit',
                        label: 'Select', // game.i18n.localize('ARS.apply'),
                        icon: 'fas fa-check',

                        default: true,
                        callback: (event, button, dialog) => {
                            const itemId = button.form.inventorySelect.value || undefined;
                            resolve(itemId);
                        },
                    },
                    {
                        action: 'cancel',
                        label: game.i18n.localize('ARS.cancel'),
                        icon: 'fas fa-times',

                        callback: (event, button, dialog) => resolve(''),
                    },
                ],
            },
            options
        ).render(true);
    });
}

/**
 *
 * SHOW a list of items in a dialog
 *
 * @param {*} itemList Array of item Ids
 * @param {*} text
 * @param {*} title
 * @param {*} options
 * @returns
 */
export async function showItems(
    actor = null,
    itemList = [],
    text = 'These items have been added to this character',
    title = 'Items Received',
    options = {}
) {
    let items = [];
    for (const itemId of itemList) {
        let item;
        if (actor) {
            item = actor.items.get(itemId);
        } else {
            item = await utilitiesManager.getItem(itemId);
        }
        // console.log("dialog.js dialogShowItems item", { item })
        if (item) items.push(item);
    }
    // console.log("dialog.js dialogShowItems", { text, itemList, items })
    const content = await renderTemplate('systems/ars/templates/dialogs/dialog-show-items.hbs', {
        text,
        items,
        CONFIG,
    });

    return new Promise((resolve) => {
        new foundry.applications.api.DialogV2({
            window: { title: title },
            modal: true,
            content: content,
            buttons: [
                {
                    action: 'submit',
                    label: game.i18n.localize('ARS.close'),
                    default: true,
                    callback: (event, button, dialog) => resolve(undefined),
                },
            ],
        }).render({ force: true });
    });
}

/**
 *
 * Simple notification popup
 *
 * @param {*} content
 * @param {*} buttontext
 * @param {*} title
 * @returns
 */
export async function showNotification(content = 'Empty notice text', buttontext = 'OK', title = 'Notification') {
    const response = await foundry.applications.api.DialogV2.prompt({
        window: { title: `${title}` },
        content: `${content}`,
        modal: true,
        ok: {
            label: `${buttontext}`,
            callback: async (html) => {},
        },
    });

    return response;
}

/**
 *
 * Prompt for a string value
 *
 * @param {*} text
 * @param {*} question
 * @param {*} title
 * @param {*} options
 * @returns
 */
export async function getString(text = '', question = 'Enter the string:', title = 'Get String', options = {}) {
    const content = `<form>
            <div class="flexrow">
                <h3>${question}</h3>
                <div>
                    <input name="enteredText" type="text" value=${text} placeholder="Enter text"/>
                </div>
            </div>
        </form>`;

    return await new Promise((resolve) => {
        new foundry.applications.api.DialogV2({
            window: { title: title },
            content: content,
            modal: true,
            buttons: [
                {
                    action: 'submit',
                    label: game.i18n.localize('ARS.submit'),
                    icon: 'fas fa-check',

                    default: true,
                    callback: (event, button, dialog) => {
                        const enteredText = button.form.elements.enteredText.value;
                        resolve(enteredText);
                    },
                },
                {
                    action: 'cancel',
                    label: game.i18n.localize('ARS.cancel'),
                    icon: 'fas fa-times',
                    callback: (event, button, dialog) => resolve(''),
                },
            ],
        }).render({ force: true });
    });
}

/**
 *
 * @param {*} attackLabel
 * @param {*} cancelLabel
 * @param {*} title
 * @param {*} flavor
 * @param {*} weapon
 * @param {*} event
 * @param {*} options
 * @returns { mod, rollMode, acLocation }
 */
export async function getAttack(
    attackLable = 'Attack',
    cancelLabel = game.i18n.localize('ARS.cancel'),
    title = 'Make Attack',
    flavor = '',
    weapon = undefined,
    event = undefined,
    options = {}
) {
    const defaultRollMode = game.settings.get('core', 'rollMode');
    // console.log("dialog.js getAttack", { attackLable,cancelLabel,title })
    let defmod = 0;
    // we set a flag on this user that stores this situational value and reuse it if it exists
    const attackDice = (await game.user.getFlag('world', 'getAttack')) || [];
    if (attackDice[title.slugify({ strict: true })]) {
        const mod = parseInt(attackDice[title.slugify({ strict: true })].value) || 0;
        if (mod) defmod = mod;
    }

    const content = await renderTemplate('systems/ars/templates/dialogs/dialog-get-attack.hbs', {
        flavor,
        defmod,
        CONFIG,
        defaultRollMode,
        weapon,
    });

    return new Promise((resolve) => {
        let dialog = new foundry.applications.api.DialogV2({
            window: { title: title },
            content: content,
            buttons: [
                {
                    action: 'submit',
                    label: attackLable ? attackLable : game.i18n.localize('ARS.apply'),
                    icon: 'fas fa-check',

                    default: true,
                    callback: (event, button, dialog) => {
                        const form = button.form.elements;
                        let modifier = parseInt(form.modifier.value) || 0;
                        const acLocation = form.acLocation.value;
                        const throwRadio = form.acLocation.checked;
                        const rollMode = form.rollMode.value;
                        const throwInstead = throwRadio ? throwRadio.value === 'thrown' : false;

                        attackDice[title.slugify({ strict: true })] = { value: modifier };
                        game.user.setFlag('world', 'getAttack', attackDice);

                        const result = {
                            mod: modifier,
                            rollMode: rollMode,
                            acLocation: acLocation,
                            throwInstead: throwInstead,
                        };

                        resolve(result);
                    },
                },
                {
                    action: 'cancel',
                    label: cancelLabel ? cancelLabel : game.i18n.localize('ARS.cancel'),
                    icon: 'fas fa-times',
                    callback: (event, button, dialog) => resolve({ cancel: true }),
                },
            ],
        });
        if (event) {
            dialog.position.left = event.clientX;
            dialog.position.top = event.clientY;
        }
        dialog.render(true);
    });
}

/**
 *
 *
 *
 * @param {*} damageLabel
 * @param {*} cancelLabel
 * @param {*} title
 * @param {*} options
 * @returns
 */
export async function getDamage(
    damageLabel = 'Damage',
    cancelLabel = game.i18n.localize('ARS.cancel'),
    title = 'Apply Damage',
    flavor = '',
    event = undefined,
    options = {}
) {
    const defaultRollMode = game.settings.get('core', 'rollMode');
    // console.log("dialog.js getDamage", { damageLabel,cancelLabel,title })
    let defmod = 0;

    // we set a flag on this user that stores this situational value and reuse it if it exists
    const damageDice = (await game.user.getFlag('world', 'getDamage')) || [];
    if (damageDice[title.slugify({ strict: true })]) {
        const mod = parseInt(damageDice[title.slugify({ strict: true })].value) || 0;
        if (mod) defmod = mod;
    }

    const content = await renderTemplate('systems/ars/templates/dialogs/dialog-get-damage.hbs', {
        flavor,
        defmod,
        CONFIG,
        defaultRollMode,
    });

    return new Promise((resolve) => {
        let dialog = new foundry.applications.api.DialogV2({
            window: { title },
            content: content,
            buttons: [
                {
                    action: 'submit',
                    label: damageLabel,
                    label: damageLabel ? damageLabel : game.i18n.localize('ARS.apply'),
                    icon: 'fas fa-check',
                    default: true,
                    callback: (event, button, dialog) => {
                        const form = button.form;
                        let modifier = parseInt(form.elements.modifier.value) || 0;
                        const dmgAdjustment = form.dmgAdjustment.value;

                        damageDice[title.slugify({ strict: true })] = { value: modifier };
                        game.user.setFlag('world', 'getDamage', damageDice);

                        const result = {
                            mod: modifier,
                            rollMode: form.elements.rollMode.value,
                            dmgAdjustment: dmgAdjustment,
                        };

                        resolve(result);
                    },
                },
                {
                    action: 'cancel',
                    label: cancelLabel ? cancelLabel : game.i18n.localize('ARS.cancel'),
                    icon: 'fas fa-times',
                    callback: (event, button, dialog) => resolve({ cancel: true }),
                },
            ],
        });

        if (event) {
            dialog.position.left = event.clientX;
            dialog.position.top = event.clientY;
        }

        dialog.render({ force: true });
    });
}

/**
 *
 * @param {*} question
 * @param {*} title
 * @param {*} flavor
 * @param {*} casting
 * @param {*} options
 * @returns
 */
export async function getInitiative(
    question = 'Modifier',
    title = 'Initiative',
    flavor = 'Rolling Initiative',
    casting = false,
    options = {}
) {
    let defmod = 0;

    const defaultRollMode = game.settings.get('core', 'rollMode');
    // we set a flag on this user that stores this situational value and reuse it if it exists
    const situationalDice = (await game.user.getFlag('world', 'situationalInitiative')) || [];
    if (situationalDice[title.slugify({ strict: true })]) {
        const mod = parseInt(situationalDice[title.slugify({ strict: true })].value) || 0;
        if (mod) defmod = mod;
        // casting = situationalDice[title.slugify({ strict: true })].casting;
    }
    const content = await renderTemplate('systems/ars/templates/dialogs/dialog-initiative-modifier.hbs', {
        flavor,
        question,
        defmod,
        casting,
        CONFIG,
        actionText: options?.actionText,
        defaultRollMode,
    });

    return new Promise((resolve) => {
        new foundry.applications.api.DialogV2({
            window: { title: title },
            content: content,
            modal: true,
            buttons: [
                {
                    action: 'submit',
                    label: game.i18n.localize('ARS.apply'),
                    icon: 'fas fa-check',
                    default: true,
                    callback: (event, button, dialog) => {
                        const form = button.form.elements;
                        let actionText = form.actiontext.value ?? '';
                        let modifier = parseInt(form.modifier.value) || 0;
                        let casting = form.casting.checked;

                        situationalDice[title.slugify({ strict: true })] = { value: modifier, casting: casting };
                        game.user.setFlag('world', 'situationalInitiative', situationalDice);

                        const result = {
                            mod: modifier,
                            casting: casting,
                            action: actionText,
                            rollMode: form.rollMode.value,
                        };

                        resolve(result);
                    },
                },
                {
                    action: 'cancel',
                    label: game.i18n.localize('ARS.cancel'),
                    icon: 'fas fa-times',
                    callback: (event, button, dialog) => resolve({ mod: undefined, rollMode: undefined }),
                },
            ],
        }).render({ force: true });
    });
}

/**
 * Get action group name and initiative speed
 * @param {*} actionGroup
 * @param {*} title
 * @returns
 */
export async function getActionGroupDetails(actionGroup, title = 'Edit Action Group') {
    const content = `
        <form>
          <div class="form-group">
            <label for="actionGroupName">Name:</label>
            <input type="text" id="actionGroupName" name="actionGroupName" value="${actionGroup.name}">
          </div>
        </form>
      `;

    //   <div class="form-group">
    //   <label for="actionGroupSpeed">Initiative:</label>
    //   <input type="number" id="actionGroupSpeed" name="actionGroupSpeed" value="${actionGroup.speedfactor}">
    // </div>

    return new Promise((resolve) => {
        new foundry.applications.api.DialogV2({
            window: { title: title },
            content: content,
            modal: true,
            buttons: [
                {
                    action: 'submit',
                    label: game.i18n.localize('ARS.apply'),
                    icon: 'fas fa-check',
                    default: true,
                    callback: (event, button, dialog) => {
                        const form = button.form;
                        let actionGroupName = form.actionGroupName.value ?? actionGroup.name;
                        const result = {
                            name: actionGroupName,
                        };

                        resolve(result);
                    },
                },
                {
                    action: 'cancel',
                    label: game.i18n.localize('ARS.cancel'),
                    icon: 'fas fa-times',
                    callback: (event, button, dialog) => resolve({ name: undefined, speed: undefined }),
                },
            ],
        }).render({ force: true });
    });
}
