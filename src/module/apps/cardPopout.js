const { ApplicationV2, HandlebarsApplicationMixin } = foundry.applications.api;
import * as chatManager from '../chat.js';
import * as utilitiesManager from '../utilities.js';

export class ARSCardPopout extends Application {
    // this is for AppV2
    // export class ARSCardPopout extends HandlebarsApplicationMixin(ApplicationV2) {
    constructor(context, options = {}) {
        const _mousePosOffset = 50;
        console.log('ARSCardPopout constructor', { context, options });
        super(options); // Ensure the parent class is designed to handle the options

        // Validating context object
        if (!context) {
            throw new Error('Context is required');
        }

        // Basic property initialization with validation
        this.item = context.item || null;
        this.context = foundry.utils.deepClone(context);
        this.actor = context.sourceActor || null;
        this.token = context.sourceToken || this.actor.getToken();

        // Additional properties
        let cardDescription = this.item ? this.item.system.description : context?.actionGroupData?.description || '';

        if (context.actionV2) {
            this.actionV2 = true;
            this.actionGroup = context.actionGroup || null;
            cardDescription = this.actionGroup.description;
        } else {
            this.actionGroup = context.actionGroup || null;
            this.actionGroupData = context.actionGroupData || null;
            this.actionGroups = context.actionGroups || null;
            this.actions = context.actions || null;
        }

        // Streamlined conditional assignment for cardType
        let cardType = context.type || this.item?.type;

        // ProperCard setup with checks
        this.properCard = {
            type: cardType,
            id: this.item ? this.item.id : this.actor ? this.actor.id : null,
            name: this.item ? this.item.name : this.actionGroup || '',
            description: cardDescription,
        };

        if (this.token?.object && this.token.object.combatHud) {
            this.combatHud = this.token.object.combatHud;
            this.token.object.popoutCards.push(this);
        }
        this.position.left = context.event.clientX + _mousePosOffset;
        this.position.top = context.event.clientY + _mousePosOffset;

        console.log('ARSCardPopout constructor', this);
    }

    // //appV2 specific
    // static DEFAULT_OPTIONS = {
    //     tag: 'popoutcard',
    //     classes: ['app-ars', 'app-popoutCard'],
    //     window: {
    //         title: 'Card Popout',
    //         resizable: true,
    //         icon: 'fas fa-dragon',
    //         // contentTag: 'contentTagwhatgoesthis',
    //         contentClasses: ['ars', 'popoutCard'],
    //     },
    //     position: {
    //         width: 'auto',
    //         height: 'auto',
    //         // zIndex: number
    //     },
    // }; // end DEFAULT_OPTIONS

    // //appV2 specific
    // static PARTS = {
    //     skill: { template: 'systems/ars/templates/chat/parts/chatCard-skill.hbs' },
    //     weapon: { template: 'systems/ars/templates/chat/parts/chatCard-weapon.hbs' },
    //     spell: { template: 'systems/ars/templates/chat/parts/chatCard-spell.hbs' },
    //     action: { template: 'systems/ars/templates/chat/parts/chatCard-action.hbs' },
    //     potion: { template: 'systems/ars/templates/chat/parts/chatCard-action.hbs' },

    //     //     header: { template: '' },
    //     //     tabs: { template: '' },
    //     //     description: { template: '' },
    //     //     foo: { template: '' },
    //     //     bar: { template: '' },
    // };

    // //appV2 get template... ish
    // _configureRenderOptions(options) {
    //     super._configureRenderOptions(options);
    //     // set the parts to only the proper card
    //     options.parts = [this.properCard.type];
    // }

    /** @override */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            classes: ['ars', 'popoutCard'],
            height: 'auto',
            // weight: 'auto',
            // height: '250',
            width: '330',
            resizable: true,
            template: 'systems/ars/templates/chat/parts/chatCard-spell.hbs',
            title: 'Card Popout',
        });
    }

    static cardTemplate = {
        skill: 'systems/ars/templates/chat/parts/chatCard-skill.hbs',
        weapon: 'systems/ars/templates/chat/parts/chatCard-weapon.hbs',
        spell: 'systems/ars/templates/chat/parts/chatCard-spell.hbs',
        action: 'systems/ars/templates/chat/parts/chatCard-action.hbs',
        potion: 'systems/ars/templates/chat/parts/chatCard-action.hbs',
        actionV2: 'systems/ars/templates/chat/parts/chatCard-actionV2.hbs',
        potionV2: 'systems/ars/templates/chat/parts/chatCard-actionV2.hbs',
    };

    get id() {
        return `${this.properCard.type}_${this.properCard.id}`;
    }
    get title() {
        return `[${this.actor.name}] ${utilitiesManager.capitalize(this.properCard.type)}: ${this.properCard.name}`;
    }

    get template() {
        let hbsfile = this.constructor.cardTemplate[`action${this.actionV2 ? 'V2' : ''}`];
        const cardType = this.actionV2 ? `${this.properCard.type}V2` : this.properCard.type;
        try {
            hbsfile = this.constructor.cardTemplate[cardType] ?? this.constructor.cardTemplate['action'];
        } catch (err) {
            console.error(`cardPopout.js template error: ${err}`);
        }
        return hbsfile;
    }
    async getData() {
        let context = await super.getData();
        this.context.isPopout = true;
        context = foundry.utils.mergeObject(context, this.context);

        const descriptionSource =
            this.item?.system?.description && context.actionGroupData
                ? this.item.system.description
                : this.properCard.description;

        if (descriptionSource) {
            const enrichedDescription = await TextEditor.enrichHTML(descriptionSource, { async: true });

            if (this.item?.system?.description && context.actionGroupData) {
                context.itemDescription = enrichedDescription;
                context.actionGroupData.description = enrichedDescription;
            } else {
                context.itemDescription = enrichedDescription;
            }
        }

        if (this.item && this.item?.pack)
            this.item.packLink = await TextEditor.enrichHTML(`@Compendium[${this.item.pack}.${this.item.id}]`, { async: true });

        if (this.item && !this.item?.isOwned)
            this.item.itemLink = await TextEditor.enrichHTML(`@Item[${this.item.id}]`, { async: true });

        return context;
    }

    //appV2 "getData()"
    // async _prepareContext(options) {
    //     let context = {
    //         isPopout: true,
    //         ...this.context,
    //     };

    //     const descriptionSource =
    //         this.item?.system?.description && context.actionGroupData
    //             ? this.item.system.description
    //             : this.properCard.description;

    //     if (descriptionSource) {
    //         const enrichedDescription = await TextEditor.enrichHTML(descriptionSource, { async: true });

    //         if (this.item?.system?.description && context.actionGroupData) {
    //             context.itemDescription = enrichedDescription;
    //             context.actionGroupData.description = enrichedDescription;
    //         } else {
    //             context.itemDescription = enrichedDescription;
    //         }
    //     }

    //     if (this.item && this.item?.pack)
    //         this.item.packLink = await TextEditor.enrichHTML(`@Compendium[${this.item.pack}.${this.item.id}]`, { async: true });

    //     if (this.item && !this.item?.isOwned)
    //         this.item.itemLink = await TextEditor.enrichHTML(`@Item[${this.item.id}]`, { async: true });

    //     return context;
    // }

    activateListeners(html) {
        super.activateListeners(html);
        html.on('click', '.chatCard-expand-view', chatManager._onToggleCardDescription);
        html.on('click', '.card-buttons button', chatManager.chatAction);
    }

    close() {
        // this will close any popoutCards that were opened by this popout.
        if (this?.token?.object?.popoutCards) this.token.object.popoutCards.pop(this);
        super.close();
    }
}
