import { ARSActor } from './actor/actor.js';
import { ARSItem } from './item/item.js';
import { ARS } from './config.js';
import * as debug from './debug.js';
import * as chatManager from './chat.js';
import * as utilitiesManager from './utilities.js';
import * as dialogManager from './dialog.js';
import { ARSDamage } from './dice/damage.js';
import { ARSRollDamage } from './dice/rolls.js';
// import { ARSRollAttack, ARSRollDamage } from './dice/rolls.js';

/**
 *
 * Various overrides
 *
 */

/**
 * Combatant extend to override isNPC to work
 * more how we expect it
 */
export class ARSCombatant extends Combatant {
    /**@override */
    _onCreate(data, options, userId) {
        // console.log('ARSCombatant _onCreate', { data, options, userId });
        super._onCreate(data, options, userId);

        /**
         * here we set the "startTime" on any temporary effects for
         * npcs that get added to the combat tracker.
         *
         * Do this so that effects on a npc timer starts at a more
         * intellingent place because they sit around on the map
         * indefinitely.
         */
        const token = utilitiesManager.getTokenById(data.tokenId, data.sceneId);
        if (token?.actor && token.actor.type !== 'character') {
            // console.log('ARSCombatant _onCreate', { token });
            const worldTime = game.time.worldTime;

            // Define an async function and immediately invoke it
            (async () => {
                for (const effect of token.actor.temporaryEffects) {
                    console.log(`Setting new start time for token ${token.name} for effect ${effect.name}.`, {
                        token,
                        effect,
                        worldTime,
                    });
                    await effect.update({
                        'duration.startTime': worldTime,
                    });
                }
            })();
        }
    }

    /**@override */
    get isNPC() {
        // return !this.actor || !this.players.length;
        return !this.actor || this.actor.type === 'npc';
    }

    getInitiativeRoll(formula, rollData = undefined) {
        console.log('overrides.js getInitiativeRoll()', { formula, rollData }, this);
        formula = formula || this._getInitiativeFormula();
        const rollContext = rollData ? rollData : this.actor?.getRollData() || {};
        return Roll.create(formula, rollContext);
    }
}

/**
 *
 * We override this so that the permissions are set recursively.
 *
 * this will check for children in folder and if exist also set permissions in those
 *
 */
export class ARSPermissionControl extends DocumentOwnershipConfig {
    async _updateObject(event, formData) {
        // console.log("overrides.js ARSPermissionControl _updateObject", { event, formData });
        // console.log("overrides.js ARSPermissionControl _updateObject this.document", this.document);
        event.preventDefault();
        if (!game.user.isGM) throw new Error('You do not have the ability to configure permissions.');
        // Collect user permissions
        const metaLevels = CONST.DOCUMENT_META_OWNERSHIP_LEVELS;
        const isFolder = this.document instanceof Folder;
        const omit = isFolder ? metaLevels.NOCHANGE : metaLevels.DEFAULT;

        const ownershipLevels = {};
        for (let [user, level] of Object.entries(formData)) {
            if (level === omit) {
                delete ownershipLevels[user];
                continue;
            }
            ownershipLevels[user] = level;
        }

        const allFolders = (folderDoc) => {
            console.log('overrides.js ARSPermissionControl', folderDoc.name, { folderDoc });
            const updates = folderDoc.contents.map((e) => {
                const ownership = foundry.utils.deepClone(e.ownership);
                for (let [k, v] of Object.entries(ownershipLevels)) {
                    if (v === metaLevels.DEFAULT) delete ownership[k];
                    else ownership[k] = v;
                    // if (v === -2) delete p[k];
                    // else p[k] = v;
                }
                // return { _id: e.id, permission: p }
                return { _id: e.id, ownership };
            });
            const cls = getDocumentClass(folderDoc.type);
            cls.updateDocuments(updates, { diff: false, recursive: false, noHook: true });
            const children = folderDoc?.children;
            if (children)
                children.forEach((e) => {
                    if (e?.folder instanceof Folder) allFolders(e.folder);
                });
        };

        // Update all documents in a Folder
        if (this.document instanceof Folder) {
            return allFolders(this.document);
        }

        // Update a single Document
        // return this.document.update({ permission: perms }, { diff: false, recursive: false, noHook: true });
        return this.document.update({ ownership: ownershipLevels }, { diff: false, recursive: false, noHook: true });
    }
}

export class ARSJournalDirectory extends JournalDirectory {
    // /** @inheritdoc */
    // static get defaultOptions() {
    //     const opts = super.defaultOptions;
    //     opts.resizable = true;
    //     return opts;
    // }

    /** @override */
    createPopout() {
        const pop = super.createPopout();
        pop.options.resizable = true;
        pop.options.height = 650;
        return pop;
    }
}

export class ARSRollTableDirectory extends RollTableDirectory {
    // /** @inheritdoc */
    // static get defaultOptions() {
    //     const opts = super.defaultOptions;
    //     opts.resizable = true;
    //     return opts;
    // }
    /** @override */
    createPopout() {
        const pop = super.createPopout();
        pop.options.resizable = true;
        pop.options.height = 650;
        return pop;
    }
}

/**
 * Override ChatMessage so we can insert token object
 * to use in chat message view (see it's img)
 */
export class ARSChatMessage extends ChatMessage {
    async getHTML() {
        // const context = await super.getHTML();
        // console.log('overrides.js ARSChatMessage ', { context });
        // return context;
        // Determine some metadata
        const context = this.toObject(false);
        context.content = await TextEditor.enrichHTML(this.content, { async: true, rollData: this.getRollData() });
        // console.trace('overrides.js ARSChatMessage ', { context }, this);
        const isWhisper = this.whisper.length;

        //get token
        const scene = context.speaker?.scene ? game.scenes.get(context.speaker.scene) : null;
        const token = scene ? scene.tokens.get(context.speaker.token) ?? undefined : undefined;

        // Construct message data
        const messageData = {
            message: context,
            user: game.user,
            author: this.author,
            alias: this.alias,
            token: token,
            cssClass: [
                this.style === CONST.CHAT_MESSAGE_STYLES.IC ? 'ic' : null,
                this.style === CONST.CHAT_MESSAGE_STYLES.EMOTE ? 'emote' : null,
                isWhisper ? 'whisper' : null,
                this.blind ? 'blind' : null,
            ].filterJoin(' '),
            isWhisper: this.whisper.length,
            canDelete: game.user.isGM, // Only GM users are allowed to have the trash-bin icon in the chat log itself
            whisperTo: this.whisper
                .map((u) => {
                    let user = game.users.get(u);
                    return user ? user.name : null;
                })
                .filterJoin(', '),
        };

        // Render message data specifically for ROLL type messages
        if (this.isRoll) {
            await this._renderRollContent(messageData);
        }

        // Define a border color
        if (this.style === CONST.CHAT_MESSAGE_STYLES.OOC) {
            messageData.borderColor = this.author?.color;
        }

        // Render the chat message
        let html = await renderTemplate(CONFIG.ChatMessage.template, messageData);
        html = $(html);

        // Flag expanded state of dice rolls
        if (this._rollExpanded) html.find('.dice-tooltip').addClass('expanded');
        Hooks.call('renderChatMessage', this, html, messageData);

        this.activateListeners(html);

        // console.log('overrides.js ARSChatMessage ', { context, html, messageData });
        return html;
    }

    activateListeners(html) {
        /** we use chatManager because as put a popout card at mouse as well */
        html.on('click', '.chatCard-expand-view', chatManager._onToggleCardDescription);
        html.on('click', '.card-buttons button', chatManager.chatAction);
    }
} // end ARSChatMessage

export class ARSChatLog extends ChatLog {
    /** @override */
    createPopout() {
        const pop = super.createPopout();
        pop.options.resizable = true;
        pop.options.height = 650;
        return pop;
    }

    /**@override so we can insert some additional context menus */
    _getEntryContextOptions() {
        let contextOptions = super._getEntryContextOptions();

        async function applyDamageHelper(li, adjustment) {
            const message = game.messages.get(li.data('messageId'));
            const damage = message.system.damage;
            const damageType = message.system.damageType;
            const adjustedDamage = Math.round(damage * adjustment);
            console.log(`overrides.js _getEntryContextOptions applyDamageHelper ${(damage, damageType, adjustedDamage)}`);
            const isDamage = adjustedDamage < 0;
            for (const target of game.user.targets) {
                // utilitiesManager.adjustActorHealth(target.actor, adjustedDamage);

                const damageDiceRoll = new ARSRollDamage(
                    target.actor,
                    undefined,
                    target,
                    String(Math.abs(adjustedDamage)),
                    {},
                    { skipSitiational: true }
                );
                await damageDiceRoll.rollDamage(isDamage);
                const arsdmg = new ARSDamage(isDamage, target.actor, target, undefined, damageType, damageDiceRoll);
                await arsdmg.applyDamageAdjustments();
                const diff = arsdmg.applyDamageToActor();
                // // send damage chat message
                arsdmg.sendDamageChatCard(diff, { flavor: ' (Manually Applied)' });
            }
        }

        async function writeJournalHelper(message) {
            const name = message.flavor
                ? message.flavor
                : await dialogManager.getString('Name', 'Enter Page Name', 'Page Name');
            const journal = await JournalEntry.create(
                {
                    name: name,
                    content: `${name} ${message.content}`,
                    folder: '',
                },
                { displaySheet: true, temporary: false }
            );
            journal.sheet.render(true);
        }

        const optionsApplyDamage = {
            name: 'CHAT.ApplyDamage', //"Apply damage to Current Target",
            icon: '<i class="fa-solid fa-swords"></i>',
            condition: (li) => {
                const message = game.messages.get(li.data('messageId'));
                return game.user.isGM && message.system.damage ? true : false;
            },
            callback: (li) => {
                applyDamageHelper(li, 1);
            },
        };

        const optionsApplyHalfDamage = {
            name: 'CHAT.ApplyHalfDamage',
            icon: '<i class="fa-solid fa-swords"></i>',
            condition: (li) => {
                const message = game.messages.get(li.data('messageId'));
                return game.user.isGM && message.system.damage ? true : false;
            },
            callback: (li) => {
                applyDamageHelper(li, 0.5);
            },
        };

        const optionsApplyQuarterDamage = {
            name: 'CHAT.ApplyQuarterDamage',
            icon: '<i class="fa-solid fa-swords"></i>',
            condition: (li) => {
                const message = game.messages.get(li.data('messageId'));
                return game.user.isGM && message.system.damage ? true : false;
            },
            callback: (li) => {
                applyDamageHelper(li, 0.25);
            },
        };

        const optionWriteToJournal = {
            name: 'CHAT.WriteToJournal',
            icon: '<i class="fa-solid fa-book"></i>',
            condition: (li) => {
                const message = game.messages.get(li.data('messageId'));
                // return message.flags?.core?.RollTable ? true : false;
                return true;
            },
            callback: (li) => {
                const message = game.messages.get(li.data('messageId'));
                writeJournalHelper(message);
            },
        };

        contextOptions.push(optionsApplyDamage);
        contextOptions.push(optionsApplyHalfDamage);
        contextOptions.push(optionsApplyQuarterDamage);
        contextOptions.push(optionWriteToJournal);

        return contextOptions;
    }

    // async getData() {
    //     const context = await super.getData();
    //     console.log('overrides.js ARSChatLog ', { context });
    //     return context;
    // }
}
export class ARSItemDirectory extends ItemDirectory {
    // /** @inheritdoc */
    // static get defaultOptions() {
    //     const opts = super.defaultOptions;
    //     opts.resizable = true;
    //     return opts;
    // }
    /** @override */
    createPopout() {
        const pop = super.createPopout();
        pop.options.resizable = true;
        pop.options.height = 650;
        // ARSItemBrowserManager.addItemBrowserButton({ id: 'items' }, pop.element);
        return pop;
    }
}

export class ARSPlaylistDirectory extends PlaylistDirectory {
    // /** @inheritdoc */
    // static get defaultOptions() {
    //     const opts = super.defaultOptions;
    //     opts.resizable = true;
    //     return opts;
    // }
    /** @override */
    createPopout() {
        const pop = super.createPopout();
        pop.options.resizable = true;
        pop.options.height = 650;
        return pop;
    }
}
export class ARSCompendiumDirectory extends CompendiumDirectory {
    // /** @inheritdoc */
    // static get defaultOptions() {
    //     const opts = super.defaultOptions;
    //     opts.resizable = true;
    //     return opts;
    // }
    /** @override */
    createPopout() {
        const pop = super.createPopout();
        pop.options.resizable = true;
        pop.options.height = 650;
        return pop;
    }
}
export class ARSRollTable extends RollTable {
    /**
     * Evaluate a RollTable by rolling its formula and retrieving a drawn result.
     *
     * Note that this function only performs the roll and identifies the result, the RollTable#draw function should be
     * called to formalize the draw from the table.
     *
     * @param {object} [options={}]       Options which modify rolling behavior
     * @param {Roll} [options.roll]                   An alternative dice Roll to use instead of the default table formula
     * @param {boolean} [options.recursive=true]   If a RollTable document is drawn as a result, recursively roll it
     * @param {number} [options._depth]            An internal flag used to track recursion depth
     * @returns {Promise<RollTableDraw>}  The Roll and results drawn by that Roll
     *
     * @example Draw results using the default table formula
     * ```js
     * const defaultResults = await table.roll();
     * ```
     *
     * @example Draw results using a custom roll formula
     * ```js
     * const roll = new Roll("1d20 + @abilities.wis.mod", actor.getRollData());
     * const customResults = await table.roll({roll});
     * ```
     */
    async roll({ roll, recursive = true, _depth = 0 } = {}) {
        const _MAX_DEPTH = 100;
        // Prevent excessive recursion
        if (_depth > _MAX_DEPTH) {
            throw new Error(`Maximum recursion depth exceeded when attempting to draw from RollTable ${this.id}`);
        }

        // If there is no formula, automatically calculate an even distribution
        if (!this.formula) {
            await this.normalize();
        }

        // Reference the provided roll formula
        roll = roll instanceof Roll ? roll : Roll.create(this.formula);
        let results = [];

        // Ensure that at least one non-drawn result remains
        const available = this.results.filter((r) => !r.drawn);
        if (!available.length) {
            ui.notifications.warn(game.i18n.localize('TABLE.NoAvailableResults'));
            return { roll, results };
        }

        // Ensure that results are available within the minimum/maximum range
        const minRoll = (await roll.reroll({ minimize: true })).total;
        const maxRoll = (await roll.reroll({ maximize: true })).total;
        const availableRange = available.reduce(
            (range, result) => {
                const r = result.range;
                if (!range[0] || r[0] < range[0]) range[0] = r[0];
                if (!range[1] || r[1] > range[1]) range[1] = r[1];
                return range;
            },
            [null, null]
        );
        if (availableRange[0] > maxRoll || availableRange[1] < minRoll) {
            ui.notifications.warn('No results can possibly be drawn from this table and formula.');
            return { roll, results };
        }

        // Continue rolling until one or more results are recovered
        let iter = 0;
        while (!results.length) {
            if (iter >= 10000) {
                ui.notifications.error(`Failed to draw an available entry from Table ${this.name}, maximum iteration reached`);
                break;
            }
            roll = await roll.reroll();
            results = this.getResultsForRoll(roll.total);
            iter++;
        }

        // Draw results recursively from any inner Roll Tables
        if (recursive) {
            let inner = [];
            for (let result of results) {
                let pack;
                let documentName;
                if (result.type === CONST.TABLE_RESULT_TYPES.DOCUMENT) documentName = result.documentCollection;
                else if (result.type === CONST.TABLE_RESULT_TYPES.COMPENDIUM) {
                    pack = game.packs.get(result.documentCollection);
                    documentName = pack?.documentName;
                }
                if (documentName === 'RollTable') {
                    const id = result.documentId;
                    const innerTable = pack ? await pack.getDocument(id) : game.tables.get(id);
                    if (innerTable) {
                        const innerRoll = await innerTable.roll({ _depth: _depth + 1 });
                        inner = inner.concat(innerRoll.results);
                    }
                } else inner.push(result);
            }
            results = inner;
        }

        // Return the Roll and the results
        return { roll, results };
    }
} // ARSRollTable

/**
 * @override to resize RollTable sheet window
 */
export class ARSRollTableConfig extends RollTableConfig {
    /** @override */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            resizable: true,
            height: '650',
        });
    }
} //ARSRollTableConfig

export class ARSTokenConfig extends TokenConfig {
    /**@override */
    static get defaultOptions() {
        const defaultOpts = super.defaultOptions;
        defaultOpts.template = 'systems/ars/templates/scene/token-config.hbs';
        return defaultOpts;
    }

    /**@override */
    async getData() {
        console.log('overrides.js ARSTokenConfig getData', {});
        const sheetData = await super.getData();
        sheetData.config = ARS;
        console.log('overrides.js ARSTokenConfig getData', { sheetData });
        return sheetData;
    }
} // ARSTokenConfig

export class ARSUser extends User {
    /**
     * isDM returns true if the 'isGM' is the primary activeGM
     * There is only 1 activeGM and any others are just GMs.
     *
     * Used for things that should only run once by GM (lighting/vision updates/etc)
     */
    get isDM() {
        return this.isGMPrimary;
    }
    get isGMPrimary() {
        // return this.hasRole(CONST.USER_ROLES.GAMEMASTER);
        return game.users.activeGM.id === game.user.id;
    }
} // end ARSUser
