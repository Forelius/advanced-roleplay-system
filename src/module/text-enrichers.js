/**
 * This will take entries in journals such as
 *
 * [[/loadscene QY9Ws6RL4Srbv11e]]{Village of Shadowdale}
 *
 * And make it a link and when clicked on execute the load in view mode.
 * //TODO: add in &Reference entries?
 *
 */
export class Enrichers {
    /**
     *
     * Load the scene
     *
     * @param {String} sceneId
     */
    static async loadScene(sceneId) {
        const scene = game.scenes.get(sceneId);
        if (scene) {
            if (scene !== canvas.scene) {
                ui.notifications.notify(`${scene.name} ${game.i18n.localize('ARS.enrichers.loadScene')}`);
                await scene.view();
            } else {
                ui.notifications.notify(`${scene.name} ${game.i18n.localize('ARS.enrichers.loadSceneAlready')}`);
            }
        } else {
            ui.notifications.error(`${game.i18n.localize('ARS.enrichers.loadSceneMissing')} ${sceneId}`);
        }
    }

    static async handleSceneLoadClick(event) {
        // Directly check if the clicked element has the class 'loadscene-link'
        if (!event.target.classList.contains('loadscene-link')) return;
        const sceneId = event.target.getAttribute('data-scene-id');
        if (!sceneId) return;
        event.preventDefault();
        event.stopPropagation();
        this.loadScene(sceneId);
    }

    static loadEnrichers() {
        CONFIG.TextEditor.enrichers.push({
            pattern: /\[\[\/(?<type>loadscene|reference) (?<config>[^\]]+)]](?:{(?<label>[^}]+)})?/gi,
            enricher: async (match, options) => {
                console.log('text-enrichers.js loadEnrichers', { match, options });
                const type = match.groups.type;
                const label = match.groups.label;
                const config = match.groups.config;

                const block = document.createElement('span');
                switch (type) {
                    case 'loadscene':
                        {
                            block.innerHTML += `
                                <a class="loadscene-link" data-action="loadScene" data-scene-id="${config}">
                                <i class="fas fa-image"></i> ${label ?? game.i18n.localize('ARS.enrichers.loadScene')}
                                </a>
                            `;
                        }
                        break;

                    default:
                        {
                            console.error(`Unknown enricher type ${type}`);
                        }
                        break;
                }
                return block;
            },
        });
    }

    static registerCustomEnrichers() {
        this.loadEnrichers();
        // Attach the event listener to the document body for delegation
        document.body.addEventListener('click', this.handleSceneLoadClick.bind(this));
    }
}
