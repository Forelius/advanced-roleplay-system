import { ARS } from '../config.js';
import { ARSItem } from '../item/item.js';
import { ARSActor } from '../actor/actor.js';
import { ARSToken, ARSTokenDocument } from '../token/token.js';
import * as utilitiesManager from '../utilities.js';
import * as dialogManager from '../dialog.js';

/**
 *  Roll extended entries
 */

/**
 * ARS Base Roll
 */
export class ARSRollBase extends Roll {
    /**
     *
     * @param {*} formula
     * @param {*} rollData
     * @param {*} options
     */
    constructor(formula, rollData = {}, options = {}) {
        super(formula, rollData, options);

        this.event = options?.event;
        this.rollData = rollData;
        this.rollOptions = options;
        this.rollMode = game.settings.get('core', 'rollMode');

        this.rawformula = formula;
        this.originalFormula = formula;
        this.variant = parseInt(game.ars.config.settings.systemVariant) ?? 0;
        // console.log('rolls.js class ARSRollBase constructor()', this, { formula, rollData, options });
    }
}

/**
 * ARSRoll defaul
 */
export class ARSRoll extends ARSRollBase {
    /**
     *
     * @param {*} formula
     * @param {*} rollData
     * @param {*} options
     */
    constructor(formula, rollData = {}, options = {}) {
        super(formula, rollData, options);
    }

    static CHAT_TEMPLATE = 'systems/ars/templates/dice/dice-roll.hbs';
    static TOOLTIP_TEMPLATE = 'systems/ars/templates/dice/tooltip.hbs';

    async getTooltip() {
        const parts = this.dice.map((d) => d.getTooltipData());
        // console.log('rolls.js class ARSRoll getTooltip()', { parts }, this);
        return renderTemplate(this.constructor.TOOLTIP_TEMPLATE, { parts, rawformula: this.rawformula });
    }

    /**
     * Render a Roll instance to HTML
     * @param {object} [options={}]               Options which affect how the Roll is rendered
     * @param {string} [options.flavor]             Flavor text to include
     * @param {string} [options.template]           A custom HTML template path
     * @param {boolean} [options.isPrivate=false]   Is the Roll displayed privately?
     * @returns {Promise<string>}                 The rendered HTML template as a string
     */

    async render({ flavor, template = this.constructor.CHAT_TEMPLATE, isPrivate = false } = {}) {
        if (!this._evaluated) await this.evaluate();
        const chatData = {
            formula: isPrivate ? '???' : this._formula,
            flavor: isPrivate ? null : flavor,
            user: game.user.id,
            rawformula: this.rawformula,
            tooltip: isPrivate ? '' : await this.getTooltip(),
            total: isPrivate ? '?' : Math.round(this.total * 100) / 100,
        };

        // console.log('rolls.js class ARSRoll render()', { flavor, template, chatData }, this);
        return renderTemplate(template, chatData);
    }
}

/**
 * ARSRollCombat base class for attack/damage rolls
 */
export class ARSRollCombat extends ARSRollBase {
    /**
     *
     * @param {*} token
     * @param {*} attackObject
     * @param {*} target
     * @param {*} formula
     * @param {*} rollData
     * @param {*} options
     */
    constructor(actorOrToken, attackObject, target, formula, rollData = {}, options = {}) {
        super(formula, rollData, options);

        if (actorOrToken instanceof ARSActor) {
            this.token = actorOrToken?.getToken() || undefined;
            this.actor = actorOrToken;
        } else if (actorOrToken instanceof ARSTokenDocument) {
            this.token = actorOrToken;
            this.actor = actorOrToken.actor;
        } else {
            // ui.notifications.error(`dice.js ARSRollCombat Unknown actor or token instance for [${actorOrToken?.name}]`);
            // throw new Error(`rolls.js ARSRollCombat Unknown actor or token instance for [${actorOrToken?.name}]`);
            this.token = actorOrToken;
            this.actor = actorOrToken.actor;
        }

        // this.actor = token?.actor;
        // this.token = token;

        this.weapon = attackObject instanceof ARSItem ? attackObject : undefined;
        // if it's not a item then it has to be Action.
        this.action = attackObject instanceof ARSItem ? undefined : attackObject;
        this.actionSource = attackObject?.parentuuid ? fromUuidSync(attackObject.parentuuid) : undefined;
        this.attackType = this.weapon ? this.weapon.system.attack.type : this?.action?.type;

        this.targets = game.user.targets;
        this.target = target;
        this.targetActor = target?.actor;
        this.situationalMod = 0;

        this.success = false;
    }

    get isWeapon() {
        return this.weapon && this.weapon.type === 'weapon';
    }
    get hasTarget() {
        return this.target != null;
    }

    /**
     *
     * Insert all the formula into formula/rollData
     *
     * @param {*} formula
     * @param {*} formulatypes
     */
    _insertFormulas(formula, formulaTypes = []) {
        let rollData = foundry.utils.deepClone(this.rollData);
        formulaTypes.forEach((ft) => {
            if (ft && ft?.formula?.length) {
                formula += ' + ' + ft.formula.join(' + ');
                rollData = foundry.utils.mergeObject(rollData, ft.rollData);
            }
        });

        // console.log('rolls.js _insertFormulas', { formula, rollData });
        // rework the formula/terms for the modifiers.
        if (formula) {
            this.rawformula = formula;
            this.terms = this.constructor.parse(this.rawformula, rollData);
            this._formula = this.resetFormula();
        }
    }

    /**
     * Get the formula for checks from effects
     *
     * {Object} target actor
     *
     * @returns { formula: bonusFormula, rollData }
     *
     */
    _getCheckEffectFormula(actor) {
        let bonusFormula = [];
        let rollData, checkFormula, checkModValue;
        const tokenRollData = actor.getRollData();
        if (actor.system?.mods?.check?.formula) {
            bonusFormula.push('@checkFormula');
            checkFormula = utilitiesManager.evaluateFormulaValueSync(actor.system?.mods?.check?.formula, tokenRollData);
        }
        if (actor.system?.mods?.check?.value) {
            bonusFormula.push('@checkModValue');
            checkModValue = parseInt(actor.system?.mods?.check?.value) || 0;
        }
        if (bonusFormula.length) {
            rollData = {
                checkFormula,
                checkModValue,
            };
            return { formula: bonusFormula, rollData };
        }

        return undefined;
    }

    /**
     *
     * Check if token is prof with this.weapon
     *
     * @returns {Boolean}
     */
    isProficientWithWeapon(actor) {
        return (
            actor.isNPC ||
            actor.proficiencies.some((profItem) =>
                Object.values(profItem.system.appliedto).some((weapon) => weapon.id === this.weapon.id)
            )
        );
    }

    /**
     *
     * Return formulas from weapon proficiencies
     *
     * @param {*} mode hit for when determining if not-prof, otherwise ''
     * @returns { formula, rollData }
     */
    getWeaponProficiencyFormula(mode = 'hit') {
        const proficiencyFormula = [];
        let proficiencyRollData = {};

        if (this.isWeapon) {
            // Iterate over proficiencies and calculate modifiers
            for (const profItem of this.actor.proficiencies) {
                for (const weapon of Object.values(profItem.system.appliedto)) {
                    if (weapon.id === this.weapon.id) {
                        const profName = profItem?.name ? utilitiesManager.safeKey(profItem?.name) : undefined;
                        // formula within formula cause issues, such as if "profItem.system.hit" was "@ranks.levels.thief"
                        // so we evaluate formula and add it as a calculated formula under the prof name
                        const weaponHitMod = utilitiesManager.evaluateFormulaValueSync(
                            profItem.system[mode],
                            this.actor.getRollData()
                        );
                        if (profName && weaponHitMod) {
                            const formulaTag = `@${profName}`;
                            if (!proficiencyFormula.includes(formulaTag)) proficiencyFormula.push(formulaTag);
                            // proficiencyFormula.push(`@${profName}`);
                            if (typeof proficiencyRollData[profName] !== 'number') {
                                proficiencyRollData[profName] = 0;
                            }
                            proficiencyRollData[profName] += weaponHitMod;
                        }
                    }
                }
            }

            // Check proficiency with the weapon
            if (mode == 'hit' && !this.isProficientWithWeapon(this.actor)) {
                const profPenalty = parseInt(this.actor.proficiency?.weapon?.penalty, 10) || 0;
                const weaponNameKey = utilitiesManager.safeKey(this.weapon.name);
                if (weaponNameKey) {
                    const profName = `${weaponNameKey}_nonproficient`;
                    const formulaTag = `@${profName}`;
                    if (!proficiencyFormula.includes(formulaTag)) proficiencyFormula.push(formulaTag);
                    // proficiencyFormula.push(`@${profName}`);
                    if (typeof proficiencyRollData[profName] !== 'number') {
                        proficiencyRollData[profName] = 0;
                    }
                    proficiencyRollData[profName] += profPenalty;
                } else {
                    ui.notifications.error(
                        `ARSRollCombat:getWeaponProficiencyFormula: Weapon has no safe key name. [${this.weapon.name}]`
                    );
                }
            }
        }

        if (proficiencyFormula.length) return { formula: proficiencyFormula, rollData: proficiencyRollData };
        else return undefined;
    }

    /**
     * //TODO: make it so the tooltip is obscured for magic on non-ID'd items???
     *
     * Calculates attack bonuses from both a weapon and its ammunition.
     * Returns an object with formula and roll data if any bonuses exist.
     */
    async getWeaponFormulas(mode = 'attack') {
        let bonusFormula = [];
        let ammo,
            ammoMod = 0,
            weaponMod = 0,
            ammoMagic = 0,
            weaponMagic = 0;

        let weaponSource = this.weapon;
        // if this isnt a weapon attack check actionSource
        if (!weaponSource && this.action) {
            if (['melee', 'thrown', 'ranged'].includes(this.attackType) && this.actionSource.type === 'weapon')
                weaponSource = this.actionSource;
        }

        if (weaponSource) {
            try {
                // Get ammo on weapon if it exists
                ammo = await weaponSource.getAmmo(true);
            } catch (error) {
                console.error('Error retrieving ammo data:', error);
                // Handle error or set ammo to a default value if necessary
            }

            // Check if ammo exists and calculate relevant modifiers
            if (ammo) {
                ammoMod = ammo.system[mode].modifier || 0;
                ammoMagic = ammo.system[mode].magicBonus || 0;

                if (ammoMod != '0') bonusFormula.push('@ammoMod');
                if (ammoMagic) bonusFormula.push('@ammoMagic');
            }

            // Calculate weapon modifiers if no ammo or weapon is ranged
            if (!ammo || weaponSource.system.attack.type === 'ranged') {
                weaponMod = mode == 'attack' ? weaponSource.system[mode].modifier || 0 : 0;
                weaponMagic = weaponSource.system[mode].magicBonus || 0;

                if (weaponMod != '0') bonusFormula.push('@weaponMod');
                if (weaponMagic) bonusFormula.push('@weaponMagic');
            }

            // Return structured data if there are any bonuses
            if (bonusFormula.length) {
                return {
                    formula: bonusFormula,
                    rollData: {
                        ammoMod,
                        weaponMod,
                        ammoMagic,
                        weaponMagic,
                    },
                };
            }
        }
        // } else if (this.action) {
        //     // actions only have a damage formula
        //     return undefined;
        // }

        // Return a consistent structure even when there are no bonuses ?
        // return {
        //     formula: [],
        //     rollData: {
        //         ammoMod: 0,
        //         weaponMod: 0,
        //         ammoMagic: 0,
        //         weaponMagic: 0,
        //     },
        // };
        return undefined;
    }
} // end ARSRollCombat
/**
 *
 * Roll Attack
 *
 */
export class ARSRollAttack extends ARSRollCombat {
    /**
     *
     * @param {*} token
     * @param {*} attackObject
     * @param {*} target
     * @param {*} formula
     * @param {*} rollData
     * @param {*} options
     */
    constructor(actorOrToken, attackObject, target, formula, rollData = {}, options = {}) {
        // console.log('rolls.js ARSRollAttack constructor', { actorOrToken, attackObject, target, formula, rollData, options });
        super(actorOrToken, attackObject, target, formula, rollData, options);

        this.thaco = this.actor?.system?.attributes?.thaco?.value ?? 20;

        // this.label = this.weapon ? '' : `(Action) ${this.action?.name}`;
        this.label = '';
        // `<div style="display: inline;" data-type='item' data-id="${this.weapon.id}" class="secure-name">${this.weapon.name}</div>`

        this.targetAcLocation = 'normal';
        this.targetAc = 10;

        this.criticalTarget = this.constructor._CRIT_TARGET;
        this.fumbleTarget = this.constructor._FUMBLE_TARGET;
        this.critical = false;
        this.fumble = false;

        this.isAttack = true;
        // console.log('rolls.js ARSRollAttack constructor', this);
    }

    /** standard crit/fumble targets on d20 */
    static _CRIT_TARGET = 20;
    static _FUMBLE_TARGET = 1;

    /**
     * Get the target ac to hit
     *
     * @returns {Number} targetAc
     */
    _getTargetAc(target) {
        if (target) {
            // const statusEffects = this.getStatusMods(target);
            const targetStatusFormula = target?.actor
                ? target.actor.getStatusFormula('ac', this.attackType, this.token?.actor)
                : undefined;
            let statusAcMod = 0;
            if (targetStatusFormula?.formula) {
                const formula = targetStatusFormula.formula.join(' + ');
                statusAcMod = utilitiesManager.evaluateFormulaValueSync(formula, targetStatusFormula.rollData);
            }
            //TODO: get system.mods.acstatus
            const acstatus = target?.actor.system?.mods?.acstatus?.toLowerCase() || '';
            if (acstatus && this.targetAcLocation === 'normal') {
                if (acstatus.includes('nodex') && acstatus.includes('noshield')) {
                    this.targetAcLocation = 'nodexshield';
                } else if (acstatus.includes('nodex')) {
                    this.targetAcLocation = 'nodex';
                } else if (acstatus.includes('noshield')) {
                    this.targetAcLocation = 'noshield';
                } else if (acstatus.includes('touch')) {
                    this.targetAcLocation = 'touch';
                } else {
                    console.warn(`Unknown system.mods.acstatus on ${target.name}`, { target });
                }
            }
            // we only use tags if attack is normal, we can't stack positional attacks and status tags (nodex/etc)
            if (this.targetAcLocation === 'normal' && targetStatusFormula && targetStatusFormula.acTag) {
                this.targetAcLocation = targetStatusFormula.acTag;
            }
            this.targetAc = target.actor.system.armorClass[this.targetAcLocation];
            switch (this.targetAcLocation) {
                case 'rear':
                    if (['ranged', 'thrown'].includes(this.attackType)) {
                        this.targetAc =
                            target.actor.system.armorClass[`ranged${this.targetAcLocation}`] +
                            parseInt(statusEffects.target.acRanged.mod);
                    } else {
                        // melee is default rear ac
                    }
                    break;

                default: // normal
                    if (['ranged', 'thrown'].includes(this.attackType)) {
                        this.targetAc = target.actor.system.armorClass['ranged'];
                        //  + parseInt(statusAcMod);
                    }
                    break;
            }
            //apply modifiers to AC if status mods exist for it
            if (statusAcMod) this.targetAc += parseInt(statusAcMod) || 0;
        }
        return this.targetAc;
    }

    /**
     *
     * Dialog: Prompt user for situational attack mods
     *
     * @returns { {Number} mod, {String} rollMode, {String} acLocation }
     */
    async _getWeaponAttackSitational() {
        const flavor = this.weapon ? await this.weapon.getStatBlock() : 'Attacking ...';

        const situational = await dialogManager.getAttack(
            game.i18n.localize('ARS.dialog.attack'),
            game.i18n.localize('ARS.dialog.cancel'),
            game.i18n.localize('ARS.dialog.attackdetails'),
            flavor,
            this.weapon,
            this.event
        );
        if (situational) {
            this.targetAcLocation = situational.acLocation ?? 'normal';
            this.situationalMod = situational.mod;
            this.rollMode = situational.rollMode;
        }

        if (situational && situational.mod) return { formula: ['@situational'], rollData: { situational: situational.mod } };
        else return situational;
    }
    /**
     * Get the system.mods.attack attack values
     * @returns
     */
    _getAttackModsFormula() {
        let bonusFormula = [],
            rollData = [];
        if (this.actor.system?.mods?.attack?.value) {
            bonusFormula.push('@mods.attack.value');
        }
        if (this.actor.system?.mods?.attack?.[this.attackType]) {
            bonusFormula.push(`@mods.attack.${this.attackType}`);
        }
        if (bonusFormula.length) return bonusFormula;
        else return undefined;
    }
    /**
     *
     * Get formula for attack modifiers
     *
     * @returns {Array} formula
     */
    _getAbilityScoreFormula() {
        let bonusFormula = [];
        switch (this.attackType) {
            case 'melee':
                if (this.actor.system.abilities.str.hit) bonusFormula.push('@abilities.str.hit');
                break;
            case 'thrown':
            case 'ranged':
                if (this.attackType === 'thrown' && this.actor.system.abilities.str.hit)
                    bonusFormula.push('@abilities.str.hit');
                if (this.actor.system.abilities.dex.missile) bonusFormula.push('@abilities.dex.missile');
                break;
            default:
                break;
        }
        if (bonusFormula.length) return bonusFormula;
        else return undefined;
    }

    /**
     *
     *
     * Helper functions to get weapon v armor
     *
     * Return a modifier formula when using a weapon type versus a armor type
     *
     *
     * @param {Object} sourceActor attacker
     * @param {Object} targetActor target
     * @param {Object} weaponUsed weapon item used to attack
     * @returns
     */
    _getWVAMod(sourceActor, targetActor, weaponUsed, actionDmgType = undefined) {
        // console.log('_getWVAMod', { sourceActor, targetActor, weaponUsed });

        if (weaponUsed && weaponUsed.type === 'weapon') actionDmgType = weaponUsed.system.damage.type;

        // Validate the input to check if all required variables are present
        function isValidInput(sourceActor, targetActor) {
            return sourceActor && targetActor;
        }

        // Find worn armor for the target actor
        function findArmor(targetActor) {
            return targetActor.armors.find((armor) => armor.isWornArmor);
        }

        // Find worn shield for the target actor
        function findShield(targetActor) {
            return targetActor.armors.find((shield) => shield.isWornShield);
        }

        // Calculate damage modifier formula for variant 1
        function getFormulaForVariant1(weaponUsed, armorWorn, shieldWorn) {
            // Get weapon style or default to empty string
            if (!weaponUsed || weaponUsed.type !== 'weapon') return '';

            const weaponStyle = weaponUsed.system?.weaponstyle ?? '';

            // Return empty formula if no armor is worn or weapon style is missing
            if (!armorWorn || !weaponStyle) {
                return 0;
            }

            // Calculate armor class type considering the worn shield
            let acType = shieldWorn ? armorWorn.system.protection.ac - 1 : armorWorn.system.protection.ac;
            acType = Math.max(0, Math.min(10, acType));

            // Fetch damage modifier based on variant, weapon style, and armor class type
            const variant = 1;
            const dmgMod = game.ars.config.weaponVarmor[variant]?.[weaponStyle]?.[acType] ?? 0;

            // Return damage modifier or empty string if no modifier is found
            return dmgMod;
        }

        // Calculate damage modifier formula for variant 2
        function getFormulaForVariant2(armorWorn, damageType) {
            // Return empty formula if no armor is worn
            if (!armorWorn || !damageType) {
                return 0;
            }

            // Get armor style or default to empty string
            const armorStyle = armorWorn.system?.armorstyle ?? '';
            if (!armorStyle) {
                return 0;
            }

            // Fetch damage modifier based on variant, armor style, and damage type
            const variant = 2;
            const dmgMod = game.ars.config.weaponVarmor[variant]?.[armorStyle]?.[damageType] ?? 0;

            // Return damage modifier or empty string if no modifier is found
            return dmgMod;
        }

        // Check input validity before proceeding
        if (!isValidInput(sourceActor, targetActor)) {
            return '';
        }

        // Fetch the game variant and worn armor and shield
        const variant = parseInt(game.ars.config.settings.systemVariant);
        const wVaEnabled = game.settings.get('ars', 'weaponVarmor');
        const armorWorn = findArmor(targetActor);
        const shieldWorn = findShield(targetActor);

        // Calculate attack modifier based on the game variant
        if (wVaEnabled) {
            if (variant <= 1) {
                return getFormulaForVariant1(weaponUsed, armorWorn, shieldWorn);
            } else if (variant === 2) {
                return getFormulaForVariant2(armorWorn, actionDmgType);
            }
        }

        return 0;
    }

    /**
     *
     * Get the formula for weapon versus armor
     *
     * @param {*} target
     * @returns { {Array} formula, {Object} rollData }
     */
    _getWeaponVersusArmorFormula(target) {
        let bonusFormula = [];
        let wvaMod = 0;

        function _actionWvADamageTypeHelper() {
            // Normalize the action name by converting it to lower case and trimming whitespace
            const aName = this.action.name.toLowerCase().trim();
            // Create a regular expression to match piercing attack types ('bite', 'horn', 'talon')
            const pierceRegex = /(?:arrow|bolt|spear|bite|horn|pierc|stab|poke|talon)/;
            // Create a regular expression to match bludgeoning attack types ('tail', 'fist', 'rock', 'stone', 'smash')
            const bludgeonRegex = /(?:buffet|bludgeon|club|hammer|mace|staff|crush|tail|fist|punch|rock|stone|smash|slam)/;
            // dont need a huge list here, default is slash. adding "slash" to be able to enforce slashing
            const slashRegex = /(?:slash)/;

            if (slashRegex.test(aName)) {
                wvAdmgType = 'slashing';
            } else if (pierceRegex.test(aName)) {
                wvAdmgType = 'piercing';
            } else if (bludgeonRegex.test(aName)) {
                wvAdmgType = 'bludgeoning';
            }
            // default is slashing
        }
        if (target) {
            switch (this.variant) {
                case 0:
                case 1:
                    {
                        wvaMod = this._getWVAMod(this.actor, target.actor, this.weapon);
                    }
                    break;

                case 2:
                    {
                        let wvAdmgType = 'slashing';
                        if (this.weapon) {
                            wvAdmgType = this.weapon.system?.damage?.type ?? 'slashing';
                        } else if (this.action) {
                            wvAdmgType = _actionWvADamageTypeHelper.bind(this);
                            // default is slashing
                        }
                        wvaMod = this._getWVAMod(this.actor, target.actor, this.weapon, wvAdmgType);
                    }
                    break;

                default:
                    break;
            }
            if (wvaMod) bonusFormula.push('@WvsA');
        }
        if (wvaMod) return { formula: bonusFormula, rollData: { WvsA: wvaMod } };
        return undefined;
    }

    /**
     *
     * Get ranged attack modifiers
     *
     * @param {*} target
     * @returns { formula, rollData }
     */
    async _getRangedAttackFormula(target) {
        let rangeModifier = 0;
        let bonusFormula = [];
        //check range and apply @range
        if (target?.document && ['ranged', 'thrown'].includes(this.attackType)) {
            const distance = this.token.document.getDistance(target.document);
            const combatAutomateRangeMods = game.settings.get('ars', 'combatAutomateRangeMods');
            if (combatAutomateRangeMods && distance && this.weapon) {
                const range = await this.weapon.getRange();
                // ARS.rangeModifiers.short/medium/long
                if (distance >= 0 && distance <= range.short) {
                    rangeModifier = ARS.rangeModifiers.short;
                } else if (distance >= range.short && distance <= range.medium) {
                    rangeModifier = ARS.rangeModifiers.medium;
                } else if (distance >= range.medium && distance <= range.long) {
                    rangeModifier = ARS.rangeModifiers.long;
                } else if (distance > range.long) {
                    rangeModifier = ARS.rangeModifiers.long;
                }
                if (rangeModifier) bonusFormula.push('@range');
            }
        }
        if (rangeModifier) return { formula: bonusFormula, rollData: { range: rangeModifier } };
        else return undefined;
    }

    async getAttackMods(forceSkip = false) {
        // console.log('rolls.js getAttackMods() this', this);

        const situational = this.options.skipSitiational || forceSkip ? undefined : await this._getWeaponAttackSitational();
        if (!this.options.skipSitiational && !forceSkip && situational && situational.cancel) return undefined;

        const abilityFormula = this._getAbilityScoreFormula();
        const attackModsFormula = this._getAttackModsFormula();
        const profFormula = this.getWeaponProficiencyFormula('hit');
        const weaponFormula = await this.getWeaponFormulas('attack');
        const wvaFormula = this._getWeaponVersusArmorFormula(this.target);
        const rangedFormula = await this._getRangedAttackFormula(this.target);
        const selfStatusFormula = this.actor.getStatusFormula('attack', this.attackType, this.target?.actor);
        const targetStatusFormula = this?.target?.actor
            ? this.target.actor.getStatusFormula('attacked', this.attackType, this.token?.actor)
            : undefined;

        const targetAc = this._getTargetAc(this.target);

        const triggerTargetFormula = await utilitiesManager.getTriggerFormula(
            this.actor,
            this.targetActor,
            'target',
            this.attackType,
            'attack',
            this.actionSource ? this.actionSource : this.weapon
        );
        const triggerAttackerFormula = await utilitiesManager.getTriggerFormula(
            this.targetActor,
            this.actor,
            'attacker',
            this.attackType,
            'attack',
            this.actionSource
        );

        // console.log('rolls.js getAttackMods()', {
        //     situational,
        //     abilityFormula,
        //     weaponFormula,
        //     profFormula,
        //     wvaFormula,
        //     rangedFormula,
        //     triggerTargetFormula,
        //     triggerAttackerFormula,
        //     selfStatusFormula,
        //     targetStatusFormula,
        // });

        // build formula and rollData for calculated adjustments
        // Initialize formula with abilityFormula or empty string
        let formula = '';
        if (abilityFormula?.length) formula = abilityFormula.join(' + ') || '';
        // Define an array of formula types
        const formulaTypes = [
            situational,
            weaponFormula,
            profFormula,
            wvaFormula,
            rangedFormula,
            triggerTargetFormula,
            triggerAttackerFormula,
            selfStatusFormula,
            targetStatusFormula,
        ];

        // Build formula string and merge rollData
        let rollData = foundry.utils.deepClone(this.rollData);
        formulaTypes.forEach((ft) => {
            if (ft && ft?.formula?.length) {
                formula += (formula ? ' + ' : '') + ft.formula.join(' + ');
                rollData = foundry.utils.mergeObject(rollData, ft.rollData);
            }
        });

        if (attackModsFormula) {
            formula += (formula ? ' + ' : '') + attackModsFormula.join(' + ');
        }

        // rework the formula/terms for the modifiers.
        if (formula) {
            this.rawformula += (formula ? ' + ' : '') + formula;
            this.terms = this.constructor.parse(this.rawformula, rollData);
            this._formula = this.resetFormula();
        }
        // console.log('rolls.js getAttackMods  this', this);

        return this;
    }

    /**
     * Roll attack with weapon
     *
     * @returns {Object} roll
     */
    async rollAttack() {
        const roll = await this.evaluate({ rollMode: this.rollMode });

        const acHit = this.actor.acHit(this.total);
        const naturalRoll = this.dice[0].total;

        this.fumble = naturalRoll == (this.fumbleTarget || 1);
        this.critical = naturalRoll == (this.criticalTarget || 20);

        this.success = acHit <= this.targetAc;
        if (this.variant == 2 && game.ars.config.settings.autohitfail) {
            if (naturalRoll == 1) this.success = false;
            if (naturalRoll == 20) this.success = true;
        }
        this.acHit = acHit;

        // if (game.dice3d) await game.dice3d.showForRoll(roll, game.user, utilitiesManager.diceRollModeVisibility(this.rollMode));
        return roll;
    }

    /**
     * Roll attack with action
     *
     * @returns {Object} roll
     */
    // async rollActionAttack() {
    //     const roll = await this.evaluate({ rollMode: this.rollMode });

    //     const acHit = this.actor.acHit(this.total);
    //     const naturalRoll = this.dice[0].total;

    //     this.fumble = naturalRoll == (this.fumbleTarget || 1);
    //     this.critical = naturalRoll == (this.criticalTarget || 20);

    //     this.success = acHit <= this.targetAc;
    //     if (this.variant == 2 && game.ars.config.settings.autohitfail) {
    //         if (naturalRoll == 1) this.success = false;
    //         if (naturalRoll == 20) this.success = true;
    //     }
    //     this.acHit = acHit;

    //     if (game.dice3d) await game.dice3d.showForRoll(roll, game.user, true);
    //     return roll;
    // }
}

export class ARSRollDamage extends ARSRollCombat {
    /**
     *
     * @param {*} token
     * @param {*} attackObject
     * @param {*} target
     * @param {*} formula
     * @param {*} rollData
     * @param {*} options
     */
    constructor(actorOrToken, attackObject, target, formula, rollData = {}, options = {}) {
        super(actorOrToken, attackObject, target, formula, rollData, options);

        this.formulas = []; // weapons/actions can have additional damage formula attached.

        this.label = this.weapon
            ? `<div style="display: inline;" data-type='item' data-uuid="${this.weapon.uuid}" data-id="${this.weapon.id}" class="secure-name">${this.weapon.name}</div> attack`
            : `(Action) ${this.action?.name}`;

        this.damageStyle = 'normal';
        // console.log('rolls.js ARSRollDamage constructor', this);
    }

    /**
     *
     * Dialog: Prompt user for situational attack mods
     *
     * @returns { {Number} mod, {String} rollMode, {String} acLocation }
     */
    async _getWeaponDamageSitational(isDamage = true) {
        const flavor = this.weapon ? await this.weapon.getStatBlock() : this.action ? `${this.action.name}` : 'Damaging ...';

        const situational = await dialogManager.getDamage(
            isDamage ? game.i18n.localize('ARS.dialog.damage') : game.i18n.localize('ARS.dialog.heal'),
            game.i18n.localize('ARS.dialog.cancel'),
            isDamage ? game.i18n.localize('ARS.dialog.damagedetails') : game.i18n.localize('ARS.dialog.healdetails'),
            flavor,
            this.event
        );
        if (situational) {
            this.damageStyle = situational.dmgAdjustment ?? '';
            this.situationalMod = situational.mod;
            this.rollMode = situational.rollMode;
        }

        if (situational && situational.mod) return { formula: ['@situational'], rollData: { situational: situational.mod } };
        else return situational;
    }
    _getDamageModsFormula() {
        let bonusFormula = [];
        if (this.actor.system?.mods?.damage?.value) bonusFormula.push('@mods.damage.value');
        if (this.actor.system?.mods?.damage?.[this.attackType]) bonusFormula.push(`@mods.damage.${this.attackType}`);
        if (bonusFormula.length) return bonusFormula;
        else return undefined;
    }

    /**
     *
     * Get formula for damage modifiers
     *
     * @returns {Array} formula
     */
    _getAbilityScoreFormula() {
        let bonusFormula = [];
        switch (this.attackType) {
            case 'damage': // damage is what actions come in as
                if (this.action.ability !== 'str') return undefined;
            case 'melee':
            case 'thrown':
                if (this.actor.system.abilities.str.dmg) bonusFormula.push('@abilities.str.dmg');
                break;
            default:
                break;
        }
        if (bonusFormula.length) return bonusFormula;
        else return undefined;
    }

    /**
     *
     * Get the roll damage formula for this weapon
     *
     * @param {*} target
     */
    _getWeaponDamageFormula(target) {
        let damageType = '';
        let formula = '1d1';
        const damageSize = this.options?.event?.currentTarget?.dataset?.damageSize || 'medium';
        if (this.weapon) {
            // if we have ammo, use ammo stats for damage, otherwise use weapon
            const weapon =
                this.weapon.ammo && this.weapon.ammo?.system?.damage?.normal
                    ? this.weapon.ammo.system.damage
                    : this.weapon.system.damage;
            const largeFormula = weapon.large;
            const normalFormula = weapon.normal;
            formula = normalFormula;
            if ((largeFormula && target && target.actor.isLarge) || (!target && damageSize === 'large')) {
                formula = largeFormula;
            }
            damageType = weapon.type;
        } else if (this.action) {
            formula = this.action.formula;
            damageType = this.action.damagetype;
        } else {
            ui.notifications.error('rolls.js _getWeaponDamageFormula: No weapon or action for damage.');
            console.error('rolls.js _getWeaponDamageFormula: No weapon or action for damage.');
        }
        return [{ formula, damageType }];
    }

    _getAdditionalDamageFormula() {
        if (!this.weapon && !this.action) {
            return [{ formula: this.originalFormula }];
        }
        let formulas = [];
        const otherDmg = this.weapon ? this.weapon.system.damage.otherdmg : this.action.otherdmg;
        for (const key in otherDmg) {
            // Check if the property actually exists on the object
            if (otherDmg.hasOwnProperty(key)) {
                const entry = otherDmg[key];
                if (entry.formula) {
                    formulas.push({ formula: entry.formula, damageType: entry.type });
                }
            }
        }
        return formulas;
    }

    _getAllDamageFormulas(target) {
        if (!this.weapon && !this.action) {
            return [{ formula: this.originalFormula }];
        }
        const weaponDamage = this._getWeaponDamageFormula(target);
        const additionalDamage = this._getAdditionalDamageFormula();
        const formulas = weaponDamage.concat(additionalDamage);
        this.formulas = formulas;
        return formulas;
    }

    #extractMultiplier(str) {
        const match = str?.match(/x(\d)/);
        if (match) {
            return match[1];
        } else {
            return null;
        }
    }

    /**
     * Get damage formula and all modifiers for weapon damage
     */
    async getDamageMods(isDamage = true) {
        const situational = this.options.skipSitiational ? undefined : await this._getWeaponDamageSitational(isDamage);
        if (!this.options.skipSitiational && situational && situational.cancel) return undefined;

        const weaponFormula = await this.getWeaponFormulas('damage');

        const abilityFormula = this._getAbilityScoreFormula();

        const damageModsFormula = this._getDamageModsFormula();

        const profFormula = this.getWeaponProficiencyFormula('damage');

        const selfStatusFormula = this.actor.getStatusFormula('damage', '', this.target?.actor);

        const targetStatusFormula = this?.target?.actor
            ? this.target.actor.getStatusFormula('damage', '', this.token?.actor)
            : undefined;

        const triggerTargetFormula = await utilitiesManager.getTriggerFormula(
            this.actor,
            this.targetActor,
            'target',
            '',
            'damage',
            this.actionSource ? this.actionSource : this.weapon
        );
        const triggerAttackerFormula = await utilitiesManager.getTriggerFormula(
            this.targetActor,
            this.actor,
            'attacker',
            '',
            'damage',
            this.actionSource
        );

        const formulas = this._getAllDamageFormulas(this.target);

        // build formula and rollData for calculated adjustments
        let formula = formulas[0].formula;
        let multiplier = this.#extractMultiplier(this.damageStyle);
        if (this.damageStyle === 'half') multiplier = 0.5;
        // if there is a multiplier we only apply it to dice roll, no other mods get multiplied
        if (multiplier) formula = `floor((${formula})*${multiplier})`;
        // Initialize formula with abilityFormula or empty string
        if (abilityFormula?.length) formula += ' + ' + abilityFormula.join(' + ') || '';

        // console.log('rolls.js getDamageMods', {
        //     situational,
        //     weaponFormula,
        //     profFormula,
        //     triggerTargetFormula,
        //     triggerAttackerFormula,
        //     selfStatusFormula,
        //     targetStatusFormula,
        // });

        // Define an array of formula types
        const formulaTypes = [
            situational,
            weaponFormula,
            profFormula,
            triggerTargetFormula,
            triggerAttackerFormula,
            selfStatusFormula,
            targetStatusFormula,
        ];

        // Build formula string and merge rollData
        let rollData = foundry.utils.deepClone(this.rollData);
        formulaTypes.forEach((ft) => {
            if (ft && ft?.formula?.length) {
                formula += ' + ' + ft.formula.join(' + ');
                rollData = foundry.utils.mergeObject(rollData, ft.rollData);
            }
        });

        if (damageModsFormula) {
            formula += ' + ' + damageModsFormula.join(' + ');
        }

        // rework the formula/terms for the modifiers.
        if (formula) {
            this.rawformula = formula;
            this.terms = this.constructor.parse(this.rawformula, rollData);
            this._formula = this.resetFormula();
        }
        // console.log('rolls.js getDamageMods  this', this, { formula });

        return this;
    }

    async rollDamage(isDamage = true) {
        const damagedRolled = await this.getDamageMods(isDamage);
        if (!damagedRolled) return undefined;

        let rollOptions = { rollMode: this.rollMode, maximize: this.damageStyle === 'max' };

        if (this.damageStyle === 'double') this.alter(2, 0);
        const roll = await this.roll(rollOptions);
        if (game.dice3d) await game.dice3d.showForRoll(roll, game.user, utilitiesManager.diceRollModeVisibility(this.rollMode));

        return roll;
    }
}

export class ARSRollSave extends ARSRollCombat {
    /**
     *
     * Resolve Save checks
     *
     * @param {*} token
     * @param {*} action
     * @param {*} target
     * @param {*} formula
     * @param {*} rollData
     * @param {*} options
     */
    constructor(actorOrToken, action, target, formula, rollData = {}, options = {}) {
        // console.log('rolls.js ARSRollSave constructor', { actorOrToken, action, target, formula, rollData, options });
        super(actorOrToken, action, target, formula, rollData, options);
        this.saveType = typeof action === 'string' ? action : action?.saveCheck?.type;
        if (typeof action === 'string') {
            this.label = `${game.i18n.localize('ARS.save')}:${game.i18n.localize(`ARS.saveTypes.${this.saveType}`)}`;
        } else {
            this.label = game.i18n.localize('ARS.save') + ':' + game.i18n.localize(`ARS.saveTypes.${this.saveType}`);
        }
        this.isSave = true;

        // console.log('rolls.js ARSRollSave constructor', this);
    }

    _getActionSaveFormula() {
        let bonusFormula = [],
            rollData;
        const formulaName = 'actionSave';
        // const savingSource = this.target ? this.target.actor : this.actor;
        const savingSource = this.actor;
        if (this.action?.saveCheck?.formula) {
            const actionSave = utilitiesManager.evaluateFormulaValueSync(
                this.action.saveCheck.formula,
                savingSource.getRollData()
            );

            bonusFormula.push(`@${formulaName}`);
            rollData = { actionSave };
        }

        if (bonusFormula.length) return { formula: bonusFormula, rollData };
        else return undefined;
    }
    _getSaveEffectFormula() {
        let bonusFormula = [],
            rollData;
        if (this.saveType !== 'none') {
            const bonusSaveEffectFormulas = [];
            const savingSource = this.target ? this.target.actor : this.actor;
            const effectSaveFormulas = savingSource.getSaveFormulaFromEffects(this.saveType, this.action, this.actionSource);
            if (effectSaveFormulas.length) {
                for (const saveFormula of effectSaveFormulas) {
                    const formulaName = utilitiesManager.safeKey(saveFormula.name);
                    const formulaValue = utilitiesManager.evaluateFormulaValueSync(
                        saveFormula.formula,
                        savingSource.getRollData()
                    );
                    bonusSaveEffectFormulas[formulaName] = formulaValue;
                    bonusFormula.push(`@${formulaName}`);
                }
            }
            rollData = {
                ...bonusSaveEffectFormulas,
            };
        } // end saveType

        if (bonusFormula.length) return { formula: bonusFormula, rollData };
        else return undefined;
    }

    async _getSaveSitational() {
        const situational = await dialogManager.getSituational(
            -1000,
            1000,
            0,
            game.i18n.localize('ARS.modifier'),
            game.i18n.localize('ARS.dialog.savedetails'),
            this.label,
            this.event
        );
        if (situational) {
            this.situationalMod = situational.mod;
            this.rollMode = situational.rollMode;
        }

        if (situational && situational.mod) return { formula: ['@situational'], rollData: { situational: situational.mod } };
        else return situational;
    }

    /**
     * Get all modifiers for save
     *
     * @param {Boolean} forceSkip skip situational mod dialog
     */
    async getMods(forceSkip = false) {
        const situational = this.options.skipSitiational || forceSkip ? undefined : await this._getSaveSitational();
        if (!this.options.skipSitiational && !forceSkip && situational && situational.cancel) return undefined;

        const saveActionFormula = this._getActionSaveFormula();

        const saveEffectFormula = await this._getSaveEffectFormula();

        const saveSelfStatusFormula = this?.token
            ? this.token.actor.getStatusFormula('save', this.saveType, this.target?.actor)
            : this?.actor
            ? this.actor.getStatusFormula('save', this.saveType, this.target?.actor)
            : '';

        const saveTargetStatusFormula = this?.target?.actor
            ? this.target.actor.getStatusFormula('save', this.saveType, this.token?.actor)
            : undefined;

        const triggerTargetFormula = await utilitiesManager.getTriggerFormula(
            this.actor,
            this.targetActor,
            'target',
            this.saveType,
            'save',
            this.actionSource
        );
        const triggerAttackerFormula = await utilitiesManager.getTriggerFormula(
            this.targetActor,
            this.actor,
            'attacker',
            this.saveType,
            'save',
            this.actionSource
        );

        // console.log('rolls.js ARSRollSave getMods()', {
        //     situational,
        //     triggerTargetFormula,
        //     triggerAttackerFormula,
        //     saveEffectFormula,
        //     saveSelfStatusFormula,
        //     saveTargetStatusFormula,
        // });

        // build formula and rollData for calculated adjustments
        let formula = this.formula;

        // Define an array of formula types
        const formulaTypes = [
            situational,
            saveActionFormula,
            triggerTargetFormula,
            triggerAttackerFormula,
            saveEffectFormula,
            saveSelfStatusFormula,
            saveTargetStatusFormula,
        ];

        // Build formula string and merge rollData
        let rollData = foundry.utils.deepClone(this.rollData);
        formulaTypes.forEach((ft) => {
            if (ft && ft?.formula?.length) {
                formula += ' + ' + ft.formula.join(' + ');
                rollData = foundry.utils.mergeObject(rollData, ft.rollData);
            }
        });

        // rework the formula/terms for the modifiers.
        if (formula) {
            this.rawformula = formula;
            this.terms = this.constructor.parse(this.rawformula, rollData);
            this._formula = this.resetFormula();
        }
        // console.log('rolls.js ARSRollSave getMods  this', this, { formula });
        return this;
    }

    async rollSave() {
        let rollOptions = { rollMode: this.rollMode };
        const roll = await this.roll(rollOptions);
        // if (game.dice3d) await game.dice3d.showForRoll(roll, game.user, utilitiesManager.diceRollModeVisibility(this.rollMode));

        this.checkTarget = this.target?.actor
            ? this.target.actor.system.saves[this.saveType].value
            : this.actor.system.saves[this.saveType].value;
        const naturalRoll = this.dice[0].total;

        this.fumble = naturalRoll == 1;
        this.critical = naturalRoll == 20;
        this.success = roll.total >= this.checkTarget;
        if (this.variant == 1) {
            if (this.fumble) this.success = false;
        }

        this.checkDiff = Math.abs(roll.total - this.checkTarget);

        return roll;
    }
}

export class ARSRollSkill extends ARSRollCombat {
    /**
     *
     * Resolve skill
     *
     * @param {*} token
     * @param {*} skill
     * @param {*} target
     * @param {*} formula
     * @param {*} rollData
     * @param {*} options
     *
     */
    constructor(actorOrToken, skill, target, formula, rollData = {}, options = {}) {
        // console.log('rolls.js ARSRollCheck constructor', { actorOrToken, skill, target, formula, rollData, options });
        formula = skill ? skill.system.features.formula : '';
        super(actorOrToken, undefined, target, formula, rollData, options);
        this.label = `Skill: ${skill?.name}`;
        this.skill = skill;
        if (skill) {
            this.abilityCheck = skill.system.features.ability !== 'none';
            this.safekey = utilitiesManager.safeKey(skill.name);
            this.checkTarget = this.abilityCheck
                ? this.actor.system.abilities[skill.system.features.ability].value
                : skill.system.features.target;
            this.ascending = skill.system.features.type === 'ascending';
            this.descending = skill.system.features.type !== 'ascending';
        }
        this.isSkill = true;

        // console.log('rolls.js ARSRollCheck constructor', this);
    }

    /**
     *
     * Returns all the modifiers from the skill items itself
     *
     * @returns
     */
    _getSkillModFormula() {
        let bonusFormula = [];
        let rollData = {};
        const skill = this.skill;
        const isRogueSkill = skill?.getIsRogueSkill;

        ['class', 'background', 'ability', 'armor', 'item', 'race', 'other'].forEach((modType) => {
            if (this.skill.system.features.modifiers[modType]) {
                const formulaKey = `skill_${modType}`;
                const formulaValue = parseInt(this.skill.system.features.modifiers[modType]) || 0;

                bonusFormula.push(`@${formulaKey}`);
                rollData[formulaKey] = formulaValue;
            }
        });

        // advanced_automation: dex/armor based modifiers for thief skills - only fall into this if skill is 'thief' grouped
        if (isRogueSkill) this._thiefSkillsDexAndArmorAdjustments(bonusFormula, rollData);

        const modFormula = this.skill.system.features.modifiers.formula ? this.skill.system.features.modifiers.formula : 0;
        let modRoll = modFormula ? utilitiesManager.evaluateFormulaValueSync(modFormula, this.actor.getRollData()) : '';
        if (modRoll) {
            bonusFormula.push('@skillModFormula');
            rollData['skillModFormula'] = modRoll;
        }

        if (bonusFormula.length) {
            return { formula: bonusFormula, rollData };
        }
    }

    // advanced_automation: thief skills: extended automation for dex/armor based modifiers (table 28/29 phb)
    _thiefSkillsDexAndArmorAdjustments(bonusFormula, rollData) {
        const actor = this.actor;
        const skill = this.skill;
        const tblDexMods = ARS.thiefSkillDexAdjustments[this.variant];
        const tblArmorMods = ARS.thiefNoArmorAdjustments[this.variant];
        const skillName = skill.name.toLowerCase();
        const skillIndex = tblDexMods[0]?.indexOf(skillName);

        // table 28 - thieving skill dexterity adjustments
        let dex = actor.system.abilities.dex.value;
        if (dex > 19) dex = 19;
        const dexMod = tblDexMods[dex][skillIndex];
        if (dexMod) {
            const formulaKey = `rogue_dex_mod`;
            const formulaValue = dexMod;
            bonusFormula.push(`@${formulaKey}`);
            rollData[formulaKey] = formulaValue;
        }

        // table 29 - thieving skill armor adjustments - check if 'no amor' is worn and apply bonus to attempt of thief skill
        // note: penalties for leather/elven chain need to be setup on the armor itself
        const noArmor = !actor.wearingArmor;
        const row = tblArmorMods[skillName];

        // no armor
        if (noArmor) {
            const armorMod = row[0];
            const formulaKey = `rogue_armor_mod`;
            const formulaValue = armorMod;
            bonusFormula.push(`@${formulaKey}`);
            rollData[formulaKey] = formulaValue;
        }
    }

    _getSkillFormulaFromEquipment() {
        const eqFormula = this.actor.getEquipmentSkillModsFormula(this.skill.name);
        if (eqFormula?.formula?.length) return { ...eqFormula };
        else return undefined;
    }

    _getSkillEffectFormula() {
        let bonusFormula = [];
        let rollData = {};
        let checkNameFormulaValue;
        const tokenRollData = this.actor.getRollData();
        if (this.actor.system.mods.skill?.[this.safekey]) {
            bonusFormula.push(`@${this.safekey}`);
            checkNameFormulaValue = utilitiesManager.evaluateFormulaValueSync(
                this.actor.system.mods.skill?.[this.safekey],
                tokenRollData
            );
            rollData[this.safekey] = checkNameFormulaValue;
        }
        if (bonusFormula.length) return { formula: bonusFormula, rollData };
        else return undefined;
    }

    async _getSitational() {
        const situational = await dialogManager.getSituational(
            -1000,
            1000,
            0,
            game.i18n.localize('ARS.modifier'),
            game.i18n.localize('ARS.dialog.skilldetails'),
            this.label,
            this.event
        );
        if (situational) {
            this.situationalMod = situational.mod;
            this.rollMode = situational.rollMode;
        }

        if (situational && situational.mod) return { formula: ['@situational'], rollData: { situational: situational.mod } };
        else return situational;
    }

    /**
     *
     * Get mods for ARSRollSkill
     *
     * @param {*} forceSkip
     * @returns
     */
    async getMods(forceSkip = false) {
        const situational = this.options.skipSitiational || forceSkip ? undefined : await this._getSitational();
        if (!this.options.skipSitiational && !forceSkip && situational && situational.cancel) return undefined;

        const skillEffectFormula = this._getSkillEffectFormula();

        const checkEffectFormula = this._getCheckEffectFormula(this.actor);

        const skillModFormula = this._getSkillModFormula();

        const skillFormulaFromEquipment = this._getSkillFormulaFromEquipment();

        // console.log('rolls.js ARSRollSkill getMods()', {
        //     situational,
        //     checkEffectFormula,
        //     skillEffectFormula,
        //     skillModFormula,
        //     skillFormulaFromEquipment,
        // });

        let rollFormula = this.formula;
        let formula = '';
        const formulaTypes = [situational, checkEffectFormula, skillEffectFormula, skillModFormula, skillFormulaFromEquipment];
        let rollData = foundry.utils.deepClone(this.rollData);

        // special
        const isRogueSkill = this.skill?.getIsRogueSkill;
        let rogueSkillTotalTarget = 0;
        let rogueSkillRollBreakdown = [];

        // build string formulas for roll
        formulaTypes.forEach((ft) => {
            if (ft && ft?.formula?.length) {
                formula += ' + ' + ft.formula.join(' + ');
                rollData = foundry.utils.mergeObject(rollData, ft.rollData);

                // when rolling a rogue skill, build a special breakdown summary AND the target number to beat (BASE + All modifiers)
                if (isRogueSkill) {
                    for (const [key, value] of Object.entries(ft.rollData)) {
                        rogueSkillRollBreakdown.push(`@${key}: ${value}`);
                    }
                    const sumValues = Object.values(ft.rollData).reduce((a, b) => a + b, 0);
                    rogueSkillTotalTarget += sumValues;
                }
            }
        });

        // final string formula
        if (formula) {
            formula = `${rollFormula}${this.descending ? ` -(${formula})` : formula}`;
        }

        // parse the formula/terms for the modifiers
        if (formula) {
            this.rawformula = formula;
            this.terms = this.constructor.parse(isRogueSkill ? `1d100` : formula, rollData);
            this._formula = this.resetFormula();

            // special - if skill check for rogue skills the check target is total modifiers + base score in the given skill
            if (isRogueSkill) {
                // adjust final check target value and the string tooltip for the breakdown
                const base = +this.skill.system.features.target;
                this.checkTarget = rogueSkillTotalTarget + base;
                this.rawformula = rogueSkillRollBreakdown.join(' + ');
            }
        }

        // console.log('rolls.js ARSRollSave getMods  this', this, { formula });
        return this;
    }

    async rollSkill() {
        let rollOptions = { rollMode: this.rollMode };
        const roll = await this.roll(rollOptions);
        // if (utilitiesManager.diceRollShowSelf(this.rollMode)) {
        //     if (game.dice3d)
        //         await game.dice3d.showForRoll(roll, game.user, utilitiesManager.diceRollModeVisibility(this.rollMode));
        // }

        const naturalRoll = this.dice[0].total;

        this.checkTargetFormula = this.checkTarget; // save this incase we want to display in chat card
        // convert the checkTarget to a evaluated number.
        this.checkTarget = await utilitiesManager.evaluateFormulaValue(this.checkTarget, this.actor.getRollData());
        this.fumble = naturalRoll == 1;
        this.critical = naturalRoll == 20;
        this.success = this.ascending ? roll.total >= this.checkTarget : roll.total <= this.checkTarget;
        this.checkDiff = Math.abs(roll.total - this.checkTarget);
        return roll;
    }
}

export class ARSRollAbilityCheck extends ARSRollCombat {
    /**
     * Make an ability check (not using a skill item)
     *
     * @param {Object} actorOrToken
     * @param {String} abilityType (str, dex, con/etc)
     * @param {*} target
     * @param {*} formula
     * @param {*} rollData
     * @param {*} options
     */
    constructor(actorOrToken, abilityType, target, formula, rollData = {}, options = {}) {
        // console.log('rolls.js ARSRollAbilityCheck constructor', {
        //     actorOrToken,
        //     abilityType,
        //     target,
        //     formula,
        //     rollData,
        //     options,
        // });
        super(actorOrToken, undefined, target, formula, rollData, options);
        this.abilityType = abilityType;
        this.label = `Ability Check: ${game.i18n.localize(`ARS.abilityTypes.${abilityType}`)}`;
        this.checkTarget =
            (this.target?.actor
                ? this.target.actor.system.abilities[abilityType].value
                : this.actor?.system?.abilities[abilityType]?.value) || '10';

        // console.log('rolls.js ARSRollAbilityCheck constructor', this);
        this.isAbilityCheck = true;
    }

    async _getSitational() {
        const situational = await dialogManager.getSituational(
            -1000,
            1000,
            0,
            game.i18n.localize('ARS.modifier'),
            game.i18n.localize('ARS.dialog.abilitycheckdetails'),
            this.label,
            this.event
        );
        if (situational) {
            this.situationalMod = situational.mod;
            this.rollMode = situational.rollMode;
        }

        if (situational && situational.mod) return { formula: ['@situational'], rollData: { situational: situational.mod } };
        else return situational;
    }

    /**
     * Get Mods for ARSRollAbilityCheck
     *
     * @param {*} forceSkip
     * @returns
     */
    async getMods(forceSkip = false) {
        const situational = this.options.skipSitiational || forceSkip ? undefined : await this._getSitational();
        if (!this.options.skipSitiational && !forceSkip && situational && situational.cancel) return undefined;

        const checkEffectFormula = this._getCheckEffectFormula(this.target ? this.target.actor : this.actor);

        // console.log('rolls.js ARSRollAbilityCheck getMods()', { situational, checkEffectFormula });
        let formula = '';
        const rollFormula = this.formula;
        const formulaTypes = [situational, checkEffectFormula];
        let rollData = foundry.utils.deepClone(this.rollData);
        formulaTypes.forEach((ft) => {
            if (ft && ft?.formula?.length) {
                formula += ' + ' + ft.formula.join(' + ');
                rollData = foundry.utils.mergeObject(rollData, ft.rollData);
            }
        });
        if (formula) {
            formula = `${rollFormula} -(${formula})`;
        }

        // rework the formula/terms for the modifiers.
        if (formula) {
            this.rawformula = formula;
            this.terms = this.constructor.parse(this.rawformula, rollData);
            this._formula = this.resetFormula();
        }

        // console.log('rolls.js ARSRollAbilityCheck getMods  this', this, { formula });
        return this;
    }

    async rollAbilityCheck() {
        let rollOptions = { rollMode: this.rollMode };
        const roll = await this.roll(rollOptions);
        // if (game.dice3d) await game.dice3d.showForRoll(roll, game.user, utilitiesManager.diceRollModeVisibility(this.rollMode));

        const naturalRoll = this.dice[0].total;

        this.critical = naturalRoll == 1;
        this.fumble = naturalRoll == 20;
        this.success = roll.total <= this.checkTarget;
        if (this.fumble) this.success = false;
        if (this.critical) this.success = true;
        this.checkDiff = Math.abs(roll.total - this.checkTarget);
        return roll;
    }
}

export class ARSRollInitiative extends ARSRollBase {}
