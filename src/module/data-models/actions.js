// import { currencyDataSchema } from './schema.js';
const fields = foundry.data.fields;
export class actionDataSchema {
    static defineSchema() {
        
        return {
            //TODO: define action schema
        };
    }
}
export class actionGroupDataSchema {
    static defineSchema() {
        
        return {
            actionGroup: new fields.ArrayField({
                ...actionDataSchema.defineSchema(),
            }),
            //TODO: define actionGroup schema
        };
    }
}

export class ARSActionsSchema {
    static defineSchema() {
        
        return {
            actionGroups: new fields.ArrayField({
                ...actionGroupDataSchema.defineSchema(),
            }),
        };
    }
}
