import { actorDataSchema } from './actor.js';
const fields = foundry.data.fields;

/**
 * Schema for Characters
 */
export default class ARSActorCharacter extends actorDataSchema {
    /** @inheritdoc */
    static migrateData(source) {
        super.migrateData(source);
        // console.log('ARSActorCharacter migrateData', { source });
        if (source.xp) {
            console.log('ARSActorCharacter migrateData xp', { source }, source.xp);
            source.details.xp = Number(source.xp) || 0;
            delete source.xp;
            source.migrate = true;
        }
        if (source.applyxp) {
            console.log('ARSActorCharacter migrateData applyxp', { source }, source.applyxp);
            source.details.applyxp = Number(source.applyxp) || 0;
            delete source.applyxp;
            source.migrate = true;
        }
    }
    /** @inheritDoc */
    static defineSchema() {
        const parentSchema = super.defineSchema();

        // character specific details data
        const characterDetails = {
            age: new fields.StringField({ default: '' }),
            sex: new fields.StringField({ default: '' }),
            height: new fields.StringField({ default: '' }),
            weight: new fields.StringField({ default: '' }),
            deity: new fields.StringField({ default: '' }),
            patron: new fields.StringField({ default: '' }),
            background: new fields.StringField({ default: '' }),
            notes: new fields.StringField({ default: '' }),
            xp: new fields.NumberField({ default: 0 }),
            applyxp: new fields.NumberField({ default: 0 }),
        };

        //append in character details
        parentSchema.details.fields = {
            ...parentSchema.details.fields,
            ...characterDetails,
        };

        return parentSchema;
    }
}
