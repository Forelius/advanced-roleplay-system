import { itemDataSchema } from './item.js';
export default class ARSItemProficiency extends itemDataSchema {
    /** @inheritdoc */
    static migrateData(source) {
        // console.log('ARSItemProficiency migrateData', { source }, source?.proficiencies?.cost, source?.cost);
        // if ('cost' in source && (source.proficiencies?.cost == undefined || source.proficiencies?.cost == null)) {
        if (source?.cost && source.proficiencies) {
            console.log(
                `ARSItemProficiency migrateData source.cost=${source.cost} source.proficiencies=${source.proficiencies?.cost}`,
                {
                    source,
                }
            );
            source.proficiencies.cost = Number(source.cost) || 0;
            // source.cost = undefined;
            source.cost = undefined;
            // delete source.cost;
            source.migrate = true;
        }
    }

    /** @inheritDoc */
    static defineSchema() {
        const fields = foundry.data.fields;
        return foundry.utils.mergeObject(super.defineSchema(), {
            proficiencies: new fields.SchemaField({
                cost: new fields.NumberField({ required: true, nullable: false, initial: 1, min: 0, default: 1 }),
            }),
            // name: new fields.StringField(),
            // type: new fields.StringField(),

            appliedto: new fields.ArrayField(new fields.SchemaField({ id: new fields.StringField({ required: false }) })),
            hit: new fields.StringField({ default: '' }),
            damage: new fields.StringField({ default: '' }),
            speed: new fields.NumberField({ default: 0 }),
            attacks: new fields.StringField({ default: '' }),
            // TODO: this can be removed someday (v13?), moved to proficiencies.cost to not collide with item.cost
            cost: new fields.StringField({ nullable: true, initial: null, required: false }),
        });
    }
}
