import { itemDataSchema, itemCostSchema } from './item.js';
export default class ARSItemPotion extends itemDataSchema {
    /** @inheritDoc */
    static defineSchema() {
        const fields = foundry.data.fields;
        return foundry.utils.mergeObject(super.defineSchema(), {
            ...itemCostSchema.defineSchema(),
        });
    }
}
