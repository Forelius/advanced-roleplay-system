import { itemDataSchema, itemSoundSchema, itemCostSchema } from './item.js';
export default class ARSItemSpell extends itemDataSchema {
    /** @inheritDoc */
    static defineSchema() {
        const fields = foundry.data.fields;
        return foundry.utils.mergeObject(super.defineSchema(), {
            ...itemSoundSchema.defineSchema(),
            ...itemCostSchema.defineSchema(),

            type: new fields.StringField({ default: 'Arcane' }),
            level: new fields.NumberField({ required: true, default: 1 }),
            learned: new fields.BooleanField({ default: false }),
            school: new fields.StringField({ default: '' }),
            sphere: new fields.StringField({ default: '' }),
            range: new fields.StringField({ default: '' }),
            components: new fields.SchemaField({
                verbal: new fields.BooleanField({ default: true }),
                somatic: new fields.BooleanField({ default: true }),
                material: new fields.BooleanField({ default: true }),
            }),
            durationText: new fields.StringField({ default: '' }),
            castingTime: new fields.StringField({ default: '' }),
            areaOfEffect: new fields.StringField({ default: '' }),
            save: new fields.StringField({ default: '' }),
        });
    }
}
