import { currencyDataSchema } from '../schema.js';
import { itemDataSchema } from './item.js';
export default class ARSItemBundle extends itemDataSchema {
    /** @inheritDoc */
    static defineSchema() {
        const fields = foundry.data.fields;
        return foundry.utils.mergeObject(super.defineSchema(), {
            ...currencyDataSchema.defineSchema(),
        });
    }
}
