import { itemDataSchema, itemSoundSchema } from './item.js';
export default class ARSItemSkill extends itemDataSchema {
    /** @inheritDoc */
    static defineSchema() {
        const fields = foundry.data.fields;
        return foundry.utils.mergeObject(super.defineSchema(), {
            ...itemSoundSchema.defineSchema(),

            groups: new fields.StringField({ default: '' }),
            features: new fields.SchemaField({
                type: new fields.StringField({ required: true, default: 'descending' }),
                ability: new fields.StringField({ required: true, default: 'none' }),
                target: new fields.StringField({ required: true, default: 20 }),
                formula: new fields.StringField({ required: true, default: '1d20' }),
                cost: new fields.NumberField({ required: true, default: 1 }),
                modifiers: new fields.SchemaField({
                    formula: new fields.StringField({ default: '' }),
                    class: new fields.NumberField({ default: 0 }),
                    background: new fields.NumberField({ default: 0 }),
                    ability: new fields.NumberField({ default: 0 }),
                    armor: new fields.NumberField({ default: 0 }),
                    item: new fields.NumberField({ default: 0 }),
                    race: new fields.NumberField({ default: 0 }),
                    other: new fields.NumberField({ default: 0 }),
                }),
            }),
        });
    }
}
