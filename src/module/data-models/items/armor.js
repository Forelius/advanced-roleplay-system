import { itemDataSchema, itemCostSchema } from './item.js';
export default class ARSItemArmor extends itemDataSchema {
    static migrateData(source) {
        // console.log('ARSItemArmor migrateData', { source });
        //TODO: move armorstyle to attack.armorstyle
        // if (source.armorstyle) {
        //     console.log('ARSItemArmor migrateData source.attack.armorstyle', { source }, source.armorstyle);
        //     source.attack.armorstyle = source.armorstyle;
        //     delete source.armorstyle;
        //     source.migrate = true;
        // }
    }
    /** @inheritDoc */
    static defineSchema() {
        const fields = foundry.data.fields;
        return foundry.utils.mergeObject(super.defineSchema(), {
            ...itemCostSchema.defineSchema(),
            protection: new fields.SchemaField({
                type: new fields.StringField({ default: '' }),
                ac: new fields.NumberField({ required: true, default: 10 }),
                modifier: new fields.NumberField({ default: 0 }),
                bulk: new fields.StringField({ default: '' }),
                points: new fields.SchemaField({
                    min: new fields.NumberField({ default: 0 }),
                    max: new fields.NumberField({ default: 0 }),
                    value: new fields.NumberField({ default: 0 }),
                }),
            }),

            armorstyle: new fields.StringField({ default: '' }),
        });
    }
}
