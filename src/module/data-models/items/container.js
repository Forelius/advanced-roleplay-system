import { itemDataSchema, itemCostSchema } from './item.js';
export default class ARSItemContainer extends itemDataSchema {
    /** @inheritDoc */
    static defineSchema() {
        const fields = foundry.data.fields;
        return foundry.utils.mergeObject(super.defineSchema(), {
            ...itemCostSchema.defineSchema(),
            capacity: new fields.SchemaField({
                weightreduction: new fields.NumberField({ initial: 0, integer: true, default: 0 }),
                value: new fields.NumberField({ initial: 0, integer: true, default: 0 }),
            }),
        });
    }
}
