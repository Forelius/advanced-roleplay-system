import { arsBaseDataModel } from '../schema.js';

const fields = foundry.data.fields;

/**
 * Base item Schema
 */
export class itemDataSchema extends arsBaseDataModel {
    static defineSchema() {
        return {
            ...super.defineSchema(),
            ...itemDescriptionSchema.defineSchema(),
            ...itemDetailsSchema.defineSchema(),
            sourceUuid: new fields.StringField({ default: '' }),
            alias: new fields.StringField({ default: '' }),
            actions: new fields.ArrayField(new fields.ObjectField()),
            //TODO: serialized action group data
            actionGroups: new fields.ArrayField(new fields.ObjectField()),
        };
    }
}
/**
 * Schema for a item with a itemList (contained) items
 */
export class itemListSchema {
    static defineSchema() {
        return {
            uuid: new fields.StringField({ required: true }),
            sourceuuid: new fields.StringField({ required: true }),
            type: new fields.StringField({ required: true }),
            name: new fields.StringField({ required: true }),
            img: new fields.StringField({ required: true }),
            id: new fields.StringField({ required: true }),
            quantity: new fields.NumberField({ initial: 1, integer: true, default: 1 }),
            count: new fields.NumberField({ initial: 0, integer: true, default: 0 }),
            cost: new fields.NumberField({ nullable: true, initial: 0, integer: true, default: 0 }),
            level: new fields.NumberField({ initial: 0, integer: true, default: 0 }),
            skilltarget: new fields.NumberField({ initial: 0, integer: true, default: 0 }),
        };
    }
}
/**
 * Schema for item descriptions
 */
export class itemDescriptionSchema {
    static defineSchema() {
        return {
            description: new fields.StringField({ required: true, default: '' }),
            dmonlytext: new fields.StringField({ required: true, default: '' }),
        };
    }
}

/**
 * Item Details block schema
 */
export class itemDetailsSchema {
    static defineSchema() {
        return {
            // alias: new fields.StringField({ required: true, default: '' }),
            attributes: new fields.SchemaField({
                rarity: new fields.StringField({ required: true, default: '' }),
                type: new fields.StringField({ required: true, default: '' }),
                subtype: new fields.StringField({ required: true, default: '' }),
                magic: new fields.BooleanField({ required: true, default: false }),
                properties: new fields.ArrayField(new fields.StringField()),
                skillmods: new fields.ArrayField(
                    new fields.SchemaField({
                        name: new fields.StringField({ required: true }),
                        value: new fields.NumberField({ required: true, integer: true }),
                    })
                ),
                conditionals: new fields.ArrayField(
                    new fields.SchemaField({
                        key: new fields.StringField({ required: true }),
                        value: new fields.StringField({ required: true }),
                    })
                ),
                identified: new fields.BooleanField({ default: true }),
                infiniteammo: new fields.BooleanField({ required: true, default: false }),
                size: new fields.StringField({ initial: 'medium', required: true, default: 'medium' }),
            }),
            charges: new fields.SchemaField({
                value: new fields.NumberField({ required: true, initial: 0, default: 0 }),
                min: new fields.NumberField({ required: true, initial: 0, default: 0 }),
                max: new fields.NumberField({ required: true, initial: 0, default: 0 }),
                reuse: new fields.StringField({ required: true, initial: 'none', default: 'none' }),
            }),
            location: new fields.SchemaField({
                state: new fields.StringField({ required: true, default: 'carried' }),
                parent: new fields.StringField({ required: true, default: '' }),
            }),
            resource: new fields.SchemaField({
                itemId: new fields.StringField({ required: true, default: '' }),
            }),
            quantity: new fields.NumberField({ required: true, initial: 0, default: 0 }),
            weight: new fields.NumberField({ required: true, initial: 0, default: 0 }),
            itemList: new fields.ArrayField(new fields.SchemaField({ ...itemListSchema.defineSchema() })),
            source: new fields.StringField({ required: true, default: '' }),
            xp: new fields.NumberField({ required: true, initial: 0, default: 0 }),
        };
    }
}

/**
 * Item sound schema for sound properties
 */
export class itemSoundSchema {
    static defineSchema() {
        return {
            audio: new fields.SchemaField({
                file: new fields.StringField({ required: true, default: '' }),
                volume: new fields.NumberField({ required: true, default: 0.5 }),
                effect: new fields.StringField({ required: true, default: '' }),
                success: new fields.StringField({ required: true, default: '' }),
                failure: new fields.StringField({ required: true, default: '' }),
            }),
        };
    }
}

/**
 * Item cost schama
 */
export class itemCostSchema {
    static defineSchema() {
        return {
            cost: new fields.SchemaField({
                value: new fields.NumberField({ required: true, initial: 0, default: 0 }),
                currency: new fields.StringField({ required: true, default: 'gp' }),
            }),
        };
    }
}

/**
 * Consolidated base schema and other schames on ALL items
 */
export default class ARSItemData extends itemDataSchema {
    /** @inheritDoc */
    static defineSchema() {
        return foundry.utils.mergeObject(super.defineSchema(), {
            ...itemCostSchema.defineSchema(),
        });
    }
}
