import { itemDataSchema } from './item.js';
export default class ARSItemRace extends itemDataSchema {
    /** @inheritDoc */
    static defineSchema() {
        const fields = foundry.data.fields;
        return foundry.utils.mergeObject(super.defineSchema(), {
            proficiencies: new fields.SchemaField({
                weapon: new fields.StringField({ required: true }),
                skill: new fields.StringField({ required: true }),
            }),
            type: new fields.StringField({ initial: '', required: true, trim: true, default: '' }),
            // name: new fields.StringField(),
            // type: new fields.StringField(),
        });
    }
}
