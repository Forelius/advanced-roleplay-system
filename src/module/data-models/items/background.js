import { itemDataSchema } from './item.js';
export default class ARSItemBackground extends itemDataSchema {
    /** @inheritDoc */
    static defineSchema() {
        const fields = foundry.data.fields;
        return foundry.utils.mergeObject(super.defineSchema(), {
            proficiencies: new fields.SchemaField({
                weapon: new fields.StringField({ required: true }),
                skill: new fields.StringField({ required: true }),
            }),
            // name: new fields.StringField(),
            // type: new fields.StringField(),
        });
    }
}
