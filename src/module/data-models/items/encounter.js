import { itemDataSchema } from './item.js';
import * as utilitiesManager from '../../utilities.js';
export default class ARSItemEncounter extends itemDataSchema {
    /** @inheritDoc */
    static defineSchema() {
        const fields = foundry.data.fields;
        return foundry.utils.mergeObject(super.defineSchema(), {
            proficiencies: new fields.SchemaField({
                weapon: new fields.StringField({ required: true }),
                skill: new fields.StringField({ required: true }),
            }),
            // name: new fields.StringField(),
            // type: new fields.StringField(),

            npcList: new fields.ArrayField(
                new fields.SchemaField({
                    id: new fields.StringField({ required: true }),
                    uuid: new fields.StringField({ required: true }),
                    name: new fields.StringField({ required: true }),
                    img: new fields.StringField({ required: true }),
                    count: new fields.StringField({ initial: '1', default: '1' }),
                    pack: new fields.StringField({ nullable: true }),
                    hp: new fields.NumberField({ integer: true, default: 0 }),
                    xp: new fields.NumberField({ integer: true, default: 0 }),
                })
            ),
        });
    }

    /** @inheritdoc */
    static migrateData(source) {
        // console.log('ARSItemEncounter migratePropertiesData', { source });
    }
}

/**
 *
 * When encounter dropped on scene add contents
 *
 * @param {*} item item object (encounter item)
 * @param {*} dropData drop data that includes x/y
 */
export async function encounterDrop(item, dropData) {
    console.log('encounter.js encounterDrop', { item, dropData });

    const usedLocations = [];

    function findUnoccupiedPosition(startX, startY, tokenWidth, tokenHeight) {
        let dx = 0,
            dy = 0;
        let direction = 0; // 0: right, 1: down, 2: left, 3: up
        let steps = 1,
            stepCount = 0;

        const gridWidth = tokenWidth * canvas.grid.w;
        const gridHeight = tokenHeight * canvas.grid.h;
        while (true) {
            // Infinite loop, will break when an unoccupied position is found
            let checkX = startX + dx * canvas.grid.size;
            let checkY = startY + dy * canvas.grid.size;

            const snappedPosition = canvas.grid.getSnappedPosition(checkX, checkY);
            const newLocation = {
                x: snappedPosition.x + canvas.grid.w / 2 - gridWidth / 2,
                y: snappedPosition.y + canvas.grid.h / 2 - gridHeight / 2,
            };

            if (isPositionUnoccupied(newLocation.x, newLocation.y, tokenWidth, tokenHeight)) {
                // console.log(`encounter.js isPositionUnoccupied() SET:`, { newLocation });
                usedLocations.push(`${newLocation.x}:${newLocation.y}`);
                return { x: newLocation.x, y: newLocation.y };
            }

            switch (direction) {
                case 0: // Move right
                    dx++;
                    break;
                case 1: // Move down
                    dy++;
                    break;
                case 2: // Move left
                    dx--;
                    break;
                case 3: // Move up
                    dy--;
                    break;

                case 4: // Move up right
                    dx++;
                    dy--;
                    break;

                case 5: // Move up left
                    dx--;
                    dy--;
                    break;

                case 6: // Move down left
                    dx--;
                    dy++;
                    break;

                case 7: // Move down right
                    dx++;
                    dy++;
                    break;
            }

            stepCount++;
            if (stepCount === steps) {
                stepCount = 0;
                direction = (direction + 1) % 4; // Change direction
                if (direction === 0 || direction === 2) steps++; // Increase step size every full cycle
                // if (direction === 0) steps++; // Increase step size every full cycle
            }
            // console.log(
            //     `encounter.js isPositionUnoccupied() x${checkX} y${checkY} w${canvas.grid.width} h${canvas.grid.height}`
            // );
            if (checkX > canvas.grid.width || checkY > canvas.grid.height || checkX < 0 || checkY < 0) {
                ui.notifications.error(`Reached edge of canvas and did not find clear location x${checkX} y${checkY}`);
                return undefined;
            }
        }
    }

    function isPositionUnoccupied(x, y, tokenWidth, tokenHeight) {
        // Check if the position is outside the canvas boundaries
        if (
            x < 0 ||
            y < 0 ||
            x + tokenWidth * canvas.grid.w > canvas.dimensions.width ||
            y + tokenHeight * canvas.grid.h > canvas.dimensions.height ||
            usedLocations.includes(`${x}:${y}`)
        ) {
            // console.log(`encounter.js isPositionUnoccupied DUMP OUT`, { x, y, tokenWidth, tokenHeight });
            return false;
        }

        // Check for overlap with existing tokens
        return !canvas.tokens.placeables.some((token) => {
            const overlapsX = x < token.x + token.width * canvas.grid.w && x + tokenWidth * canvas.grid.w > token.x;
            const overlapsY = y < token.y + token.height * canvas.grid.h && y + tokenHeight * canvas.grid.h > token.y;
            return overlapsX && overlapsY;
        });
    }

    if (item) {
        for await (const npcr of item.system.npcList) {
            let actor = npcr.uuid ? await fromUuid(npcr.uuid) : await game.actors.get(npcr.id);
            if (!actor && npcr.pack) {
                actor = await game.packs.get(npcr.pack).getDocument(npcr.id);
            }
            if (actor) {
                if (actor.compendium) {
                    const actorData = game.actors.fromCompendium(actor);
                    // create local actor, use same id so we can use it instead if we drop this again
                    actor = await Actor.implementation.create({ ...actorData, id: npcr.id, _id: npcr.id }, { keepId: true });

                    // place this actor into a dump folder
                    const dumpfolder = await utilitiesManager.getFolder('Encounter Drops', 'Actor');
                    actor.update({ folder: dumpfolder.id });
                }

                const countFor = (await utilitiesManager.evaluateFormulaValue(npcr.count)) || 1;
                console.log(`encounter.js EncounterDrop() spawning ${countFor} ${actor.name}`);
                for (let i = 0; i < countFor; i++) {
                    // Prepare the Token data
                    const td = await actor.getTokenDocument({ x: dropData.x, y: dropData.y, hidden: event.altKey });

                    // Find an unoccupied position
                    const unoccupiedPosition = findUnoccupiedPosition(td.x, td.y, td.width, td.height);
                    if (!unoccupiedPosition) {
                        // ui.notifications.warn(`No unoccupied space found for the ${actor.name} Token.`);
                        console.warn(`No unoccupied space found for the ${actor.name} Token.`);
                        continue;
                    }
                    await td.updateSource({
                        x: unoccupiedPosition.x,
                        y: unoccupiedPosition.y,
                    });

                    console.log(`encounter.js EncounterDrop() updateSource`, { td, unoccupiedPosition });

                    // Validate the final position
                    if (!canvas.dimensions.rect.contains(td.x, td.y)) continue;
                    // console.log(`encounter.js EncounterDrop() canvas.dimensions.rect`, { td });

                    await td.constructor.create(td, { parent: canvas.scene });
                    // console.log(`encounter.js ...END EncounterDrop() td.constructor`, { td });
                }
            }
        }
    }
}
