import { itemDataSchema, itemSoundSchema, itemCostSchema } from './item.js';
export default class ARSItemWeapon extends itemDataSchema {
    static migrateData(source) {
        // console.log('ARSItemWeapon migrateData', { source });
        //TODO: move weaponstyle to attack.weaponstyle
        // if (source.weaponstyle) {
        //     console.log('ARSItemWeapon migrateData source.attack.weaponstyle', { source }, source.weaponstyle);
        //     source.attack.weaponstyle = source.weaponstyle;
        //     delete source.weaponstyle;
        //     source.migrate = true;
        // }
    }

    /** @inheritDoc */
    static defineSchema() {
        const fields = foundry.data.fields;
        return foundry.utils.mergeObject(super.defineSchema(), {
            ...itemSoundSchema.defineSchema(),
            ...itemCostSchema.defineSchema(),
            attack: new fields.SchemaField({
                speed: new fields.NumberField({ default: 1 }),
                type: new fields.StringField({ default: 'melee' }),
                perRound: new fields.StringField({ default: '1/1' }),
                modifier: new fields.NumberField({ default: 0 }),
                magicBonus: new fields.NumberField({ default: 0 }),
                magicPotency: new fields.NumberField({ default: 0 }),
                range: new fields.SchemaField({
                    short: new fields.StringField({ default: '' }),
                    medium: new fields.StringField({ default: '' }),
                    long: new fields.StringField({ default: '' }),
                }),
                primary: new fields.BooleanField({ default: false }),
            }),
            damage: new fields.SchemaField({
                type: new fields.StringField({ default: 'slashing' }),
                normal: new fields.StringField({ default: '1d6' }),
                large: new fields.StringField({ default: '1d8' }),
                otherdmg: new fields.ArrayField(new fields.ObjectField({ default: '' })),
                modifier: new fields.NumberField({ default: 0 }),
                magicBonus: new fields.NumberField({ default: 0 }),
            }),
            weaponstyle: new fields.StringField({ required: true, default: '' }),
        });
    }
}
