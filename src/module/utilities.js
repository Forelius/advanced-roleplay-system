import { ARS } from './config.js';
import * as effectManager from './effect/effects.js';
import * as actionManager from './apps/action.js';
import * as initLibrary from './library.js';
import * as dialogManager from './dialog.js';
import { ARSCharacterSheet } from './actor/actor-sheet-character.js';
import { ARSNPCSheet } from './actor/actor-sheet-npc.js';

import * as debug from './debug.js';

/**
 *
 * Capitalize string
 *
 * @param {*} s
 * @returns
 */
export function capitalize(s) {
    if (typeof s !== 'string') return '';
    return s.charAt(0).toUpperCase() + s.slice(1);
}

/**
 *
 * Request a action to be performed by the connected GM
 *
 *           utilitiesManager.runAsGM({
 *               operation: 'applyActionEffect',
 *               user: game.user.id,
 *               sourceActorId: source.id,
 *               sourceTokenId: sourceToken.id
 *               targetActorId: target.id,
 *               targetTokenId: token.id,
 *               targetItemId: item.id,
 *               itemUpdate: {'some.path': value}
 *               sourceAction: sourceAction
 *           });
 *
 * @param {Object} data Requested command data
 */
export async function runAsGM(data = {}) {
    // if GM we skip to the command
    if (game.user.isGM) {
        await runGMCommand(data);
    } else {
        // send data to socket and look for GM to run the command for user
        const dataPacket = {
            requestId: foundry.utils.randomID(16),
            type: 'runAsGM',
            ...data,
        };
        // Emit a socket event
        console.trace('runAsGM', { data, dataPacket });
        await game.socket.emit('system.ars', dataPacket);
    }
}

/**
 *
 * Process the requested GM Command
 *
 * @param {*} data Requested command data
 */
export async function processGMCommand(data = {}) {
    // const findGM = game.user === game.users.filter((user) => user.isGM && user.active).sort((a, b) => a.id - b.id)[0];
    const activeGMs = game.users.filter((user) => user.isGM && user.active);
    const findGM = activeGMs.length ? activeGMs[0] : null;
    console.trace('processGMCommand', { data, activeGMs, findGM });
    if (!findGM) {
        ui.notifications.error(`No GM connected to process requested command.`);
        console.trace('processGMCommand No GM connected to process requested command.');
    }
    // check to see if the GM we found is the person we've emit'd to and if so run command.
    else if (findGM.id === game.user.id) {
        if (!game.ars.runAsGMRequestIds[data.requestId]) {
            // console.log("utilities.js processGMCommand", { data });

            // We do this to make sure the command is only run once if more than
            // one GM is on the server
            game.ars.runAsGMRequestIds[data.requestId] = data.requestId;

            /**
             * "data" is serialized and deserialized so the type is lost.
             * Because of that we just exchange IDs when we need protos/etc
             * and load them where needed
             *
             */
            // let source = data.sourceId ? canvas.tokens.get(data.sourceId) : undefined;
            // let target = data.targetId ? canvas.tokens.get(data.targetId) : undefined;

            // const sourceActor = data.sourceActorId ? game.actors.get(data.sourceActorId) : undefined;
            // const targetActor = data.targetActorId ? game.actors.get(data.targetActorId) : undefined;
            // const targetToken = data.targetTokenId ? canvas.tokens.get(data.targetTokenId) : undefined;

            // console.log("utilities.js processGMCommand", { data });

            await runGMCommand(data);
        } else {
            // requestId already processed
            console.log('utilities.js', 'processGMCommand', 'Unknown asGM request DUPLICATE ', { data });
            ui.notifications.error(`Duplicate asGM request command.`, {
                permanent: true,
            });
        }
    }
}

/**
 *
 * Run command as GM, final leg (after sending it to socket or directly executed by GM)
 *
 * @param {Object} data
 */
async function runGMCommand(data) {
    console.log('utilitis.js runGMCommand', { data });

    const sourceActor = data.sourceActorId ? game.actors.get(data.sourceActorId) : undefined;
    const sourceToken = data.sourceTokenId ? canvas.tokens.get(data.sourceTokenId) : undefined;
    const targetActor = data.targetActorId ? game.actors.get(data.targetActorId) : undefined;
    const targetToken = data.targetTokenId ? canvas.tokens.get(data.targetTokenId) : undefined;
    const targetItemId = data.targetItemId ? data.targetItemId : undefined;
    const targetItemUuid = data.sourceItem ? data.sourceItem : undefined;
    const combatTracker = data.combatTrackerId ? game.combats.get(data.combatTrackerId) : undefined;

    // console.log("utilitis.js runGMCommand", { sourceActor, sourceToken, targetActor, targetToken, targetItemId })
    switch (data.operation) {
        // case 'game.party.updateMember':
        //     if (data.sourceActorId) {
        //         game.party.updateMember(data.sourceActorId);
        //     }
        //     break;

        case 'deleteEmbeddedDocuments':
            if (targetToken && targetItemId) {
                targetToken.actor.deleteEmbeddedDocuments('Item', [targetItemId], { hideChanges: true });
            } else if (targetActor && targetItemId) {
                targetActor.deleteEmbeddedDocuments('Item', [targetItemId], {
                    hideChanges: true,
                });
            }
            break;

        case 'createEmbeddedDocuments':
            if (targetActor && data.itemData) {
                await targetActor.createEmbeddedDocuments('Item', [data.itemData], { hideChanges: true });
            }
            break;

        case 'deleteActiveEffect':
            await targetToken.actor.deleteEmbeddedDocuments('ActiveEffect', data.effectIds);
            break;

        case 'applyActionEffect':
            await effectManager.applyActionEffect(
                sourceToken ? sourceToken.actor : sourceActor,
                targetToken,
                data.sourceAction,
                data.user
            );
            break;

        case 'applyActionEffect-Item':
            if (targetItemUuid) {
                const item = fromUuidSync(targetItemUuid);
                if (item) {
                    await effectManager.applyActionEffect(item, undefined, data.sourceAction, data.user);
                }
            }
            break;

        case 'adjustTargetHealth':
            await setActorHealth(targetActor ? targetActor : targetToken.actor, data.targetHPresult);
            if (targetToken?.hasActiveHUD) canvas.tokens.hud.render();
            break;

        case 'unsetFlag':
            await targetToken.document.unsetFlag('world', data.flag.tag);
            break;

        case 'setFlag':
            await targetToken.document.setFlag('world', data.flag.tag, data.flag.data);
            break;

        case 'setCombatTrackerFlag':
            if (combatTracker) await combatTracker.setFlag('ars', data.flag.tag, data.flag.data);
            break;

        case 'itemUpdate':
            let itemToUpdate;
            if (targetToken && targetItemId && data.update) {
                itemToUpdate = await targetToken.actor.getEmbeddedDocument('Item', targetItemId);
                if (itemToUpdate) itemToUpdate.update(data.update);
            } else if (targetActor && targetItemId && data.update) {
                itemToUpdate = await targetActor.getEmbeddedDocument('Item', targetItemId);
                if (itemToUpdate) itemToUpdate.update(data.update);
            }
            break;

        case 'actorUpdate':
            if (targetActor && data.update) {
                await targetActor.update(data.update);
            } else if (targetToken && data.update) {
                await targetToken.actor.update(data.update);
            }
            break;

        case 'partyAddLogEntry':
            if (data.text && game.party?.addLogEntry) {
                await game.party.addLogEntry(data.text);
            }
            break;

        // case 'partyShareLootedCoins':
        //     if (sourceToken && game.party?.shareLootedCoins) {
        //         game.party.shareLootedCoins(sourceToken);
        //     }
        //     break;

        default:
            console.log('utilities.js processGMCommand Unknown asGM/runGMCommand request ', data.operation, { data });
            break;
    }
}

/**
 *
 * Adjust actor heath accounting for min/max
 *
 * @param {*} actor
 * @param {*} adjustment  12 or -12
 */
export function adjustActorHealth(actor, adjustment) {
    console.log('utilitis.js adjustActorHealth ', { actor, adjustment });

    const nCurrent = parseInt(actor.system.attributes.hp.value);
    const nMax = parseInt(actor.system.attributes.hp.max);
    const nMin = parseInt(actor.system.attributes.hp.min);

    // console.log("utilitis.js adjustActorHealth ", { actor, adjustment, nCurrent, nMax, nMin });

    const nNew = Math.clamp(nCurrent + parseInt(adjustment), nMin, nMax);
    setActorHealth(actor, nNew);
}
/**
 *
 * Set new health value on token.
 *
 * @param {*} targetActor  targetActor's health to adjust
 * @param {*} value  The new value of target tokens health
 */
export async function setActorHealth(targetActor, value) {
    console.log('utilities.js setActorHealth', { targetActor, value });
    await targetActor.update({ 'system.attributes.hp.value': value });
    // set status markers for health values
    // setHealthStatusMarkers(targetActor);
    // moved to _update() in actor.js
}

/**
 *
 * If the actor hp is at min or lower, mark them defeated
 *
 * @param {*} actorData
 */
export async function setHealthStatusMarkers(targetActor) {
    console.log('utilities.js setHealthStatusMarkers', { targetActor });
    // need token object, not token document
    const token = targetActor.getToken()?.object;
    if (token) {
        // console.log("utilities.js setHealthStatusMarkers", { token });
        const optionNegativeHP = game.settings.get('ars', 'optionNegativeHP');
        const isDead =
            optionNegativeHP && targetActor.type == 'character'
                ? targetActor.system.attributes.hp.value <= targetActor.system.attributes.hp.min
                : targetActor.system.attributes.hp.value <= 0;
        const isUnconscious = !isDead && optionNegativeHP ? targetActor.system.attributes.hp.value <= 0 : false;
        const hasDeathStatus = token.actor.hasStatusEffect('dead');
        const hasUnconsciousStatus = token.actor.hasStatusEffect('unconscious');
        // find dead status
        const statusDead = CONFIG.statusEffects.find((e) => e.id === 'dead');
        const effectDead = token.actor && statusDead ? statusDead : CONFIG.controlIcons.defeated;
        const statusUnconscious = CONFIG.statusEffects.find((e) => e.id === 'unconscious');
        const effectUnconscious = token.actor && statusUnconscious ? statusUnconscious : undefined;

        // check to see if they need dead mark or remove dead mark
        // if ((!dead && alreadyDown) || (dead && !alreadyDown)) {
        if ((!hasUnconsciousStatus && isUnconscious) || (hasUnconsciousStatus && !isUnconscious)) {
            await token.toggleEffect(effectUnconscious, {
                overlay: isUnconscious,
                active: isUnconscious,
            });
        }

        if ((!hasDeathStatus && isDead) || (hasDeathStatus && !isDead)) {
            await token.toggleEffect(effectDead, {
                overlay: isDead,
                active: isDead,
            });
        }
        // if (hasDeathStatus && isDead) {
        //     // dont need to add it, we have it already
        // } else {
        //     await token.toggleEffect(effect, {
        //         overlay: defeated,
        //         active: defeated,
        //     });
        // }
        // }
    }
}

/**
 *
 * @param {Number} slotIndex
 * @param {String} slotType arcaneSlots or divineSlots
 * @param {Boolean} bValue  Set true or false
 */
export async function memslotSetUse(actor, slotType, slotLevel, slotIndex, bValue) {
    let memSlots = foundry.utils.deepClone(actor.system.spellInfo.memorization);
    memSlots[slotType][slotLevel][slotIndex].cast = bValue;
    await actor.update({ 'system.spellInfo.memorization': memSlots });
}

/**
 *
 * @param {String} slotType  arcaneSlots or divineSlots
 * @param {Number} slotIndex
 *
 */
export function isMemslotUsed(actor, slotType, slotLevel, slotIndex) {
    // console.log("utilitis.js  isMemslotUsed", { actor, slotType, slotLevel, slotIndex });
    const spellUsed = actor.system.spellInfo.memorization[slotType][slotLevel][slotIndex]?.cast || false;
    return spellUsed;
}

// not sure we need this
// /**
//  * Return ammo remaining for weapon
//  *
//  * @param {*} actor
//  * @param {*} weapon
//  * @returns
//  */
// export async function getAmmoRemaining(actor, weapon) {
//     let itemId = weapon.system.resource.itemId;
//     if (!itemId) {
//         // itemId = await dialogManager.getInventoryItem(
//         //     actor,
//         //     `Ammo for ${weapon.name}`,
//         //     `Select Ammo`,
//         //     { inventory: actor.weapons }
//         // );
//         if (!itemId) return 0; // If no item id found, assume no ammo left
//     }
//     if (itemId) {
//         // let item = actor.items.get(itemId);
//         let item = actor.getEmbeddedDocument("Item", itemId);
//         if (item) {
//             return item.system.quantity;
//         }
//     }
//     return 0; // Default return value indicating no ammo left
// }

/**
 * Use ammo from a weapon. If ammo runs out, prompts user to select a new ammo item.
 *
 * @param {Object} actor - The actor entity using the weapon.
 * @param {Object} weapon - The weapon entity from which ammo is being used.
 * @returns {Promise<boolean>} - Returns true if ammo was successfully used, false otherwise.
 */
export async function useWeaponAmmo(actor, weapon) {
    if (!game.ars.config.ammoAttacks.includes(weapon.system.attack.type)) return true;
    try {
        // if weapon has ammo set to infinite yeap out
        if (weapon.system?.attributes?.infiniteammo) {
            return true;
        }

        // Use optional chaining to ensure there's no error if weapon is undefined.
        const ammoItem = weapon?.ammo;

        // Determine the quantity of the current ammo item.
        const ammoQuantity = ammoItem ? ammoItem.system.quantity : 0;

        // Check if there's a valid ammo item with non-zero quantity.
        if (ammoItem && ammoQuantity > 0) {
            await adjustAmmoQuantity(ammoItem, ammoQuantity);
            return true;
        }

        // If no ammo or quantity is zero, prompt user to select a new ammo item.
        const newAmmoItemId = await promptForAmmoSelection(actor, weapon);
        if (!newAmmoItemId) return false;

        // Update weapon with new ammo item's ID.
        await weapon.update({ 'system.resource.itemId': newAmmoItemId });

        // Recursively call this function to handle the newly set ammo.
        return await useWeaponAmmo(actor, weapon);
    } catch (error) {
        console.error('Error in useWeaponAmmo function:', error);
        return false;
    }
}

/**
 * Prompt the user to select a new ammo item from their inventory.
 *
 * @param {Object} actor - The actor entity in need of new ammo.
 * @param {Object} weapon - The weapon entity requiring ammo.
 * @returns {Promise<string|null>} - Returns the ID of the selected ammo item or null if none was chosen.
 */
async function promptForAmmoSelection(actor, weapon) {
    return dialogManager.getInventoryItem(actor, `Ammo for ${weapon.name}`, `Select Ammo`, { inventory: actor.weapons });
}

/**
 * Adjust the quantity of a given ammo item.
 *
 * @param {Object} ammoItem - The ammo item whose quantity needs to be adjusted.
 * @param {number} currentQuantity - The current quantity of the ammo item.
 */
async function adjustAmmoQuantity(ammoItem, currentQuantity) {
    // If there's more than one ammo item left, decrease its quantity.
    if (currentQuantity - 1 >= 0) {
        await ammoItem.update({ 'system.quantity': currentQuantity - 1 });
    } else {
        // If no ammo is left, remove the ammo item from the weapon.
        // await ammoItem.update({ 'system.resource.-=itemId': null });
        await ammoItem.update({ 'system.resource.itemId': '' });
    }
}

/**
 * Return charges remaining for action
 *
 * @param {*} actor
 * @param {*} itemSource
 * @param {*} action
 * @returns
 */
export function getActionCharges(actor, itemSource, action) {
    // console.log("utilities.js getActionCharges", actor?.name, action?.name, {
    //     actor,
    //     itemSource,
    //     action,
    // });
    if (!actor || !action) return 0;
    const type = action.resource.type;
    const cost = action.resource.count.cost;
    const max = Number.isInteger(action.resource.count.max)
        ? action.resource.count.max
        : evaluateFormulaValueSync(action.resource.count.max, actor.getRollData());
    let used = parseInt(action.resource.count.value);
    const remaining = max - used;
    const remainingUses = Math.floor(remaining / cost);
    let itemId = action.resource.itemId;

    switch (type) {
        case 'charged':
            return remainingUses;

        case 'item':
        case 'powered':
            if (!itemId && type !== 'powered') {
                if (!itemId) return 0;
            } else if (!itemId && type === 'powered' && itemSource) {
                itemId = itemSource.id;
            }
            let item = actor.getEmbeddedDocument('Item', itemId);
            if (!item && itemSource && type === 'powered') item = itemSource;

            if (item) {
                if (type === 'item') {
                    const itemCount = item.system.quantity;
                    const itemCountUses = Math.floor(itemCount / cost);
                    return itemCountUses;
                } else if (type === 'powered' && item.system.charges) {
                    const poweredUsed = item.system.charges.value;
                    const maxCount = item.system.charges.max;
                    const poweredRemaining = maxCount - poweredUsed;
                    const powerRemainingUses = Math.floor(poweredRemaining / cost);
                    return powerRemainingUses;
                }
            } else {
                return 0;
            }
            break;

        default:
            return 0;
    }
    return 0;
}

/**
 *
 * Use a charge for an action
 *
 * @param {*} actor
 * @param {*} action
 * @returns
 */
export async function useActionCharge(actor, action, itemSource) {
    // console.trace('utilities.js useActionCharge', { actor, action, itemSource });

    if (!game.ars.config.chargedActions.includes(action.type) || action.resource.type == 'none') return true;

    let actionBundle = itemSource
        ? foundry.utils.deepClone(itemSource.system.actions)
        : foundry.utils.deepClone(actor.system.actions);

    const type = action.resource.type;
    const cost = action.resource.count.cost;
    const max = Number.isInteger(action.resource.count.max)
        ? action.resource.count.max
        : await evaluateFormulaValue(action.resource.count.max, actor.getRollData());
    let used = parseInt(action.resource.count.value);
    let itemId = action.resource.itemId;

    console.log('utilities.js useActionCharge', {
        type,
        cost,
        max,
        used,
        itemId,
    });

    switch (type) {
        case 'charged':
            if (used + cost <= max) {
                actionBundle[action.index].resource.count.value = used + cost;
                if (!actionBundle[action.index].resource.trackTime)
                    actionBundle[action.index].resource.trackTime = game.time.worldTime;
                itemSource
                    ? await itemSource.update({
                          'system.actions': actionBundle,
                      })
                    : await actor.update({ 'system.actions': actionBundle });
                // console.log("utilities.js useActionCharge TRUE")
                return true;
            } else {
                // no charges left
                return false;
            }
            break;

        case 'item':
        case 'powered':
            if (!itemId && type !== 'powered') {
                if ((['potion'].includes(itemSource.type) || itemSource.isScroll) && itemSource.system.quantity) {
                    itemId = itemSource.id;
                }
                // if still no itemId prompt
                if (!itemId) {
                    itemId = await dialogManager.getInventoryItem(actor, `Resource for ${action.name}`, `Select Resource`);
                }
                if (!itemId) return false;
                actionBundle[action.index].resource.itemId = itemId;
                if (itemSource) {
                    await itemSource.update({ 'system.actions': actionBundle });
                } else {
                    await actor.update({ 'system.actions': actionBundle });
                }
            } else if (!itemId && type === 'powered' && itemSource) {
                actionBundle[action.index].resource.itemId = itemSource.id;
                await itemSource.update({ 'system.actions': actionBundle });
            }
            let item = await actor.getEmbeddedDocument('Item', itemId);
            if (!item && itemSource && type === 'powered') item = itemSource;

            if (item) {
                if (type === 'item') {
                    let itemCount = item.system.quantity;
                    // console.log("utilities.js useActionCharge itemCount", itemCount)
                    if (itemCount - cost >= 0) {
                        // console.log("utilities.js useActionCharge itemCount - cost", (itemCount - cost))
                        await item.update({
                            'system.quantity': itemCount - cost,
                        });
                        return true;
                    } else {
                        // no more left, remove this item so next time it prompts to select
                        if (itemCount <= 0) {
                            actionBundle[action.index].resource.itemId = '';
                            itemSource
                                ? await itemSource.update({
                                      'system.actions': actionBundle,
                                  })
                                : await actor.update({
                                      'system.actions': actionBundle,
                                  });
                        }
                        return false;
                    }
                } else if (type === 'powered') {
                    // we use charges from another item to power this action
                    let poweredUsed = item.system.charges.value;
                    let maxCount = item.system.charges.max;
                    if (poweredUsed + cost <= maxCount) {
                        const newUsed = poweredUsed + cost;
                        await item.update({ 'system.charges.value': newUsed });
                        return true;
                    } else {
                        return false;
                    }
                }
            } else {
                ui.notifications.error(
                    `Unable to find the object associated with this power. Check the action->resource section and verify the item is set correctly. `
                );
            }
            break;
    }
    // console.log("utilities.js useActionCharge FALSE")
    return false;
}

/**
 * Evaluate a formula to it's total number value, async
 * @param {String} formula
 * @param {*} data
 * @returns
 */
export async function evaluateFormulaValue(formula, data = {}, options = {}) {
    // console.log('utilities.js evaluateFormulaValueAsync', { formula, data });
    if (formula) {
        let fRoll;
        try {
            fRoll = await new Roll(String(formula), data).evaluate();
            // console.log("utilities.js evaluateFormulaValue 2", { formula, data, fRoll });
        } catch (err) {
            console.warn('utilities.js evaluateFormulaValueAsync formula process error', { err, formula, data, options });
            return 0;
        }

        // console.log("utilities.js evaluateFormulaValue 1", { fRoll });
        if (options && options.showRoll) {
            if (game.dice3d) await game.dice3d.showForRoll(fRoll, game.user, true);
        }
        return fRoll.total; // round it off
    } else {
        // console.log("utilities.js evaluateFormulaValue NOT FORMULA PASSED");
        return 0;
    }
}

/**
 *
 * Eval a formula w/o async
 *
 * @param {*} formula
 * @param {*} RollData
 * @returns
 */
export function evaluateFormulaValueSync(formula, data = {}, options = {}) {
    // console.log("utilities.js evaluateFormulaValue 1", { formula, data });
    if (formula) {
        let fRoll;
        try {
            fRoll = new Roll(String(formula), data).evaluateSync();
        } catch (err) {
            // console.trace();
            console.warn('utilities.js evaluateFormulaValue formula process', { err, data, formula, options });
            return 0;
        }
        if (options && options.showRoll) {
            if (game.dice3d) game.dice3d.showForRoll(fRoll, game.user, false);
        }
        return fRoll.total;
    } else {
        // console.log("utilities.js evaluateFormulaValue NO FORMULA PASSED");
        return 0;
    }
}

/**
 * Do somethings when NPC token first placed.
 *
 * Roll hp using static hp, hp calculation or hitdice
 *
 * @param {Object} token NPC Token instance
 */
export async function postNPCTokenCreate(token) {
    // wont run this if the npc token already has a .max value
    // if (!token.actor.system.attributes.hp.max) {
    const hitdice = String(token.actor.system.hitdice);
    const hpCalc = String(token.actor.system.hpCalculation);
    const hpStatic = parseInt(token.actor.system.attributes.hp.value);
    let formula = '';

    let hp = 0;
    if (hpStatic > 0) {
        hp = hpStatic;
    } else if (hpCalc.length > 0) {
        formula = hpCalc;
    } else if (hitdice.length > 0) {
        try {
            const aHitDice = hitdice.match(/^(\d+)(.*)/);
            const sHitDice = aHitDice[1];
            const sRemaining = aHitDice[2];
            formula = sHitDice + 'd8' + sRemaining;
        } catch (err) {
            ui.notifications.error(`Error in utilities.js postNPCTokenCreate() ${err}`);
        }
    }

    if (!hpStatic) {
        const roll = await new Roll(formula).evaluate();
        hp = roll.total;
    }
    // at least 1 hp
    hp = Math.max(hp, 1);

    console.log('_postNPCTokenCreate:createToken:hp', { hp });

    const npcNumberedNames = game.settings.get('ars', 'npcNumberedNames');
    let newName = '';
    if (npcNumberedNames) {
        // set NPC name # with random value so they can be called out
        const MaxRandom = 25;
        const npcTokens = token.collection
            ? token.collection.filter(function (mapObject) {
                  return mapObject?.actor?.type === 'npc';
              })
            : [];
        let randomRange = MaxRandom + npcTokens.length;
        let loops = 0;

        do {
            loops += 1;
            const nNumber = Math.floor(Math.random() * (randomRange + loops)) + 1;
            // if the name contains #XX remove it, happens when you copy/paste a existing npc token
            let sName = token.name.replace(/#\d+/, '');
            sName = `${sName} #${nNumber}`;
            const matchedNPCs = npcTokens.filter(function (npcToken) {
                return npcToken.name.toLowerCase() === sName.toLowerCase();
            });
            if (matchedNPCs.length < 1) {
                newName = sName;
            }
            if (loops > 100) {
                newName = `${token.name} #xXxXxX`;
            }
        } while (newName == '');
    } else {
        newName = token.name;
    }

    // if we have a list of damage entries and nothing in actions or no weapons, create some
    try {
        const damageList = token.actor.system.damage.match(/(\d+[\-dD]\d+([\-\+]\d+)?)/g);
        // token.actor.weapons
        // if (damageList?.length && token.actor.system?.weapons?.length < 1) {
        if (damageList?.length && token.actor?.weapons?.length < 1) {
            if (!token.actor.system.actions || token.actor.system.actions?.length < 1) {
                actionManager.createActionForNPCToken(token, damageList);
            }
        }
    } catch (err) {
        ui.notifications.error(`Error in utilities.js postNPCTokenCreate() ${err}`);
    }

    // calculate size when dropped.
    const protoSizeWidth = token.actor.prototypeToken.width;
    const protoSizeHeight = token.actor.prototypeToken.height;
    const customSettingSize = protoSizeHeight != 1 || protoSizeWidth != 1;
    console.log('_postNPCTokenCreate:createToken:hp', { token, protoSizeHeight, protoSizeWidth, hp }, protoSizeHeight != 1);
    const sizeSetting = ARS.tokenSizeMapping[token.actor.system.attributes.size] ?? 1;

    /**
     * do this so we can calculate xp at drop time with current hp for older systems
     *
     * 15+(@system.attributes.hp.max*100) would be 15+maxHP*100 and when killed granted to party xp
     *
     */
    const rollData =
        foundry.utils.mergeObject(token.actor.getRollData(), {
            // system: {
            attributes: {
                hp: { max: hp },
            },
            // }
        }) || 0;

    const xpTotal = await evaluateFormulaValue(token.actor.system.xp.value, rollData);

    console.log('utilities.js postNPCTokenCreate', { rollData, xpTotal }, token.actor.system.xp.value);

    // set values for updates
    const actorUpdates = {
        'system.attributes.hp.value': hp,
        'system.attributes.hp.max': hp,
        'system.attributes.hp.min': 0,
        'system.xp.value': xpTotal,
        name: newName,
    };
    const tokenUpdates = {
        name: newName,
        width: customSettingSize ? protoSizeWidth : sizeSetting,
        height: customSettingSize ? protoSizeHeight : sizeSetting,
        'xp.value': xpTotal,
    };

    // generate coin formulas when spawning npc
    for (const coin in token.actor.system.currency) {
        const formula = token.actor.system.currency[coin];
        if (formula) {
            const coinValue = parseInt(await evaluateFormulaValue(formula, null)) || 0;
            // actorUpdates[`system.currency.${coin}`] = coinValue;
            token.actor.giveSpecificCurrency(coinValue, coin);
        }
    }
    // console.log("utilities.js postNPCTokenCreate", { actorUpdates, tokenUpdates })
    // use this for token update instead?
    Hooks.call('arsUpdateToken', this, token, tokenUpdates, 300);
    Hooks.call('arsUpdateActor', token.actor, actorUpdates);
}

/**
 *
 * Sort callback function to sort by record.name
 *
 * @param {*} a
 * @param {*} b
 * @returns
 */
export function sortByRecordName(a, b) {
    if (a?.name && b?.name) {
        const nameA = a.name.toLowerCase();
        const nameB = b.name.toLowerCase();
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }
    }
    return 0;
}

/**
 *
 * Sort by the item.data.sort value
 *
 * @param {*} a
 * @param {*} b
 * @returns
 */
export function sortBySort(a, b) {
    const nameA = a.sort;
    const nameB = b.sort;
    if (nameA < nameB) {
        return -1;
    }
    if (nameA > nameB) {
        return 1;
    }
    return 0;
}

/**
 *
 * Sort callback function to sort by record.level
 *
 * @param {*} a
 * @param {*} b
 * @returns
 */
export function sortByLevel(a, b) {
    if (a.level < b.level) {
        return -1;
    }
    if (a.level > b.level) {
        return 1;
    }
    return 0;
}

/**
 *
 * Wrapper function to get item from world or from packs
 *
 * @param {*} itemId
 * @returns
 */
export async function getItem(itemId) {
    // look for item in world first
    let item = getWorldItem(itemId);
    // if not in world, find in packs
    if (!item) item = await getPackItem(itemId);
    return item;
}

/**
 *
 * Get item by name in world or pack file, first match
 *
 * @param {*} name
 * @returns
 */
export function getItemByName(name) {
    let item = getWorldItemByName(name);
    if (!item) item = getPackItemByName(name);
    return item;
}

/**
 *
 * Get an item from any pack file
 *
 * @param {*} itemId
 * @returns
 */
export async function getPackItem(itemId) {
    let item;
    const allItemPacks = game.packs.filter((i) => i.metadata.type === 'Item');
    // console.log("utilities.js getPackItem", { allItemPacks });
    for (const pack of allItemPacks) {
        const foundItem = await game.packs.get(pack.collection).getDocument(itemId);
        if (foundItem) {
            if (game.user.isGM || pack.visible) {
                item = foundItem;
            }
            break;
        }
    }
    return item;
}

/**
 *
 * Get an item in the Campaign local
 *
 * @param {*} itemId
 * @returns
 */
export function getWorldItem(itemId) {
    const item = game.items.get(itemId);
    // console.log("utilitiesManager.js getWorldItem 1", { itemId, item });
    return item;
}

/**
 *
 * Get a item by name from pack files
 *
 * @param {*} name
 * @returns
 */
export function getPackItemByName(name) {
    return game.ars.library.packs.items.find((entry) => entry.name.toLowerCase() === name.toLowerCase());
}

/**
 *
 * Get a item from world files by name
 *
 * @param {*} name
 * @returns
 */
export function getWorldItemByName(name) {
    return game.items.getName(name);
}

/**
 * Function to allow refresh of library data, used by modules
 */
export async function initializeLibrary() {
    await initLibrary.default();
}

/**
 *
 * Get folder record or create one to match name/type.
 *
 * @param {String} folderName "Dropped NPCs"
 * @param {String} type "Actor" or "Item" .etc
 * @param {String} sort "a" for auto, "m" for manual
 * @param {Number} sortNumber "0"
 * @returns Folder
 */
export async function getFolder(folderName, type, sort = 'a', sortNumber = 0) {
    let folder;
    // const folderList = game.folders.filter(a => { a.name === folderName && a.type == type });
    const foundFolder = game.folders.getName(folderName);
    // console.log("utilitiesManager.js getFolder 1", { foundFolder })
    if (foundFolder) {
        // console.log("utilitiesManager.js getFolder 2", { foundFolder })
        folder = foundFolder;
    } else {
        let folderData = {
            name: folderName,
            type: type,
            sorting: 'a',
            sort: 0,
        };
        const rootFolder = await Folder.implementation.create(folderData);
        // console.log("utilitiesManager.js getFolder 3", { rootFolder })
        folder = rootFolder;
    }

    // console.log("utilitiesManager.js getFolder", { folder })
    return folder;
}
/**
 *
 *  Clear save Cache
 *
 * @param {Actor} sourceActor
 * @param {Token} targetToken
 */
export async function deleteSaveCache(targetToken) {
    await runAsGM({
        operation: 'unsetFlag',
        user: game.user.id,
        targetTokenId: targetToken.id,
        targetActorId: targetToken.actor.id,
        // sourceActorId: sourceActor.id,
        flag: {
            tag: 'saveCache',
        },
    });
}

/**
 *
 * Set the saveCache for follow up spell damage reductions
 *
 * @param {Actor} sourceActor
 * @param {Token} targetToken
 */
export async function setSaveCache(sourceActor, targetToken) {
    await runAsGM({
        operation: 'setFlag',
        user: game.user.id,
        targetTokenId: targetToken.id,
        targetActorId: targetToken.actor.id,
        sourceActorId: sourceActor.id,
        flag: {
            tag: 'saveCache',
            data: { save: 'halve', sourceId: sourceActor.id },
        },
    });
}

/**
 * Send generic chat message with minimal requirements
 *
 * @param {*} speaker ChatMessage.getSpeaker({ actor: this.actor })
 * @param {*} title
 * @param {*} message
 * @param {*} img
 * @param {*} chatCustomData
 */
export function chatMessage(
    speaker = ChatMessage.getSpeaker(),
    title = `Message`,
    message = `Forgot something...`,
    img = '',
    chatCustomData = { rollMode: game.settings.get('core', 'rollMode') }
) {
    let imageHTML = '';
    if (img) {
        imageHTML = `<div class="a25-image"><img src="${img}" height="64"/></div>`;
    }
    let chatData = {
        title: title,
        content: `<div><h2>${title}</h2></div>` + `${imageHTML ? imageHTML : ''}` + `<div>${message}</div>`,
        user: game.user.id,
        speaker: speaker,
        style: game.ars.const.CHAT_MESSAGE_STYLES.OTHER,
    };
    foundry.utils.mergeObject(chatData, { ...chatCustomData });

    console.log('utilities.js chatMessage()', { speaker, title, message, img, chatCustomData, chatData });

    //use user current setting? game.settings.get("core", "rollMode")
    if (chatCustomData.rollMode) ChatMessage.applyRollMode(chatData, chatCustomData.rollMode);
    return ChatMessage.create(chatData);
}

/**
 *
 * Send a private message to list of "targetUsers"
 *
 * @param {Object} targetUsers
 * @param {*} speaker
 * @param {String} title
 * @param {String} message
 * @param {String} img
 * @param {Object} chatCustomData
 */
export function chatMessagePrivate(
    targetUsers = undefined,
    speaker = ChatMessage.getSpeaker(),
    title = `Message`,
    message = `Forgot something...`,
    img = '',
    chatCustomData = { rollMode: game.settings.get('core', 'rollMode') }
) {
    if (!targetUsers) {
        throw new Error('utilities.js, chatMessagePrivate() missing targetUsers');
    }
    console.log('utilities.js chatMessagePrivate', { targetUsers, speaker, title, message, img, chatCustomData });

    const imageHTML = img ? `<div class="a25-image"><img src="${img}" height="64"/></div>` : '';
    const chatData = {
        title: title,
        content: `<div><h2>${title}</h2></div>` + `${imageHTML}` + `<div>${message}</div>`,
        user: game.user.id,
        speaker: speaker,
        style: CONST.CHAT_MESSAGE_STYLES.WHISPER,
        whisper: targetUsers.map((u) => u.id),
        ...chatCustomData,
    };

    //use user current setting? game.settings.get("core", "rollMode")
    if (chatCustomData.rollMode) ChatMessage.applyRollMode(chatData, chatCustomData.rollMode);
    ChatMessage.create(chatData);
}
/**
 *
 * Simple message for SideVSide initiative rolls
 *
 * @param {*} speaker
 * @param {*} title
 * @param {*} message
 * @param {*} roll
 */
export async function chatMessageForSideVSideInitiative(speaker, title, roll) {
    const content = await renderTemplate('systems/ars/templates/chat/parts/chatCard-sidevside-roll.hbs', {
        title,
        roll,
    });

    let chatData = {
        title: title,
        content: content,
        user: game.user.id,
        rolls: [roll],
        rollMode: game.settings.get('core', 'rollMode'),
        speaker: speaker,
        style: game.ars.const.CHAT_MESSAGE_STYLES.OTHER,
    };
    //use user current setting? game.settings.get("core", "rollMode")
    // ChatMessage.applyRollMode(chatData, game.settings.get("core", "rollMode"));
    ChatMessage.create(chatData);
}

/**
 *
 * Return a itemList record from a item
 *
 * @param {*} itm Item Record
 * @returns { id: itm.id, uuid: itm.uuid, img: itm.img, name: itm.name, type: itm.type, quantity: itm.system.quantity }
 */
export function makeItemListRecord(itm) {
    if (itm) {
        return {
            id: itm.id,
            uuid: itm.uuid,
            img: itm.img,
            name: itm.name,
            type: itm.type,
            quantity: itm.system.quantity,
        };
    }
}

/**
 *
 * Take a time type (round|turn|hour|day) and convert the
 * count to seconds for duration management.
 *
 * @param {*} type
 * @param {*} count
 * @returns Integer (seconds)
 */
export function convertTimeToSeconds(count = 1, type = 'round') {
    // console.log("utilities.js convertTimeToSeconds", { count, type })
    let timeMultiplier = 60;
    switch (type) {
        // turn is 10 rounds
        case 'turn':
            timeMultiplier = 600;
            break;
        // hour is 6 turns
        case 'hour':
            timeMultiplier = 3600;
            break;
        // day is 24 hours
        case 'day':
            timeMultiplier = 86400;
            break;

        // default is round, 60 seconds
        default:
            timeMultiplier = 60;
            break;
    }

    // console.log("utilities.js convertTimeToSeconds count * timeMultiplier", count * timeMultiplier)
    const result = count ? count * timeMultiplier : undefined;
    return result;
}

/**
 *
 * return array of all items in all compendiums
 *
 * @param {String} objectType Default 'Item'
 * @param {Boolean} gmOnly  Default false
 * @returns
 */
export async function getPackItems(objectType = 'Item', isGM = false) {
    // console.log("utilities.js getPackItems", { objectType, isGM })
    const allItemPacks = game.packs.filter((i) => i.metadata.type === objectType);
    // console.log("utilities.js getPackItems", { allItemPacks }, typeof allItemPacks, allItemPacks.length)
    const maxPacks = allItemPacks.length;
    let packItems = [];
    let count = 0;

    SceneNavigation.displayProgressBar({
        label: `${game.i18n.localize('ARS.loadingItemBrowser')}`,
        // pct: ((count / allItemPacks.length) * 100).toFixed(0),
        pct: 1,
    });

    for (const pack of allItemPacks) {
        // console.log("utilities.js getPackItems", { pack }, pack.collection, pack.index.size)
        if (pack.visible) {
            // cap it at 99 so it shows until we're finished (see pct: 100 later)
            const percent = Math.min((count / maxPacks) * 100, 99).toFixed(0);

            SceneNavigation.displayProgressBar({
                label: `${game.i18n.localize('ARS.loadingItemBrowser')}: ${pack.title}`,
                pct: percent,
            });

            const items = await pack.getDocuments();
            console.log(`Loaded ${pack.title} ${objectType}:`, { items });
            packItems = packItems.concat(items);
        }
        count++;
    }

    SceneNavigation.displayProgressBar({
        label: `${game.i18n.localize('ARS.loadingItemBrowser')}`,
        pct: 100,
    });

    return packItems;
}

/**getPack
 *
 * Check for sale npc/lootable npc actor locks
 * If the player is no longer active, remove it.
 *
 * @param {*} sheet
 */
export async function cleanStaleSheetLocks(sheet) {
    const lockId = sheet.object.system.opened;
    if (lockId) {
        // console.log("utilities.js cleanStaleSheetLocks", { lockId });
        const lockedBy = game.users.get(lockId);
        // console.log("utilities.js cleanStaleSheetLocks", { lockedBy });
        if (!lockedBy || !lockedBy?.active) {
            await runAsGM({
                sourceFunction: 'cleanSheetLocks',
                operation: 'actorUpdate',
                user: game.user.id,
                targetTokenId: sheet.object.token.id,
                targetActorId: sheet.object?.token?.id ? undefined : sheet.actor.id,
                update: { 'system.opened': null },
            });
        }
    }
    console.log('utilities.js cleanStaleSheetLocks end');
}

/**
 *
 * Check to see if using speed to initiative, if they've already roll, then roll initiative
 *
 * @param {*} actor
 * @param {*} item
 * @returns
 */
export async function rollInitiativeWithSpeed(actor, item, manualRoll = false) {
    console.log('utilities.js rollInitiativeWithSpeed', {
        actor,
        item,
        manualRoll,
    });
    const initiativeUseSpeed = game.settings.get('ars', 'initiativeUseSpeed');
    if (initiativeUseSpeed && item && actor.getCombatant() && (!actor.initiative || (actor.initiative && manualRoll))) {
        const combatant = actor.getCombatant();
        // const combat = combatant?.combat;
        // // console.log(`rollInitiativeWithSpeed=>`, { combat, combatant });
        // // don't let them roll initiative with speed if round already started.
        // if (!combat.getFlag('ars', 'beginningOfRound')) {
        //     ui.notifications.warn('Round has already started, you can only use general initiative roll.');
        //     return false;
        // }
        let initSpeed = 0;
        switch (item.type) {
            case 'weapon':
                //TODO: Apply prof mods to speeds?
                initSpeed = parseInt(item.system.attack.speed) || 0;
                break;
            case 'spell':
                const spellCastInit = parseInt(item.system.castingTime);
                if (Number(item.system.castingTime) || spellCastInit === 0) initSpeed = spellCastInit;
                // assume its 1 round, turn/hour/etc so it doesn't really need a speed cept last.
                else initSpeed = 99;
                break;

            default:
                return false;
                break;
        }
        await rollCombatantInitiative(combatant, combatant.combat, event.ctrlKey, {
            initSpeedMod: initSpeed,
            useSpeed: true,
            useWeapon: item.type === 'weapon',
            useSpell: item.type === 'spell',
            casting: item.type === 'spell',
            question: 'Modifier',
            title: 'Initiative',
            flavor: `Initiative for ${item.system.attributes.identified ? item.name : item.alias ? item.alias : 'weapon'}`,
            action: `${item.type === 'spell' ? 'Casting' : 'Attack with'} ${
                item.system.attributes.identified ? item.name : item.alias ? item.alias : 'weapon'
            }`,
            img: item.img,
            item: item,
        });
        return true;
    }
    return false;
}

/**
 * Roll initiative. Check turn, check values, check for situation dialog() then roll
 *
 * @param {*} combatant
 * @param {*} combat
 * @param {*} ctrlKey
 * @param {*} initOptions
 * @returns
 */
export async function rollCombatantInitiative(
    combatant,
    combat,
    ctrlKey = false,
    initOptions = {
        initSpeedMod: 0,
        useSpeed: false,
        useWeapon: false,
        useSpell: false,
        casting: false,
        question: 'Modifier',
        title: 'Initiative',
        flavor: 'Rolling Initiative',
        formula: undefined,
        action: game.i18n.localize('ARS.dialog.actionDefault'),
        img: 'icons/svg/d10-grey.svg',
        item: undefined,
    }
) {
    console.log('utilities.js rollCombatantInitiative', {
        combatant,
        combat,
        ctrlKey,
        initOptions,
    });
    // const initSideVSide = game.settings.get('ars', 'initSideVSide');
    const beginningOfRound = combat.getFlag('ars', 'beginningOfRound');

    // if (!game.user.isGM && (combatant.initiative !== null || combat.turn !== 0)) {
    if (!game.user.isGM && combatant.initiative !== null) {
        ui.notifications.warn(`You cannot roll initiative more than once.`);
        return;
    }
    const dialogOptions = { actionText: initOptions.action };
    const init = ctrlKey
        ? {
              mod: 0,
              rollMode: initOptions?.rollMode ? initOptions.rollMode : '',
          }
        : await dialogManager.getInitiative(
              initOptions.question,
              initOptions.title,
              initOptions.flavor,
              initOptions.casting,
              dialogOptions
          );

    const currentInitiative = combat._getCurrentTurnInitiative();
    const startingTurnIndex = combat.turn;

    if (isNaN(init.mod)) return null;
    if (init.casting) {
        combatant.setFlag('world', 'initCasting', true);
    }
    const initFormula = combatant._getInitiativeFormula();
    let formula = initOptions.formula ? initOptions.formula : `${initFormula}`;
    // do this incase they adjusted.
    if (init.action) {
        initOptions.action = init.action;
    }

    if (initOptions.useSpeed) {
        // formula += ` + ${initOptions.initSpeedMod}`;
        formula += ` + ${initOptions.useWeapon ? '@speedfactor' : '@casttime'}`;
        if (initOptions.useWeapon) {
            //store some values and reuse during automated rolls as "default" weapon
            combatant.token.setFlag('world', 'lastInitiativeFormula', formula);
            combatant.token.setFlag('world', 'lastInitiativeMod', initOptions.initSpeedMod);
            combatant.token.setFlag('world', 'init.action', initOptions.action);
        }
    }

    if (initOptions?.item) {
        const itemInitFormula = initOptions.item.system?.attack?.speedmod;
        if (itemInitFormula) formula += ` + ${itemInitFormula}`;
    }

    if (combatant.actor?.system?.mods?.initiative) formula += ' + @mods.initiative';
    if (combatant.actor.initiativeModifier) formula += ' + @initStatusMod';
    // if (init.mod) formula += ` + ${init.mod}`;
    if (init.mod) formula += ` + @situational`;
    if (!beginningOfRound && combat.turn && currentInitiative) {
        // formula += ` +${currentInitiative + 1}`;
        formula += ` + @initOutOfTurn`;
    } else if (!beginningOfRound && combat.turn && !currentInitiative) {
        ui.notifications.warn(`${combatant?.actor?.name} rolled after round started.`);
        console.warn(`${combatant?.actor?.name} rolled after round started?`, { combat, currentInitiative }, beginningOfRound);
        // formula += ` +${combat._getLastInInitiative() + 1}`;
        formula += ` + @initLate`;
    }
    console.log('utilities.js rollCombatantInitiative', { formula, combat, combatant });
    // await combat.rollInitiative([combatant.id], {
    //     formula: formula,
    //     updateTurn: !startingTurnIndex,
    //     messageOptions: { rawformula: formula, flavor: initOptions.flavor, rollMode: init.rollMode },
    // });
    const rollData = foundry.utils.mergeObject(combatant.actor?.getRollData(), {
        speedfactor: initOptions.initSpeedMod,
        casttime: initOptions.initSpeedMod,
        situational: init.mod,
        initOutOfTurn: beginningOfRound ? 0 : currentInitiative + 1,
        initLate: combat._getLastInInitiative() + 1,
    });
    await combat.rollInitiativeIndividual(combatant, {
        formula: formula,
        updateTurn: !startingTurnIndex || beginningOfRound,
        rollData: rollData,
        messageOptions: { img: initOptions.img, rawformula: formula, flavor: initOptions.flavor, rollMode: init.rollMode },
    });
    if (initOptions.action) {
        combatant.setFlag('world', 'init.action', initOptions.action);
    }
    // dont update turn if combat is already progressed past first person
    if (startingTurnIndex && !beginningOfRound) {
        if (combatant.index < startingTurnIndex) {
            console.log('utilities.js rollCombatantInitiative', 'SET TO :', combatant.index);
            await combat.update({ turn: combatant.index });
        }
    } else {
        console.log('utilities.js rollCombatantInitiative', 'SET TO DEFAULT:', 0);
        await combat.update({ turn: 0 });
    }
    // return combat.turn ? combat : await combat.update({ turn: 0 });
    if (combatant.actor?.sheet?.rendered && combatant.actor.isOwner) combatant.actor.sheet.render();
    return combat;
}

/**
 *
 * Debug code to see what total CP value before/after resulted in to check math on calculateCoins()
 *
 * @param {*} availableCurrency
 * @returns
 */
function getCurrentCPTotal(availableCurrency) {
    const variant = game.ars.config.settings.systemVariant;
    const currencyBaseExchange = ARS.currencyValue[variant];
    let totalAvailable = 0;
    for (let currency in availableCurrency) {
        totalAvailable += availableCurrency[currency] * currencyBaseExchange[currency];
    }

    return totalAvailable;
}

/**
 *
 * Calculate copper base from current carried coins
 * Calculate copper base for costAmount
 *
 * @param {*} availableCurrency
 * @param {*} costAmount
 * @param {*} costCurrency
 * @returns { avaliable: totalAvailable, costBase: costInBaseValue, canPurchase: (totalAvailable < costInBaseValue) }
 */
export function calculateCopperBase(availableCurrency, costAmount, costCurrency) {
    const variant = game.ars.config.settings.systemVariant;
    const currencyBaseExchange = ARS.currencyValue[variant];

    // Convert the cost amount and currency type to base value
    let costInBaseValue = costAmount * currencyBaseExchange[costCurrency];

    // Calculate the total available currency in base value
    let totalAvailable = 0;
    for (let currency in availableCurrency) {
        totalAvailable += availableCurrency[currency] * currencyBaseExchange[currency];
    }

    return {
        avaliable: totalAvailable,
        costBase: costInBaseValue,
        currencyType: costCurrency,
        canBuy: totalAvailable >= costInBaseValue,
    };
}

/**
 *
 * Pay for something using costInBase (copper value)
 *
 * adjust quantities of carried currency or
 * delete carried currency item until costInBase has been paid.
 *
 * Pay a specified amount from an actor's available currencies.
 *
 * @param {Object} actor - The actor from whom the amount will be deducted.
 * @param {number} costInBase - The total cost in base currency (copper).
 * @param {string} costCurrency - The currency type for the cost.
 * @return {Object} An object containing the currencies spent and change remaining.
 * @returns { spent: spentCurrency, change: changeGiven }
 */
export async function payCopperBase(actor, costInBase, costCurrency) {
    // Get the variant and currency exchange rates from the game settings
    const variant = game.ars.config.settings.systemVariant;
    const currencyBaseExchange = ARS.currencyValue[variant];
    const carriedCurrency = actor.carriedCurrencyItems;

    // Initialize lists for updates and deletions
    const updateList = [];
    const deleteList = [];

    // Initialize counters for spent and change
    let spentCurrency = { cp: 0, sp: 0, ep: 0, gp: 0, pp: 0 };
    let change = { cp: 0, sp: 0, ep: 0, gp: 0, pp: 0 };

    // Initialize remaining cost and change variables
    let costRemaining = costInBase;
    let changeGiven = 0;

    // Loop through each type of currency from lowest to highest value
    for (const currencyOrder of Object.keys(currencyBaseExchange)) {
        // Loop through each carried currency item to find matching types
        for (const coinItem of carriedCurrency) {
            const currencyType = coinItem.system.cost.currency.toLowerCase();

            // Skip if the current item's currency type does not match
            if (currencyOrder !== currencyType) continue;

            // Calculate the item's total value in base currency
            const quantity = parseInt(coinItem.system.quantity) || 0;
            const itemBaseValue = quantity * currencyBaseExchange[currencyType];

            // Handle cases where the item's value can be fully used for payment
            if (itemBaseValue <= costRemaining && itemBaseValue >= currencyBaseExchange[costCurrency]) {
                spentCurrency[currencyType] += quantity;
                costRemaining -= itemBaseValue;
                deleteList.push(coinItem.id);

                // Handle cases where only part of the item's value is needed
            } else if (itemBaseValue > costRemaining) {
                const remaining = itemBaseValue - costRemaining;
                const remainingBase = remaining % currencyBaseExchange[currencyType];
                const correctChangeRemaining = Math.floor(remaining / currencyBaseExchange[currencyType]);

                spentCurrency[currencyType] += Math.ceil((itemBaseValue - remaining) / currencyBaseExchange[currencyType]);
                if (remainingBase) changeGiven += remainingBase;
                costRemaining = 0;

                // Add to update list if some quantity remains, else mark for deletion
                if (correctChangeRemaining) {
                    updateList.push({
                        item: coinItem,
                        quantity: correctChangeRemaining,
                    });
                } else {
                    deleteList.push(coinItem.id);
                }
            }
        }
    }

    // Apply all the queued updates and deletions
    for (const { item, quantity } of updateList) {
        await item.update({ 'system.quantity': quantity });
    }
    if (deleteList.length) {
        await actor.deleteEmbeddedDocuments('Item', deleteList);
    }

    // Calculate change if any
    if (changeGiven) change = await giveCurrency(actor, changeGiven);

    return { spent: spentCurrency, change };
}

/**
 *
 * Give currency to actor of amount copperBaseGiven as copperBase
 * Will attempt to give highest value currency first looking for
 * existing currency items of those types, if they do not exist
 * will create
 *
 * @param {*} actor
 * @param {*} copperBaseGiven
 *
 * @returns { change: change }
 *
 */
export async function giveCurrency(actor, copperBaseGiven) {
    const variant = game.ars.config.settings.systemVariant;
    const currencyBaseExchange = ARS.currencyValue[variant];
    const currencies = Object.keys(currencyBaseExchange);
    const change = {
        cp: 0,
        sp: 0,
        ep: 0,
        gp: 0,
        pp: 0,
    };
    let remainingChange = copperBaseGiven;
    const currencyItems = [];

    // Loop through each currency from highest to lowest
    for (let i = currencies.length - 1; i >= 0; i--) {
        const currencyOrder = currencies[i];
        let count = Math.floor(remainingChange / currencyBaseExchange[currencyOrder]);
        // const leftOver = remainingChange % currencyBaseExchange[currencyOrder];
        if (count > 0) {
            // Decrease the remaining change
            remainingChange -= count * currencyBaseExchange[currencyOrder];
            // Try to update the actor's existing currency items
            const coinItem = actor.carriedCurrencyItems.find((coinItem) => {
                const currencyType = coinItem.system.cost.currency.toLowerCase();
                return currencyOrder === currencyType;
            });
            if (coinItem) {
                // update existing coin item
                const quantity = parseInt(coinItem.system.quantity) || 0;
                const newQuantity = quantity + count;
                await coinItem.update({ 'system.quantity': newQuantity });
            } else {
                // create coin item
                change[currencyOrder] += Math.ceil(count);
                const currencyName =
                    game.i18n.localize(`ARS.currency.short.${currencyOrder}`) + ` ${game.i18n.localize('ARS.coins')}` ||
                    'Change';
                const currencyWeight = 1 / ARS.currencyWeight[variant];
                const currencyData = {
                    name: currencyName,
                    type: 'currency',
                    img: ARS.icons.general.currency[currencyOrder],
                    system: {
                        quantity: count,
                        weight: currencyWeight,
                        cost: {
                            currency: currencyOrder,
                        },
                    },
                };
                currencyItems.push(currencyData);
            }
        }
    }

    if (currencyItems.length > 0) {
        await actor.createEmbeddedDocuments('Item', currencyItems, {
            hideChanges: true,
        });
    }

    return { ...change };
}

/**
 *
 * Escape any regular expression characters in string
 *
 * @param {*} string
 * @returns
 */
export function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}

/**
 *
 * Find a item name/type match in current inventory of actor if it exists
 *
 * finds itemToMatch.name split up by space or comma to see if its in the item.name
 * of the actors inventory of items
 *
 * @param {*} targetActor actor these items exist on
 * @param {*} itemToMatch item object to find similar
 * @param {*} itemTypes  Filter itemToMatch types out (optional)
 * @param {*} matchToType Only compare itemToMatch against items of this type (optional) i.e. look for a name of weapon itemToMatch in proficiency item names
 * @returns
 */
export function findSimilarItem(targetActor, itemToMatch, itemTypes = ['item', 'weapon'], matchToType = null) {
    const MIN_WORD_LENGTH = 3;
    const ignoreAttributeTypes = [
        // "alchemical",
        'ammunition',
        // "animal",
        // "art",
        // "clothing",
        'daily food and lodging',
        // "equipment packs",
        // "gear",
        // "gem",
        // "jewelry",
        'provisions',
        // "scroll",
        // "service",
        'herb or spice',
        // "tack and Harness",
        'tool',
        // "transport",
        'other',
    ];

    // console.log("utilities.js findSimilarItem", { targetActor, itemToMatch, itemTypes, matchToType });

    //if this isnt a item type valid to look for, return not found
    if (
        (itemTypes.length && !itemTypes.includes(itemToMatch.type)) ||
        (itemToMatch?.system?.attributes?.magic && !itemToMatch?.system?.attributes?.identified)
    ) {
        return undefined;
    }

    // if only matching a item, only look at specific attribute types, not everything
    if (
        !matchToType &&
        !['type', 'subtype'].some((attr) =>
            ignoreAttributeTypes.includes(itemToMatch?.system?.attributes?.[attr]?.toLowerCase() ?? '')
        )
    ) {
        return undefined;
    }

    function matchWords(nameWord, queryWord) {
        const sanitizedName = escapeRegExp(nameWord);
        const sanitizedQuery = escapeRegExp(queryWord);
        const nameRegex = new RegExp(`\\b${sanitizedName}\\b`, 'i');
        const queryRegex = new RegExp(`\\b${sanitizedQuery}\\b`, 'i');
        return nameRegex.test(sanitizedQuery) || queryRegex.test(sanitizedName);
    }

    function escapeRegExp(str) {
        return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
    }

    /**search for every piece of item.name in every part of the object name in the list of objects     */
    function searchObjectsByName(queryItem, objectList) {
        const ignoreWords = ['and', 'art', 'day', 'etc', 'from', 'for', 'gem', 'level', 'night', 'per', 'the', 'tun', 'was'];
        const escapedQuery = escapeRegExp(queryItem.name);
        const filteredList = objectList.filter((obj) => {
            const { id, type, name } = obj;
            if (queryItem.id === id) return false;

            const nameWords = name
                .toLowerCase()
                .split(/[\s,]+/)
                .filter((w) => w.length >= MIN_WORD_LENGTH && !ignoreWords.includes(w));
            const queryWords = escapedQuery
                .toLowerCase()
                .split(/[\s,]+/)
                .filter((w) => w.length >= MIN_WORD_LENGTH && !ignoreWords.includes(w));
            const matchType = matchToType ? type === matchToType : queryItem.type === type;

            return matchType && queryWords.some((qWord) => nameWords.some((nWord) => matchWords(nWord, qWord)));
        });

        return filteredList;
    }

    const foundItems = searchObjectsByName(itemToMatch, targetActor.items);

    // console.log("utilities.js findSimilarItem", { foundItems });
    return foundItems;
}

/**
 *
 * Fix double spacing.
 * Remove end of line when ending in -CR
 * Remove CR on lines not ending with .
 * Replace EOL with <p/>
 *
 * @param {*} text
 * @returns
 */
export function textCleanPaste(text) {
    let newText = text;
    // remove double space
    newText = newText.replace(/ {2,}/g, ' ');
    // remove double space
    newText = newText.replace(/[\s]+[\r\n]/g, '\r\n');
    // unwrap lines ending in -
    newText = newText.replace(/-(?:\s*[\n\r])/g, '');
    // remove CRs for lines w/o .
    newText = newText.replace(/(?<!\.|\.\s)[\n\r]/g, ' ');
    // replace EOL with <p/>
    newText = newText.replace(/[\n\r]+/g, '<p/>');
    // clean up highbit chars and replace with ascii
    newText = this.replaceHighBitChars(newText);

    return newText;
}

/**
 *
 * Swap out odd characters for matching ascii
 *
 * @param {*} inputString
 * @returns
 */
export function replaceHighBitChars(inputString) {
    // Define a lookup table for high-bit characters and their ASCII equivalents
    const highBitToAsciiMap = {
        Á: 'A',
        á: 'a',
        Â: 'A',
        â: 'a',
        Ã: 'A',
        ã: 'a',
        Ä: 'A',
        ä: 'a',
        Å: 'A',
        å: 'a',
        Æ: 'AE',
        æ: 'ae',
        Ç: 'C',
        ç: 'c',
        È: 'E',
        è: 'e',
        É: 'E',
        é: 'e',
        Ê: 'E',
        ê: 'e',
        Ë: 'E',
        ë: 'e',
        Ì: 'I',
        ì: 'i',
        Í: 'I',
        í: 'i',
        Î: 'I',
        î: 'i',
        Ï: 'I',
        ï: 'i',
        Ð: 'D',
        ð: 'd',
        Ñ: 'N',
        ñ: 'n',
        Ò: 'O',
        ò: 'o',
        Ó: 'O',
        ó: 'o',
        Ô: 'O',
        ô: 'o',
        Õ: 'O',
        õ: 'o',
        Ö: 'O',
        ö: 'o',
        Ø: 'O',
        ø: 'o',
        Ù: 'U',
        ù: 'u',
        Ú: 'U',
        ú: 'u',
        Û: 'U',
        û: 'u',
        Ü: 'U',
        ü: 'u',
        Ý: 'Y',
        ý: 'y',
        Þ: 'P',
        þ: 'p',
        ß: 's',
        Œ: 'OE',
        œ: 'oe',
        Š: 'S',
        š: 's',
        Ÿ: 'Y',
        ž: 'z',
        Ž: 'Z',
        ƒ: 'f',
        '—': '--', // Add em dash to its ASCII equivalent
    };

    // Replace high-bit characters with their ASCII equivalents
    const asciiString = inputString.replace(/[^\x00-\x7F]/g, (char) => highBitToAsciiMap[char] || char);

    return asciiString;
}

/**
 *
 * If line starts with Word: add <h3>Word</h3>
 * Find any dice roll and add [[//roll diceRoll]]
 *
 * @param {*} text
 * @returns
 */
export function textAddSimpleMarkup(text) {
    let newText = text;

    // remove period from vs. and vrs.
    function replaceVsAndVrs(text) {
        const regex = /(vs|vrs)[.]/gi;
        return text.replace(regex, (match) => match.slice(0, -1));
    }

    newText = replaceVsAndVrs(newText);

    // replace Word: with <h3>word</h3>
    newText = newText.replace(/<p\/>([\w\/]+):/gim, (match, word) => `<h2>${word}</h2>`);
    // replace dice roll strings with inline roll
    // newText = newText.replace(/(\d+)?d(\d+)([+-]\d+)?/gm, (match) => `[[/roll ${match}]]`);
    newText = newText.replace(
        /(?<!\[\[?\/roll [^\]]*?)(\d+)?d(\d+)([+-]\d+)?(?![^\[]*?\]?])/gm,
        (match) => `[[/roll ${match}]]`
    );
    newText = textAddBoldTags(newText, ARS.markupPhrases);

    return newText;
}

export function textAddBoldTags(text, phrases) {
    const regexPhrases = phrases.join('|');
    const regex = new RegExp(`[^.!?]*(?:${regexPhrases})[^.!?]*[.!?]`, 'gi');

    return text.replace(regex, (match) => `<b>${match}</b>`);
}

// This function converts a range into a dice roll string (e.g., '2d6+1')
// range: a string representing a range of possible values (e.g., '1-6')

/**
 * This function converts a range into a dice roll string (e.g., '2d6+1')
 *
 * @param {String} range: a string representing a range of possible values (e.g., '1-6')
 * @returns String
 */
export function convertToDiceRoll(range) {
    // Split the range into min and max, then convert them to numbers
    const [min, max] = range.split('-').map(Number);

    // Array of valid dice types (number of sides)
    const validDiceTypes = [2, 4, 6, 8, 10, 12, 20, 100];

    // Check if the range is valid (max should be greater or equal to min)
    if (max < min) {
        throw new Error('Invalid range: Max value should be greater than or equal to Min value');
    }

    // Calculate the difference between max and min values
    const difference = max - min + 1;

    // Initialize variables for optimal dice roll configuration
    let numberOfSides = difference;
    let numberOfDice = 1;
    let bestDifference = Number.MAX_SAFE_INTEGER;
    let bestModifier = 0;

    // Iterate through each dice type to find the optimal configuration
    for (let dice of validDiceTypes) {
        // Iterate through possible dice counts
        for (let count = 1; count <= difference; count++) {
            // Iterate through possible modifiers
            for (let modifier = 0; modifier < dice; modifier++) {
                // Calculate min and max values for the current configuration
                const minValue = count + modifier;
                const maxValue = count * dice + modifier;
                const currentDifference = Math.abs(maxValue - minValue + 1 - difference);

                // Check if the current configuration satisfies the input range
                // and has a smaller difference than the best configuration found so far
                if (min >= minValue && max <= maxValue && currentDifference < bestDifference) {
                    bestDifference = currentDifference;
                    numberOfSides = dice;
                    numberOfDice = count;
                    bestModifier = modifier;
                }
            }
        }
    }

    // Generate the modifier string based on the bestModifier value
    const modifierString = bestModifier > 0 ? `+${bestModifier}` : '';

    // Return the dice roll string with the optimal configuration
    return `${numberOfDice}d${numberOfSides}${modifierString}`;
}

// This function calculates the effective level of a character based on hit dice (HD)
// hitDice: the hit dice string (e.g., '1d6+2')
export function effectiveLevel(hitDice) {
    // Helper function to extract hit dice value and modifier from the hit dice string
    function helperGetHitDice(hitDice) {
        try {
            const hitDiceString = String(hitDice);
            const [, hitDiceValue, , modifierType, modifierValue] = hitDiceString?.match(/^(\d+)(([+\-])(\d+))?/);

            if (hitDiceValue) {
                const parsedHitDiceValue = parseInt(hitDiceValue);
                const parsedModifierType = modifierType || '';
                const parsedModifierValue = modifierValue || 0;
                const calculatedModifierValue = parseInt(`${parsedModifierType}${parsedModifierValue}`) || 0;

                return [parsedHitDiceValue, calculatedModifierValue];
            }
        } catch (err) {
            ui.notifications.error(`Error in utilities.js effectiveLevel() ${err}`);
            return [1, 0];
        }
        return [1, 0];
    }

    // Get the hit dice and modifier value
    const [hdValue, modValue] = helperGetHitDice(hitDice);
    const variant = ARS.settings.systemVariant;
    let effectiveHD = -1;

    // Handle different system variants (0 for OSRIC, others for different versions)
    if (Number(variant) === 0) {
        if (hdValue > 0 && modValue > 0) {
            effectiveHD = hdValue + 2;
        } else if (hdValue === 1 && modValue === -1) {
            effectiveHD = hdValue;
        } else if (hdValue === 1 && modValue < -1) {
            effectiveHD = 0;
        } else if (hdValue > 0) {
            effectiveHD = hdValue + 1;
        } else {
            effectiveHD = hdValue;
        }
    } else {
        const bonusHD = Math.floor(modValue / 4);
        effectiveHD = hdValue + bonusHD + (modValue > 0 ? 1 : modValue < 0 ? -1 : 0);
    }

    // Ensure the effective HD is within the valid range of 0 to 21
    effectiveHD = Math.min(Math.max(effectiveHD, 0), 21);

    // Return the effective HD
    return effectiveHD;
}

/**
 * Converts a given string into a format safe for use as an object key.
 *
 * @param {string} str The string to be converted.
 * @return {string} A string safe for use as an object key.
 */
export function safeKey(str) {
    // Replace spaces with underscores and remove special characters
    str = str;
    // let key = str.slugify({ strict: true });
    let key = str
        .replace(/([_\-\.])([a-z])/g, (match, separator, nextChar) => separator + nextChar.toUpperCase()) // Capitalize character following hyphen/dot/underscore
        .replace(/\s+/g, '-') // Replace spaces with underscores
        .replace(/[^\w\-]+/g, '') // Remove all non-word chars
        .replace(/\-\-+/g, '-') // Replace multiple hyphens with single underscore
        .replace(/^-+/, '') // Trim hyphens from the start
        .replace(/-+$/, '');

    // Ensure the key does not start with a digit
    if (/^\d/.test(key)) {
        key = '_' + key;
    }

    // Convert to camelCase
    key = key.replace(/_([a-z])/g, function (g) {
        return g[1].toUpperCase();
    });

    return key;
}

/**
 *
 * This takes a dice string like 2-5 and figures out a way
 * to make it a dice roll (2d3-1, 1d4+1/etc)
 *
 * Old NPC entries have this ;(
 *
 * @param {String} diceString
 * @returns
 */
export function diceFixerOLD(diceString) {
    let fixedRoll = '';
    const rollValues = diceString?.match(/^(\d+)[\-](\d+)$/);
    let nCount = rollValues[1];
    let nSize = rollValues[2];
    if (nCount === 1) {
        fixedRoll = `${nCount}d${nSize}`;
    } else {
        const nSizeAdjusted = Math.floor(nSize / nCount);
        const nRemainder = nSize % nCount; // nSize - (nSizeAdjusted * nCount);

        if (nCount > nSizeAdjusted) {
            fixedRoll = `${nSizeAdjusted}d${nCount}`;
        } else {
            fixedRoll = `${nCount}d${nSizeAdjusted}`;
        }

        if (nRemainder > 0) {
            fixedRoll += `+ ${nRemainder}`;
        } else if (nRemainder < 0) {
            fixedRoll += `${nRemainder}`;
        }
    }

    return fixedRoll;
}
export function diceFixer(rangeStr) {
    const [min, max] = rangeStr.split('-').map(Number);
    if (isNaN(min) || isNaN(max) || min >= max) {
        throw new Error('Invalid range string. Ensure it is in the format "min-max" with min < max.');
    }

    const range = max - min + 1;
    let diceRoll = '';

    if (range <= 6) {
        diceRoll = `1d${range} + ${min - 1}`;
    } else {
        const diceCombinations = [
            { sides: 4, count: Math.ceil(range / 4) },
            { sides: 6, count: Math.ceil(range / 6) },
            { sides: 8, count: Math.ceil(range / 8) },
            { sides: 10, count: Math.ceil(range / 10) },
            { sides: 12, count: Math.ceil(range / 12) },
            { sides: 20, count: Math.ceil(range / 20) },
        ];

        for (const combo of diceCombinations) {
            if (combo.sides * combo.count >= range) {
                diceRoll = `${combo.count}d${combo.sides} + ${min - 1}`;
                break;
            }
        }
    }

    return diceRoll;
}

/**
 *
 * Look at the trigger effects on source and see if they are triggered because of opponent
 *
 * @param {*} source actorObject
 * @param {*} opponent actorObject
 * @param {String} directionType 'target' or 'attacker' from target.alignment or attacker.alignment
 * @param {String} combatType null for all, melee,ranged,thrown or save types such as spell,breath/etc
 * @param {String} type attack, damage, save, magicpotency
 * @param {*} sourceItem
 * @returns
 */
export async function getTriggerFormula(
    source,
    opponent,
    directionType = 'target',
    combatType = undefined,
    type = 'attack',
    sourceItem
) {
    let rollData = [],
        bonusFormula = [];

    /** update rollData/bonusFormula */
    async function _updateFormulas(effect, triggerType, details) {
        const formulaKey = safeKey(`effect.${effect?.name ? effect.name : 'trigger'}.${directionType}.${triggerType}`);
        const formulaTag = `@${formulaKey}`;
        if (!bonusFormula.includes(formulaTag)) bonusFormula.push(formulaTag);
        if (typeof rollData[formulaKey] !== 'number') {
            rollData[formulaKey] = 0;
        }
        const formulaResult = await evaluateFormulaValue(details.formula, source.getRollData());
        rollData[formulaKey] += parseInt(formulaResult) || 0;
        console.log('_updateFormulas', { rollData });
    }
    /**
     * generate and set formula for this trigger
     * @param {Object} effect
     * @param {String} triggerType alignment, type, distance etc...
     * @param {Object} details
     */
    async function _setFormula(effect, triggerType, details) {
        //attack and damage details.type
        if (details.type.startsWith('damage') && details.type.startsWith(type)) {
            await _updateFormulas(effect, triggerType, details);
        } else if (combatType && details.type.startsWith('attack') && details.type.startsWith(type)) {
            const checkType = `${type}.${combatType}`;
            switch (checkType) {
                //attack.melee, attack.ranged, attack.thrown, damage.melee, damage.ranged, ramage.thrown
                case `${type}.melee`:
                case `${type}.ranged`:
                case `${type}.thrown`:
                    {
                        if (details.type == type || details.type.endsWith(combatType)) {
                            await _updateFormulas(effect, triggerType, details);
                        }
                    }
                    break;

                default:
                    // attack or damage, use for all
                    {
                        if (details.type == type) _updateFormulas(effect, triggerType, details);
                    }
                    break;
            }
            //save details.type
        } else if (details.type.startsWith('save') && details.type.startsWith(type)) {
            const saveTypes = details?.saveTypes
                ?.toLowerCase()
                ?.split(',')
                ?.map((text) => text.trim()) || ['all'];

            if (saveTypes.length && (saveTypes.includes(combatType) || saveTypes.includes('all'))) {
                await _updateFormulas(effect, triggerType, details);
            }
            //save details.type
        } else if (details.type.startsWith('magicpotency') && details.type.startsWith(type)) {
            await _updateFormulas(effect, triggerType, details);
        } else {
            // ui.notifications.warn(`utilities.js getTriggerFormula() _setFormula ${details.type}`);
            console.log('utilities.js getTriggerFormula _setFormula unknown details.type', { details });
        }
    }

    /**
     * test triggers
     * @param {*} effect
     * @param {*} triggerType
     * @param {*} details
     */
    async function _testTrigger(effect, triggerType, details) {
        switch (triggerType) {
            case 'alignment':
                const alignments = details.trigger
                    .toLowerCase()
                    .split(',')
                    .map((text) => text.trim());
                if (alignments.includes(opponent.system.details.alignment)) {
                    await _setFormula(effect, triggerType, details);
                }
                break;

            case 'type':
                let types = opponent.type === 'character' ? opponent.types : undefined;
                if (opponent.type !== 'character') {
                    if (opponent.system.details.type) {
                        types = opponent.system.details.type
                            .toLowerCase()
                            .split(',')
                            .map((text) => text.trim());
                    }
                }
                if (types?.length) {
                    const triggerTypes = details.trigger
                        ?.toLowerCase()
                        .split(',')
                        .map((text) => text.trim());
                    const foundTypeMatch = triggerTypes?.some((typeTrigger) => {
                        return types.includes(typeTrigger);
                    });
                    if (foundTypeMatch) await _setFormula(effect, triggerType, details);
                }
                break;

            case 'size':
                const sizes = details.trigger
                    .toLowerCase()
                    .split(',')
                    .map((text) => text.trim());
                if (sizes.includes(opponent.system.attributes.size)) {
                    await _setFormula(effect, triggerType, details);
                }
                break;

            case 'distance':
                // console.log("opponent.js getTriggerFormula", { thisActor, actor, triggerType, details, details.trigger })
                // const distances = details.trigger.split(',').map((text) => text.trim());
                const [minDistanceStr, maxDistanceStr] = details.trigger.split(',').map((text) => text.trim());
                const minValue = parseInt(minDistanceStr || 0);
                const maxValue = parseInt(maxDistanceStr || 1);
                const distance = source.getToken().getDistance(opponent.getToken());
                if (distance <= maxValue && distance >= minValue) {
                    await _setFormula(effect, triggerType, details);
                }
                break;

            case 'always':
                await _setFormula(effect, triggerType, details);
                break;

            case 'hitdice':
                console.log('utilities.js getTriggerFormula TODO', {
                    triggerType,
                    details,
                });
                break;

            case 'weapontype':
                console.log('utilities.js getTriggerFormula TODO', {
                    triggerType,
                    details,
                });
                break;

            case 'properties_all':
                {
                    const actorPropertiesSanitized = Object.values(opponent.system?.properties).map((value) =>
                        value.toLowerCase().trim()
                    );
                    const propsSearch = details.trigger
                        .toLowerCase()
                        .split(',')
                        .map((text) => text.trim());
                    if (propsSearch.every((prop) => actorPropertiesSanitized.includes(prop))) {
                        await _setFormula(effect, triggerType, details);
                    }
                }
                break;
            case 'properties_any':
                {
                    const actorPropertiesSanitized = Object.values(opponent.system?.properties).map((value) =>
                        value.toLowerCase().trim()
                    );
                    const propsSearch = details.trigger
                        .toLowerCase()
                        .split(',')
                        .map((text) => text.trim());
                    if (propsSearch.some((prop) => actorPropertiesSanitized.includes(prop))) {
                        await _setFormula(effect, triggerType, details);
                    }
                }
                break;
            // case 'properties_missing': {
            //   const actorPropertiesSanitized = Object.values(opponent.system?.properties).map(value => value.toLowerCase().trim());
            //   const propsSearch = details.trigger.toLowerCase().split(',').map(text => text.trim());
            //   console.log("utilities.js getTriggerFormula TODO", { triggerType, details, actorPropertiesSanitized, propsSearch })
            //   if (actorPropertiesSanitized.every(prop => !propsSearch.includes(prop))) {
            //     _setFormula(effect, triggerType, details);
            //   }
            // }
            //   break;

            default:
                ui.notifications.warn(`utilities.js _processEffectTests() unknown triggerType (${triggerType})`);
                console.log('utilities.js _processEffectTests() unknown triggerType', { triggerType });
                break;
        } // end switch triggerType
    }

    /** start of function */
    if (source && opponent) {
        /**
         * Creates an array of effect data objects, each with a single change based on provided key-value pairs.
         * We build this so that the conditionals that are just key/value can be iterated over properly
         *
         * @param {Array} changesArray - An array of objects with key and value properties.
         * @returns {Array} An array of effect data objects.
         */
        async function helper_createEffectsFromConditionals(changesArray) {
            if (!changesArray) return [];
            return changesArray.map((change) => {
                return {
                    label: change.name ? change.name : 'Proficiency Conditional Trigger',
                    img: 'icons/svg/sword.svg', // Customize the icon as needed
                    changes: [{ key: change.key, mode: CONST.ACTIVE_EFFECT_MODES.CUSTOM, value: change.value }],
                };
            });
        }

        const sourceEffects = source.getActiveEffects(sourceItem);
        const conditionalEffects = sourceItem ? await helper_createEffectsFromConditionals(sourceItem.conditionals) : [];

        const effectList = conditionalEffects?.length ? sourceEffects.concat(conditionalEffects) : sourceEffects;
        for (const effect of effectList) {
            for (const change of effect?.changes) {
                try {
                    const details = JSON.parse(change.value);
                    //target.alignment to [target, alignement]
                    //^direction ^
                    //            \-triggerType
                    const triggers = change.key
                        .toLowerCase()
                        .split('.')
                        .map((text) => text.trim());
                    // [target, alignement]
                    // direction = target
                    // triggerType = alignment
                    const [direction, triggerType] = triggers;
                    if (direction == directionType) {
                        // console.log('----------->', { directionType, direction, effect, change, details });
                        await _testTrigger(effect, triggerType, details);
                    }
                } catch (err) {}
            } // end changes
        } // end all effects

        if (bonusFormula.length) {
            return { formula: bonusFormula, rollData };
        }
    }
    return undefined;
}

/**
 * Refresh actor sheets that are opened
 *
 */
export function refreshAllActorSheetsOpened() {
    for (let key in ui.windows) {
        const app = ui.windows[key];
        // Check if the window is an instance of ActorSheet
        if (app instanceof ARSCharacterSheet || app instanceof ARSNPCSheet) {
            // Re-render the actor sheet
            app.render(true);
        }
    }
}

/**
 *
 * Pass text in, returns an array of all unique words in lower case
 *
 * @param {*} text
 * @returns {Array}
 */
export function getUniqueWordsAsArray(text) {
    // Convert the text to lowercase and then split into words using a regular expression.
    const words = text?.toLowerCase()?.match(/\b\w+\b/g);

    // Use a Set to remove duplicates and then convert it back to an array
    const uniqueWords = Array.from(new Set(words));

    return uniqueWords;
}

/**
 * Tests if a given formula is valid based on simplified criteria:
 * - Contains mathematical operations and/or recognized JavaScript Math function names.
 * - If non-Math words are present, "@" must also be present anywhere in the formula.
 *
 * @param {string} formula The formula to test.
 * @returns {boolean} True if the formula meets the criteria, false otherwise.
 */
export function isValidFormula(formula) {
    // List of JavaScript Math function names
    const mathFunctions = [
        'abs',
        'acos',
        'acosh',
        'asin',
        'asinh',
        'atan',
        'atan2',
        'atanh',
        'cbrt',
        'ceil',
        'cos',
        'cosh',
        'exp',
        'floor',
        'log',
        'max',
        'min',
        'pow',
        'random',
        'round',
        'sin',
        'sinh',
        'sqrt',
        'tan',
        'tanh',
        'trunc',
    ];

    // Regex to match mathematical operations
    const mathOpsRegex = /[\d\s]+[\+\-\*\/]+[\d\s]+/;
    // Check for presence of any Math function name
    const containsMathFunction = mathFunctions.some((fn) => formula.includes(fn));
    // Check for "@" symbol
    const containsAtSymbol = formula.includes('@');

    // Determine if there are any words that are not recognized Math functions
    let containsNonMathWords = false;
    formula.replace(/\b([a-zA-Z_][a-zA-Z0-9_]*)\b/g, (match, p1) => {
        if (!mathFunctions.includes(p1) && !p1.startsWith('@')) {
            containsNonMathWords = true;
        }
    });

    const mathops = mathOpsRegex.test(formula);

    let valid = mathops || containsMathFunction;
    if (!valid) valid = !containsNonMathWords || (containsNonMathWords && containsAtSymbol);

    // A formula is valid if:
    // It contains mathematical operations or recognized Math functions, and
    // If it contains non-Math words, "@" must also be present
    return valid;
}

/**
 * Test if a value is a json parsable string
 *
 * @param {*} value
 * @returns
 */
export function isJSONString(value) {
    try {
        JSON.parse(value);
        return true;
    } catch (e) {
        return false;
    }
}

/**
 *
 * Make sure name used for formula is unique
 *
 * @param {Array} effectFormulas ({ name: `${effect.name}-${change.key}`, formula: details.formula })
 * @param {String} baseName
 * @returns String
 *
 */
export function getUniqueFormulaName(effectFormulas, baseName) {
    let uniqueName = baseName;
    let counter = 1;

    // Check if the uniqueName already exists in effectFormulas
    while (effectFormulas.some((formula) => formula.name === uniqueName)) {
        // If it does, append/increment a counter to make the name unique
        uniqueName = `${baseName}-${counter}`;
        counter++;
    }

    return uniqueName;
}

/**
 * Retrieves the human-readable label of a compendium pack given an item's pack identifier.
 * @param {string} packIdentifier - The compendium pack identifier from item.pack.
 * @returns {string|null} The human-readable label of the compendium, or null if not found.
 */
export function getCompendiumLabelFromPackIdentifier(packIdentifier) {
    // Find the compendium pack in the game.packs collection
    const pack = game.packs.find((p) => p.collection === packIdentifier);

    // If the pack is found, return its label; otherwise, return null
    return pack ? pack.metadata.label : null;
}

/**
 * Sync owned spells with source spells.
 *
 * Search through an actor's owned items for spells, look for matching spells
 * in the game items and compendiums, and update the actor's spell with
 * differences from the found spell.
 *
 * @param {Actor} actor - The actor to search through.
 */
export async function updateActorSpellsFromSources(actor) {
    // Ensure the actor has items
    if (!actor.items) return;

    /** helper function to match spell */
    function validSpellMatch(item, spell) {
        return (
            item.type === 'spell' &&
            item.system?.attributes?.type.toLowerCase() !== 'scroll' &&
            item.name === spell.name &&
            item.system.type === spell.system.type &&
            item.system.level === spell.system.level
        );
    }

    /**
     * Update the spell on the actor with differences from the found spell.
     * @param {Item} spell - The actor's spell to update.
     * @param {Item} foundSpell - The found matching spell.
     */
    async function updateSpell(spell, foundSpell) {
        const diff = foundry.utils.diffObject(spell.toObject(), foundSpell.toObject());

        // Define an array of properties to exclude from the update
        const excludeProperties = ['quantity', 'learned', 'xp', 'attributes'];

        // Remove the excluded properties from diff.system if present
        if (diff.system) {
            excludeProperties.forEach((prop) => {
                if (diff.system.hasOwnProperty(prop)) delete diff.system[prop];
            });
        }

        // Check if there's anything left to update after excluding properties
        if (!foundry.utils.isEmpty(diff) && !foundry.utils.isEmpty(diff.system)) {
            ui.notifications.warn(`Updating ${spell.name}.`, { permanent: true });
            console.log(`Updating ${spell.name}`, { spell, foundSpell }, diff);
            await spell.update({ system: diff.system });
        }
    }

    // Search in compendiums
    const allItemPacks = game.packs.filter((i) => i.metadata.type === 'Item');
    let packItems = [];
    for (let pack of allItemPacks) {
        const packContent = await pack.getDocuments();
        packItems = packItems.concat(packContent);
    }

    // Search for matches in game.items and loaded compendiums
    for (let spell of actor.spells) {
        let matches = [];
        game.items.forEach((item) => {
            if (validSpellMatch(item, spell)) {
                matches.push({ item, source: 'World' });
            }
        });

        packItems.forEach((item) => {
            if (validSpellMatch(item, spell)) {
                // matches.push({ item, source: pack.metadata.label });
                matches.push({ item, source: getCompendiumLabelFromPackIdentifier(item.pack) });
            }
        });

        // If there are multiple matches, prompt the user to select one
        if (matches.length > 1) {
            let dialogContent = `<form><div class="form-group"><label>Multiple matches found for ${spell.name}, select one:</label><select id="match-select">`;
            matches.forEach((match, index) => {
                dialogContent += `<option value="${index}">${match.item.name} (${match.source})</option>`;
            });
            dialogContent += `</select></div></form>`;

            new Dialog({
                title: `Select Match for ${spell.name}`,
                content: dialogContent,
                buttons: {
                    select: {
                        icon: '<i class="fas fa-check"></i>',
                        label: 'Select',
                        callback: (html) => {
                            const selectedIndex = parseInt(html.find('#match-select').val());
                            const selectedMatch = matches[selectedIndex];
                            updateSpell(spell, selectedMatch.item);
                        },
                    },
                },
            }).render(true);
        } else if (matches.length === 1) {
            // If there is exactly one match, update the spell directly
            updateSpell(spell, matches[0].item);
        }
    }
    ui.notifications.notify(`Update complete.`, { permanent: true });
}

export function playAudioDeath(rollMode = 'publicroll') {
    const audioPlayTriggers = game.settings.get('ars', 'audioPlayTriggers');
    const audioTriggersVolume = game.settings.get('ars', 'audioTriggersVolume');
    console.log('utilities.js playAudioDeath', {
        rollMode,
        audioPlayTriggers,
        audioTriggersVolume,
    });
    if (audioPlayTriggers) {
        const audioTriggerDeath = game.settings.get('ars', 'audioTriggerDeath');
        foundry.audio.AudioHelper.play(
            {
                src: audioTriggerDeath,
                volume: audioTriggersVolume,
            },
            false
        );
    }
}
export function playAudioUnconscious(rollMode = 'publicroll') {
    const audioPlayTriggers = game.settings.get('ars', 'audioPlayTriggers');
    const audioTriggersVolume = game.settings.get('ars', 'audioTriggersVolume');
    console.log('utilities.js playAudioUnconscious', {
        rollMode,
        audioPlayTriggers,
        audioTriggersVolume,
    });
    if (audioPlayTriggers) {
        const audioTriggerUnconscious = game.settings.get('ars', 'audioTriggerUnconscious');
        foundry.audio.AudioHelper.play(
            {
                src: audioTriggerUnconscious,
                volume: audioTriggersVolume,
            },
            false
        );
    }
}

/**
 * Delays the execution of a function.
 * @param {Function} callback - A function to execute after the delay.
 * @param {number} delay - The delay in milliseconds before executing the callback.
 * @returns {number} The timeout ID that can be used to cancel the timeout with clearTimeout.
 */
export function delayExecution(callback, delay) {
    const timeoutId = setTimeout(callback, delay);
    return timeoutId;
}

/**
 * Return whether dice rolls for dice so nice should show to everyone or not
 * @param {String} rollMode
 * @returns {Boolean}
 */
export function diceRollModeVisibility(rollMode, self = false) {
    console.log('CONST', { CONST });
    switch (rollMode) {
        case CONST.DICE_ROLL_MODES.PUBLIC:
            return true;
        case CONST.DICE_ROLL_MODES.PRIVATE:
        case CONST.DICE_ROLL_MODES.BLIND:
        case CONST.DICE_ROLL_MODES.SELF:
            return false;
            break;
        default:
            return true;
    }
}

/**
 *
 * Find Token by ID and return it
 *
 * @param {string} tokenId - The ID of the token to find.
 * @param {string} [sceneId] - The ID of the scene to search. If not provided, searches all scenes.
 * @returns {Token|null} The found Token object or null if not found.
 *
 */
export function getTokenById(tokenId, sceneId = null) {
    // Check if a specific sceneId is provided
    if (sceneId) {
        // Try to find the specified scene using the provided sceneId
        let scene = game.scenes.get(sceneId);
        // If the scene is found, try to find the token within that scene
        if (scene) {
            let token = scene.tokens.get(tokenId);
            // If the token is found, return the token object
            if (token) {
                return token;
            }
        }
    } else {
        // If no sceneId is provided, loop through all scenes in the game
        for (let scene of game.scenes.contents) {
            // Try to find the token within the current scene using the provided tokenId
            let token = scene.tokens.get(tokenId);
            // If the token is found, return the token object
            if (token) {
                return token;
            }
        }
    }
    // If the token is not found in the specified or all scenes, return null
    return null;
}

/**
 *
 * Create a hash id that is the same using the provided effectId and actorId
 *
 * @param {String} effectId
 * @param {String} actorId
 * @returns {String} hash of the 2 ids.
 */
export function createHashFromIds(effectId, actorId) {
    const combinedString = `${effectId}-${actorId}`;
    let hash = 0;
    for (let i = 0; i < combinedString.length; i++) {
        const char = combinedString.charCodeAt(i);
        hash = (hash << 5) - hash + char;
        hash |= 0; // Convert to 32bit integer
    }
    const uniqueId = Math.abs(hash).toString(36).padStart(16, '0').slice(0, 16);
    return uniqueId;
}

// export async function dothis() {
// }
