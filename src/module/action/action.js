import { ARS } from '../config.js';
import { ARSItem } from '../item/item.js';
import { ARSActor } from '../actor/actor.js';
import { ActionSheet, ActionGroupSheet } from './action-sheet.js';
import * as utilitiesManager from '../utilities.js';
import * as dialogManager from '../dialog.js';
/**
 * //TODO:
 * 
 * Actions that are attached to items or actors, basically in anything
 * 
 * This class will take the old action object and put it into 
 * the class and it will become a ARSAction object
 * 
 *       "action":{
         "img":"",
        //  "templates":[
        //     "itemDescription"
        //  ],
        "abilityCheck": {
            "type": "none",
            "formula": ""
        }
        "castShapeAngle": {
            "formula": ""
        },
        "castShapeLength": {
            "formula": ""
        },
        "castShapeRadius": {
            "formula": ""
        },
        "castShapeWidth": {
            "formula": ""
        },
        "castShapeProperties": {
            "castShapeRadius": {
                "formula": ""
            },
            "range": {
                "formula": ""
            },
            "inRangeColor": "#f2ed69",
            "outOfRangeColor": "#a80000"
        },

        // "castShape": {
        //     "angle": { "formula": "" },
        //     "length": { "formula": "" },
        //     "radius": { "formula": "" },
        //     "width": { "formula": "" },
        //     "shape": { "type": "circle" },
        //     "selection": { "type": "all" },
        //     "properties": {
        //         "radius": { "formula": "" },
        //         "range": { "formula": "" },
        //         "inRangeColor": "#f2ed69",
        //         "outOfRangeColor": "#a80000"
        //     }
        // }

        "type": "cast",
        "targeting": "",
        "targetShape": {
            "type": "circle"
        },
        "targetShapeSelection": {
            "type": "all"
        },         
         "ability":"none",
         "type":"cast",
         "targeting":"",
         "successAction":"",
         "targetType": "",
         "formula":"",
         "otherdmg":[
            
         ],
         "effectList":[
            
         ],
         "effect":{
            "duration":{
               "formula":"",
               "type":"round"
            },
            "changes":[
               
            ]
         },
         "speed":0,
         "damagetype":"slashing",
         "resource":{
            "type":"none",
            "itemId":"",
            "reusetime":"",
            "count":{
               "cost":1,
               "min":0,
               "max":1,
               "value":0
            }
         },
         "magicpotency": 0,
         "properties":[]
      },
 * 
 */
export class ARSAction {
    /**
     *
     * @param {Object} source actor, item
     * @param {Number} atIndex where in the actions list should this be placed?
     * @param {Number} atIndex sort index number
     *
     */
    constructor(source = {}, data = {}) {
        this.source = source;
        // this.sort = atIndex ? atIndex : 0;
        this.sort = data.sort ?? undefined;
        if (data.name) {
            this.name = data.name;
        } else if (source?.name) {
            this.name = source.name;
        } else {
            this.name = 'unknown';
        }
        if (data.img) {
            this.img = data.img;
        } else if (source?.img) {
            this.img = source.img;
        } else {
            this.img = ARS.icons.general.combat.cast;
        }

        //TODO: switch to this
        // "castShape": {
        //     "angle": { "formula": "" },
        //     "length": { "formula": "" },
        //     "radius": { "formula": "" },
        //     "width": { "formula": "" },
        //     "shape": { "type": "circle" },
        //     "selection": { "type": "all" },
        //     "properties": {
        //         "radius": { "formula": "" },
        //         "range": { "formula": "" },
        //         "inRangeColor": "#f2ed69",
        //         "outOfRangeColor": "#a80000"
        //     }
        // }
        this.castShapeAngle = data.castShapeAngle || { formula: '' };
        this.castShapeLength = data.castShapeLength || { formula: '' };
        this.castShapeRadius = data.castShapeRadius || { formula: '' };
        this.castShapeWidth = data.castShapeWidth || { formula: '' };
        this.castShapeProperties = data.castShapeProperties || {
            castShapeRadius: { formula: '' },
            range: { formula: '' },
            inRangeColor: '#f2ed69',
            outOfRangeColor: '#a80000',
        };

        this.type = data.type || 'cast';
        this.targeting = data.targeting || '';
        this.targetShape = data.targetShape || { type: 'circle' };
        this.targetShapeSelection = data.targetShapeSelection || { type: 'all' };

        this.successAction = data.successAction || '';
        this.formula = data.formula || '';
        this.otherdmg = data.otherdmg || [];
        this.effectList = data.effectList || [];
        this.effect = data.effect || { duration: { formula: '', type: 'round' }, changes: [] };
        this.itemList = data.itemList || [];
        this.speed = data.speed || 0;
        this.damagetype = data.damagetype || 'slashing';
        this.dmonlytext = data.dmonlytext || '';
        this.resource = data.resource || {
            type: 'none',
            itemId: '',
            reusetime: '',
            count: { cost: 1, min: 0, max: 1, value: 0 },
        };
        this.magicpotency = data.magicpotency || 0;
        this.misc = data.misc || '';
        this.properties = data.properties || [];

        this.saveCheck = data.saveCheck || { formula: '', type: 'spell' };

        this.ability = data.ability || 'none';

        this.abilityCheck = data.abilityCheck || {
            type: 'none',
            formula: '',
        };

        this.parentuuid = data?.parentuuid ? data.parentuuid : undefined;

        if (!this.parentuuid) {
            this.parentuuid = source?.uuid ? source.uuid : undefined;
        }
        this.id = data.id || foundry.utils.randomID(16);

        this.#populateFormulaEvals();
    }

    // serialize() {
    //     return {
    //         id: this.id,
    //         sort: this.sort,
    //         name: this.name,
    //         img: this.img,
    //         ability: this.ability,
    //         abilityCheck : this.abilityCheck,
    //         castShapeAngle: this.castShapeAngle,
    //         castShapeLength: this.castShapeLength,
    //         castShapeRadius: this.castShapeRadius,
    //         castShapeWidth: this.castShapeWidth,
    //         castShapeProperties: this.castShapeProperties,
    //         type: this.type,
    //         targeting: this.targeting,
    //         targetShape: this.targetShape,
    //         targetShapeSelection: this.targetShapeSelection,
    //         successAction: this.successAction,
    //         formula: this.formula,
    //         otherdmg: this.otherdmg,
    //         effectList: this.effectList,
    //         effect: this.effect,
    //         itemList: this.itemList,
    //         speed: this.speed,
    //         damagetype: this.damagetype,
    //         dmonlytext: this.dmonlytext,
    //         resource: this.resource,
    //         magicpotency: this.magicpotency,
    //         misc: this.misc,
    //         properties: this.properties,
    //         saveCheck: this.saveCheck,
    //         parentuuid: this.parentuuid,
    //         sourceuuid: this.source.uuid,
    //     };
    // }

    get uuid() {
        return `${this.source.uuid}.Action.${this.id}`;
    }
    serialize() {
        // Extract the source UUID if available, otherwise use a placeholder
        const sourceUuid = this.source && this.source.uuid ? this.source.uuid : 'unknown-source';

        // Create a serialized representation of the ARSAction instance.
        // Use object destructuring to exclude the `source` property and manually include all other properties.
        const { ...serialized } = this;

        // remove thse from serialization (item/owner object, add these via its uuid during load/creation)
        delete serialized.source;
        delete serialized.parentGroup;
        delete serialized.formulaEvaluated;

        // Return the serialized data with the source UUID explicitly added.
        return {
            ...serialized, // Spread the remaining properties of the action
            sourceuuid: sourceUuid, // Include the source UUID
        };
    }

    /** deserilize data into class based style for ease of use */
    static deserialize(source, data = {}) {
        // const source = fromUuidSync(data.sourceuuid);
        return new ARSAction(source, data);
    }

    // Convert the class instance to a simple object
    toObject() {
        return Object.assign({}, this);
    }

    /**
     * convience function to save.
     *
     * @returns
     */
    save() {
        console.log('triggering action save', this);
        return this.parentGroup.save();
    }
    /**
     *
     * Create ARSAction from raw actionObject (not ARSAction yet)
     *
     * Used for conversion of old actions to ARSAction
     *
     * @param {Object} actionObject
     */
    static createFromOldAction(actionObject) {
        if (typeof actionObject !== 'object' || actionObject === null) {
            throw new Error('Invalid actionObject provided to ARSAction createFromAction');
        }

        // Copy all enumerable properties from actionObject to this
        Object.assign(this, actionObject);

        // Deep clone specific properties
        this.sort = actionObject.index;
        this.properties = foundry.utils.deepClone(actionObject.properties);
        this.effectList = foundry.utils.deepClone(actionObject.effectList);
        this.effect = foundry.utils.deepClone(actionObject.effect);
        this.resource = foundry.utils.deepClone(actionObject.resource);

        return this;
    }

    /**
     *
     * Convert an old actions bundle to ARSActionGroup
     *
     * @param {Object} actionSource
     * @param {Object} bundle
     * @returns
     */
    static convertFromActionBundle(actionSource, bundle) {
        console.log('ARSAction convertFromActionBundle', { actionSource, bundle });
        if (!Array.isArray(bundle) || bundle.length === 0) {
            // throw new Error('Invalid or empty bundle provided');
            return [];
        }

        if (!game.user.isGM && actionSource instanceof ARSItem && !actionSource.isIdentified) {
            actionSource.system.actionCount = 0;
            return [];
        }

        const actions = {};
        let sortOrder = 1;
        // First loop: Create ARSAction instances and group them by name
        for (const action of bundle) {
            if (!action || !action.name) {
                throw new Error('Invalid action structure in bundle');
            }

            const newAction = new ARSAction(actionSource, action);
            if (!actions[action.name]) {
                actions[action.name] = [];
            }
            newAction.sort = sortOrder;
            actions[action.name].push(newAction);
            sortOrder++;
        }

        const actionGroups = new Map();

        // Second loop: Create ARSActionGroup instances from the grouped actions
        Object.entries(actions).forEach(([name, groupActions]) => {
            const actionWithSpeed = groupActions.find((action) => ['cast', 'melee', 'thrown', 'ranged'].includes(action.type));
            const speed = actionWithSpeed ? actionWithSpeed.speed : 0;

            const newGroup = new ARSActionGroup(actionSource, { name, speed }, groupActions);
            actionGroups.set(newGroup.id, newGroup);
        });

        return actionGroups;
    }

    /**
     * Helper function to process visual formula for chatCards/etc
     */
    async #populateFormulaEvals() {
        if (this.formula) {
            let rollData = this.source?.getRollData() ?? '';
            if (this.source instanceof ARSItem && this.source.isOwned) rollData = this.source?.actor?.getRollData() ?? '';
            try {
                let roll = new Roll(this.formula, rollData);
                await roll.evaluate();
                this.formulaEvaluated = roll.formula;
            } catch (err) {
                console.warn(`action.js #populateFormulaEvals formula ${this.formula}`, { err });
            }
        }
    }
} // end class ARSAction

/**
 * Class that is a group of Actions.
 */
export class ARSActionGroup {
    /**
     *
     * @param {Object} actor or item object this action group is from
     * @param {Obect} context
     * @param {Array} actions
     */
    constructor(object = undefined, context = { name: 'unknown-group', speed: 0 }, actions = []) {
        console.log('ARSActionGroup constructor', { object, context, actions });
        // Initialize the instance with provided values or defaults.
        this.source = object;
        this.name = context?.name;
        if (context.img) {
            this.img = context.img;
        } else if (object?.img) {
            this.img = object.img;
        } else {
            this.img = ARS.icons.general.combat.cast;
        }
        this.description = context.description ? context.description : object?.description ?? '';
        this.actions = new Map(); // Initialize an empty Map to store action details.

        // Sort actions based on their sort value to ensure processing order.
        actions?.sort((a, b) => a.sort - b.sort);

        // Flags to track specific types of actions present in the input.
        let hasMelee = false,
            hasCast = false,
            hasDamage = false,
            hasResource = false,
            hasUses = false;

        // Determine the type of `object` to set `actor` and `item` appropriately.
        const actor = object instanceof ARSActor ? object : object.actor;
        const item = object instanceof ARSItem ? object : undefined;

        let sortOrder = 1;

        // Iterate over each action to configure and evaluate its properties.
        actions.forEach((action) => {
            // Ensure each action is a valid object with necessary properties.
            if (typeof action !== 'object' || !('sort' in action)) {
                throw new Error('Invalid action structure. Each action must be an object with a `sort` property.');
            }

            // property pointing to parent group the action resides in
            action.parentGroup = this;

            // Process each action based on its type to set flags.
            switch (action.type) {
                case 'cast':
                    hasCast = true;
                    break;
                case 'melee':
                    hasMelee = true;
                    break;
                case 'damage':
                case 'heal':
                    hasDamage = true;
                    break;
            }

            // Check if the action involves resources and update flags accordingly.
            if (action.resource?.type !== 'none') {
                hasResource = true;
                // Calculate resource uses if applicable.
                if (actor && action.resource.type !== 'none') {
                    hasUses = utilitiesManager.getActionCharges(actor, item, action);
                }
            }
            if (!action.sort) action.sort = sortOrder;
            this.add(action);

            sortOrder++;
        });

        // Store flags as instance properties.
        this.hasMelee = hasMelee;
        this.hasCast = hasCast;
        this.hasDamage = hasDamage;
        this.hasResource = hasResource;
        this.hasUses = hasUses;

        // Add each action to the instance's Map for further processing or reference.
        // actions.forEach((action) => this.add(action));

        // Generate a unique ID for the instance for identification purposes.
        this.id = context.id ? context.id : foundry.utils.randomID(16);
    }

    get speed() {
        // Define the valid action types
        const validActionTypes = ['cast', 'melee', 'ranged', 'thrown'];

        // Use Array.from to convert the Map's values to an array and then use reduce
        return Array.from(this.actions.values()).reduce((maxSpeed, action) => {
            return validActionTypes.includes(action.type) && action.speed > maxSpeed ? action.speed : maxSpeed;
        }, 0);
    }

    // Convert the class instance to a simple object
    toObject() {
        return Object.assign({}, this);
    }

    /**
     *
     * move actiontoMove action to before actionTarget or to end if no actionTarget
     *
     * @param {ARSAction} actionToMove
     * @param {ARSAction} actionTarget
     */
    moveAction(actionToMove, actionTarget) {
        if (!this.actions.has(actionToMove.id)) {
            throw new Error('actionToMove not found in the group');
        }

        let actionList = Array.from(this.actions.values());

        // Remove actionToMove from its current position
        actionList = actionList.filter((action) => action.id !== actionToMove.id);

        // Determine the insertion index for actionToMove
        let targetIndex = actionTarget ? actionList.findIndex((action) => action.id === actionTarget.id) : actionList.length;
        if (actionTarget && targetIndex === -1) {
            throw new Error('actionTarget not found in the group');
        }

        // Insert actionToMove at the target index or at the end if actionTarget is null
        actionList.splice(targetIndex, 0, actionToMove);

        // Update the index and sort value for each action based on its new position
        this.actions.clear();
        actionList.forEach((action, index) => {
            action.sort = index; // Update the action's sort property to match its index
            this.actions.set(action.id, action);
        });
    }

    add(action, sortIndex = undefined) {
        if (!action || typeof action !== 'object') {
            throw new Error('Invalid action provided');
        }

        // Determine the sort index for the new action
        if (sortIndex !== undefined && typeof sortIndex !== 'number') {
            throw new Error('Sort index must be a number');
        }

        if (sortIndex === undefined) {
            // If no sort index is provided, set it to the highest current index + 1
            const maxIndex = Array.from(this.actions.values()).reduce((max, curr) => Math.max(max, curr.sort), -1);
            sortIndex = maxIndex + 1;
        }

        // Set the sort property on the action
        action.sort = sortIndex;

        // Insert action into the map
        this.actions.set(action.id, action);

        // Optional: re-sort actions in the map if order is important in subsequent operations
        this.sortActions();
    }

    /**sort the actions map */
    sortActions() {
        const sortedActions = Array.from(this.actions.values()).sort((a, b) => a.sort - b.sort);
        this.actions.clear();
        sortedActions.forEach((action) => this.actions.set(action.id, action));
    }

    /**
     * Remove action from group
     *
     * @param {*} action
     * @returns
     */
    remove(action) {
        const actionId = action.id;
        if (action) {
            action.group = null;
            this.actions.delete(actionId); // Remove from the actions
        }

        return this;
    }

    /** serialize data for save to database */
    serialize() {
        const actionsSerialized = Array.from(this.actions.values()).map((action) => action.serialize());
        return {
            id: this.id,
            name: this.name,
            img: this.img ?? '',
            description: this.description ?? '',
            // speed: this.speed,
            actions: actionsSerialized,
            sourceuuid: this.source.uuid,
        };
    }

    // /** deserilize data into class based style for ease of use */
    // static deserialize(source, data = {}) {
    //     const actions = [];

    //     if (data.actions) {
    //         for (const [id, action] of Object.entries(data.actions)) {
    //             actions.push(action.deserialize(source));
    //         }
    //     }

    //     // const source = fromUuidSync(data.sourceuuid);
    //     return new ARSActionGroup(source, { id: data.id, name: data.name, speed: data.speed }, actions);
    // }

    /** deserialize an array of serialized ARSActionGroup objects */
    static deserializeGroups(source) {
        const serializedGroups = source.system.actionGroups;
        if (!Array.isArray(serializedGroups)) {
            throw new Error('Expected an array of serialized groups');
        }

        const deserializedMap = new Map();

        serializedGroups.forEach((groupData) => {
            // const source = fromUuidSync(groupData.sourceuuid);
            const actions = (groupData.actions || []).map((actionData) => ARSAction.deserialize(source, actionData));
            const group = new ARSActionGroup(source, { ...groupData }, actions);
            deserializedMap.set(groupData.id, group);
        });

        return deserializedMap;
    }

    /** serialize an array of ARSActionGroup class objects */
    static serializeGroups(allGroups = new Map()) {
        if (!(allGroups instanceof Map)) {
            throw new Error('Expected a Map of ARSActionGroup instances');
        }
        return Array.from(allGroups.values()).map((group) => group.serialize());
    }

    /**
     * Save all actionGroups on source
     *
     * @param {*} source
     * @returns
     */
    static async saveAll(source = undefined) {
        const serializedGroups = this.serializeGroups(source.actionGroups);
        console.log('action.js saveAll', { source, serializedGroups });
        await source.update({ 'system.actionGroups': serializedGroups });
        // refresh Map() with saved data by loading the new saved data
        // this might be moved to actor.prepareData()??
        // re-load the action groups to ensure consistency
        // do this because dont have a prepareData for just actions
        // for things like calculated speed for the actionGroup
        source.actionGroups = this.loadAll(source);
    }

    /**
     * Load the actionGroups for this source
     *
     * @param {*} source
     * @returns @returns {Map(ARSActionGroup)}
     */
    static loadAll(source = undefined) {
        const actionGroups = this.deserializeGroups(source);
        // set the top level map
        source.actionGroups = actionGroups;
        return actionGroups;
    }

    /**
     *
     * Save a specific ActionGroup.
     * @param {ARSActionGroup} actionGroup - The ActionGroup to be saved.
     * @returns {Promise<void>}
     *
     */
    static async saveGroup(actionGroup) {
        if (!(actionGroup instanceof ARSActionGroup)) {
            throw new Error('Invalid ActionGroup provided');
        }

        const serializedGroup = actionGroup.serialize();
        console.log('Saving specific ActionGroup', { serializedGroup });

        // Assuming 'source' here refers to the actor or item the ActionGroup belongs to.
        const source = actionGroup.source;
        if (!source) {
            throw new Error('ActionGroup has no source to save to');
        }

        // Find the specific action group within the source's action groups
        const existingGroups = source.system.actionGroups || [];
        const groupIndex = existingGroups.findIndex((group) => group.id === serializedGroup.id);

        if (groupIndex !== -1) {
            // Update the existing action group
            existingGroups[groupIndex] = serializedGroup;
        } else {
            // Add the new action group
            existingGroups.push(serializedGroup);
        }

        await source.update({ 'system.actionGroups': existingGroups });

        // re-load the action groups to ensure consistency
        // ARSActionGroup.loadAll(source);
        actionGroup.source.actionGroups = this.loadAll(source);
    }

    //convenience function to save all from a actionGroup entry
    save() {
        return ARSActionGroup.saveAll(this.source);
    }

    //convenience function to load all all from a actionGroup entry
    load() {
        return ARSActionGroup.loadAll(this.source);
    }
} // end class ARSActionGroup

/**
 * Finds an action by its ID within a nested Map structure.
 *
 * @param {Map} actionGroups - A Map of action groups, where each group contains a Map of actions.
 * @param {string} actionId - The ID of the action to find.
 * @returns {object|null} - The action object if found, otherwise null.
 */
export function findActionById(actionGroups, actionId) {
    // Check if actionGroups is a Map
    if (!(actionGroups instanceof Map)) {
        console.error('Invalid input: actionGroups should be a Map');
        return null;
    }

    // Check if actionId is provided and is a non-empty string
    if (typeof actionId !== 'string' || actionId.trim() === '') {
        console.error('Invalid input: actionId should be a non-empty string');
        return null;
    }

    // Iterate through each action group in the Map
    for (const [groupKey, actionGroup] of actionGroups) {
        // Check if the actionGroup has a Map of actions
        if (actionGroup.actions instanceof Map) {
            // Iterate through each action in the actionGroup's actions Map
            for (const [actionKey, action] of actionGroup.actions) {
                // Check if the current action's ID matches the provided actionId
                if (action.id === actionId) {
                    return action;
                }
            }
        } else {
            console.warn(`actionGroup with key ${groupKey} does not contain a Map of actions`);
        }
    }

    // Return null if no action with the given ID is found
    return null;
}

/**
 * Manage actions (add/delete/edit)
 * @param {*} event
 * @param {*} object
 */
export async function manageActions(event, object) {
    event.stopPropagation();

    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;
    const li = element.closest('li');
    const actionToPerform = dataset.action;
    const actionGroupId = li.dataset.actionGroupId;
    const actionId = li.dataset.actionId;
    // const effectId = li.dataset.effectId;
    const sourceitemUuid = li.dataset.sourceitemUuid;

    // find the source of this actionGroup
    const sourceObject = sourceitemUuid ? await fromUuid(sourceitemUuid) : object;

    // find the actionGroup
    const actionGroup = sourceObject.actionGroups.get(actionGroupId);

    // find the action in the group
    const action = actionGroup ? actionGroup.actions.get(actionId) ?? undefined : undefined;

    console.log('action.js manageActions', { sourceObject, element, dataset, li, actionToPerform, actionGroup, action });
    switch (actionToPerform) {
        case 'create':
            actionGroup.add(new ARSAction(sourceObject, { name: `${actionGroup.name}`, speed: 0 }));
            await actionGroup.save();
            break;

        case 'delete':
            if (await dialogManager.confirm('Delete action?', `Delete ${action.name}`)) {
                actionGroup.actions.delete(actionId);
                await actionGroup.save();
            }
            break;

        case 'edit':
            const actionSheet = new ActionSheet(action);
            actionSheet.render(true);
            break;

        default:
            console.log('action.js manageActions Unknown actionToPerform type=', actionToPerform);
            break;
    }
}

/**
 * Manage Action Groups (add/delete/edit)
 *
 * @param {*} event
 * @param {*} object
 */
export async function manageActionGroups(event, object) {
    event.stopPropagation();

    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;
    const li = element.closest('li');
    const actionToPerform = dataset.action;
    const actionGroupId = li?.dataset?.actionGroupId;
    const actionId = dataset.actionId;

    const sourceitemUuid = li?.dataset?.sourceitemUuid ?? undefined;

    // find the source of this actionGroup
    const sourceObject = sourceitemUuid ? await fromUuid(sourceitemUuid) : object;

    // find the actionGroup
    const actionGroup = actionGroupId ? sourceObject.actionGroups.get(actionGroupId) : undefined;

    // const actionGroup = object.actionGroups.get(actionGroupId) ?? undefined;

    // const actionGroup = object.actionGroups?.get(actionGroupId) ?? undefined;
    // const effectId = li.dataset.effectId;

    console.log('action.js manageActionGroups', { sourceObject, element, dataset, li, actionToPerform });
    switch (actionToPerform) {
        case 'create':
            const newActionGroup = new ARSActionGroup(sourceObject, { name: 'New Group' }, [
                new ARSAction(sourceObject, { name: `New Action`, speed: 0 }),
            ]);
            sourceObject.actionGroups.set(newActionGroup.id, newActionGroup);
            await newActionGroup.save();
            // object.sheet.render();
            break;

        case 'delete':
            if (await dialogManager.confirm('Delete Action Group and all actions within?', `Delete ${actionGroup.name}`)) {
                sourceObject.actionGroups.delete(actionGroupId);
                await ARSActionGroup.saveAll(sourceObject);
            }
            break;

        case 'edit':
            // new ActionGroupSheet({ actionGroup }, {}).render(true);
            const actionGroupEditSheet = new ActionGroupSheet({ actionGroup }, {});
            actionGroupEditSheet.render(true);
            // const result = await dialogManager.getActionGroupDetails(actionGroup, `Edit Action Group: ${actionGroup.name}`);
            // if (result.name) actionGroup.name = result.name;
            // await actionGroup.save();
            break;
        default:
            console.log('action.js manageActionGroups Unknown actionToPerform type=', actionToPerform);
            break;
    }
}

/**
 *
 * build drag package for action
 *
 * @param {*} event
 * @returns
 */
export async function onDragActionStart(event) {
    console.log('action.js onDragStart', { event });
    const classes = Array.from(event.target.classList);
    if (classes.includes('action-move-control')) {
        console.log('Has action-move-control class');
        event.stopPropagation();
        const element = event.target;
        const listItem = element.closest('li');
        const actionId = listItem.dataset.actionId;
        const actionGroupId = listItem.dataset.actionGroupId;
        const sourceUuid = listItem.dataset.sourceitemUuid;
        const dragData = {
            type: 'Action',
            sourceUuid: sourceUuid,
            actionId: actionId,
            actionGroupId: actionGroupId,
        };
        console.log('action.js onDragStart', { dragData });
        event.dataTransfer.setData('text/plain', JSON.stringify(dragData));
        return true;
    } // end action-move-control
    return false;
}

/**
 *
 * capture onDrops for actions (move/copy/etc)
 *
 * @param {*} event
 * @returns
 */
export async function onDropAction(event) {
    console.log('action.js onDropAction', { event });
    const classes = Array.from(event.currentTarget.classList);
    if (classes.includes('action-move-control')) {
        event.preventDefault();
        // Get the dropped data (in case of text data, for example)
        const data = event.dataTransfer.getData('text');
        // Logging the dropped data for demonstration purposes
        console.log('action.js onDropAction Dropped data:', data);
        // get values
        const context = JSON.parse(data);
        const source = await fromUuid(context.sourceUuid);
        const actionGroup = source.actionGroups.get(context.actionGroupId);
        const action = actionGroup.actions.get(context.actionId);
        console.log('action.js onDropAction', { source, actionGroup, action });

        const element = event.currentTarget;
        const listItem = element.closest('li');
        const actionId = listItem.dataset.actionId;
        const actionGroupId = listItem.dataset.actionGroupId;
        const sourceUuid = listItem.dataset.sourceitemUuid;
        const targetSource = await fromUuid(sourceUuid);
        const targetActionGroup = targetSource.actionGroups.get(actionGroupId);
        const targetAction = targetActionGroup.actions.get(actionId);

        console.log('action.js onDropAction', { targetSource, targetActionGroup, targetAction });

        targetActionGroup.moveAction(action, targetAction);
        await targetActionGroup.save();

        return true;
        // get target action details.
    }
    return false;
}
