import { ARSActorSheet } from './actor-sheet.js';
import * as utilitiesManager from '../utilities.js';
import * as debug from '../debug.js';

export class ARSCharacterSheet extends ARSActorSheet {
    /** @override */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            classes: ['ars', 'sheet', 'actor', 'character', 'character-sheet'],
            template: 'systems/ars/templates/actor/character-sheet.hbs',
            // template: "systems/ars/templates/actor/character-sheet.hbs",
            actor: this.actor, // for actor access in character-sheet.hbs
            width: 600,
            height: 700,
            // height: "auto",
            tabs: [{ navSelector: '.sheet-tabs', contentSelector: '.sheet-body', initial: 'main' }],
        });
    }
}
